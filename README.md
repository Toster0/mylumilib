# README #

-------------------------------------------------------

**Highly recommended is to fork ([how to](https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo,+Compare+Code,+and+Create+a+Pull+Request)) this repository, this enables to contribute via pull requests. Don't hesitate to create Issues!**

-------------------------------------------------------

## News Ticker:
* LumiLib is now on v0.04
* The MultiSlotOperators are now ready to use!
* For convenience, the base classes of single slot and respectively multi slot operators are now in single scripts.
* The single slot operator design has changed to be consistent with the multi slot equivalent. Now the ArrayVector must be set via the constructor instead of the setInputSlot method.


## LumiLib Features: ###

* The LumiLib is a multi platform flowbased 3D volume streaming framework ([Wiki](https://bitbucket.org/SvenWanner/lumilib/wiki/Home)).

* Main usage is to stream (rgb, grayscale) image sequences, either from already existing files or from expected incoming files, e.g. from a camera stream.

* Image streams can be separated into packages piping them through an operator chain while the engine is waiting for the next package.

* The main data container ([ArrayVector](https://bitbucket.org/SvenWanner/lumilib/wiki/ArrayVector)) can be considered as a vector of (3D + color channels) volumes.

* ArrayVectors are coupled to hdf5 storage containers to dump vector entries to hard drive during runtime.

* Main purpose is an easy development of dynamically loadable [WorkFlows](https://bitbucket.org/SvenWanner/lumilib/wiki/Workflow)

* WorkFlows consists of [Operator](https://bitbucket.org/SvenWanner/lumilib/wiki/Operator)-chains having the intention of an easy development via a predefined object oriented class structure. 

* For a global merging of the resulting package vectors a similar engine is provided to stream the vector packages enabling a memory saving access to all available vector entries produced through the WorkFlow pipeline.

* To avoid forking the repository you'll find release versions in the [download section](https://bitbucket.org/SvenWanner/lumilib/downloads).

---------------------------------------------------------------------------------------------------------------------------


### Installation ###

After installing all dependencies, add the root folder of your repository clone to your PYTHONPATH. You can use the LumiLib either via the several executable scripts in LumiLib/Executables or interactively using a python terminal like IPython or Dreampie. ([Wiki](https://bitbucket.org/SvenWanner/lumilib/wiki/Home))

On Unix systems you can call the setup.sh script via 
```
$ sh setup.sh
```
This automatically generates the Api documentation

---------------------------------------------------------------------------------------------------------------------------
#### Setup:


_Dependencies:_

pip, h5py, scipy, numpy, psutil, doxygen, PyOpenGL, matplotlib, python-qt4, python-qt4-gl, python-qt4-dev, pyqt4-dev-tools, qimage2ndarray, libvigraimpex-dev, python-configparser, cython (min v0.21), pillow, six, dateutil, pyparsing, multiprocessing


#### On Ubuntu:

add to your .bashrc:
```
PYTHONPATH=$PYTHONPATH:/your/path/to/lumilib 
```
The install_dependencies.sh script contains commands to install all dependencies on your system (tested Ubuntu 14.04). 

#### On Windows:

_Dependencies:_

pip, h5py, scipy, numpy, Pillow, psutil, doxygen, PyOpenGL, matplotlib, PyQt4, qimage2ndarray, vigranumpy, python-configparser, cython (min v0.21), six, python-dateutil, pyparsing, multiprocessing


install packages listed above. LumiLib is only tested using Python 2.7.8. No guarantee for other versions. 

-add to PYTHONPATH:
```
	C:\Python27\Lib
	C:\Python27\DLLs
	C:\Python27\Lib\site-packages
	C:\Python27\Scripts
	C:\your\path\to\lumilib 
```
-add to Path:
```
	C:\Python27\Lib\site-packages
	C:\Python27\Scripts
```


---------------------------------------------------------------------------------------------------------------------------

### Who do I talk to? ###

* Sven Wanner sven.wanner@iwr.uni-heidelberg.de