from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import time
import traceback

from LumiLib.IO.ArrayVector import ArrayVector
from LumiLib.Tools.refocusing import refocusEpi



class MultiSlotBaseOperator(object):
    def __init__(self, dataVec):
        """
        This class is the basis of all multislot operators
        available. It has the basic functionality to set in
        and output slots and parameter slots. It automatically
        checks the input slots shape and provides this info
        to all children. when out and input slot are set a
        function __allocateResultMemory__ is called auto-
        matically.
        """

        # each operator need an ArrayVector instance to operate on
        assert isinstance(dataVec, ArrayVector), "Input data object of wrong type or not set!"

        # these fields are developer relevant and need to be set in child classes
        #========================================================================
        self._operator_inslot_names = []    # names of input slots set by operator designer
        self._operator_outslot_names = []   # names of output slots set by operator designer
        self._essential_parameter = []      # keeps the essential parameter names
        #========================================================================

        self._dataVec = dataVec             # ArrayVector instance keeping the data

        self._parameter = {}                # keeps the parameter

        # maps the operator input slot names to the names set by the user
        # e.g. operator designer defined slot 0 as mySlot0 and slot 1 as mySlot1,
        # then after explicitly setting the slots channel1, channels2 in a workflow
        # self._input_slots maps mySlot1 -> channel1, mySlot2 -> channel2
        self._input_slots = {}

        # maps the operator input slot names to the names set by the user
        # e.g. operator designer defined slot 0 as myOutSlot0 and slot 1 as myOutSlot1,
        # then after explicitly setting the slots ochannel1, ochannels2 in a workflow
        # self._input_slots maps myOutSlot1 -> ochannel1, myOutSlot2 -> ochannel2
        self._output_slots = {}

        # holding the input slot data shapes
        self._input_slots_shape = {}

        # holding the input slot data types
        self._input_dtypes = {}

        self._is_ready = False              # flag is True when all essential slots are set
        self._force_recomputation = True    # if True the operator will always compute even if data are available
        self._progress_step = 0             # computation progress step
        self._progress = 0                  # computation total progress

        self.__name__ = "MultiSlotBaseOperator"  # name of the operator


    def run(self):
        """
        Start the Operators processing chain
        """
        start = time.time()

        if self._is_ready:
            print("\n=====================================\napply", self.__name__, "...")

            # if _force_recomputation is is True process, else
            # process only if not all output channel are available
            if self._force_recomputation:
                self.__process__()
            else:
                try:
                    for osname in self._output_slots.keys():
                        self._dataVec.setVisible(osname)
                except:
                    self.__process__()

            end = time.time()
            print("finished in {0:.2f} s!".format(end-start))
        else:
            assert False, "Operator has at least one empty input slots!"


    def setInputSlot(self, slot_name, channel_name):
        """
        Associate an input slot name to an operator slot name and check if operator is ready
        @param slot_name: <str> the slot_name the channel_name should be associated with
        @param channel_name: <str> the channel of input ArrayVector set to slot_name
        """

        assert isinstance(channel_name, str), "Input Error, channel_name of wrong type!"
        assert isinstance(slot_name, str), "Input Error, slot_name of wrong type!"
        assert len(self._operator_inslot_names) > 0, "No input slots are defined!"

        self._input_slots[slot_name] = channel_name
        self._input_slots_shape[slot_name] = self._dataVec.shapeOf(channel_name)
        self._input_dtypes[slot_name] = self._dataVec.dtypeOf(channel_name)
        self.__checkIfReady__()


    def setOutputSlot(self, slot_name, channel_name=None):
        """
        Associate an output slot name to an operator slot name and check if operator is ready
        @param slot_name: <str> the slot_name the channel_name should be associated with
        @param channel_name: <str> the channel of input ArrayVector set to slot_name
        """

        if channel_name is not None:
            assert isinstance(channel_name, str), "Input Error, channel_name of wrong type!"
        assert isinstance(slot_name, str), "Input Error, slot_name of wrong type!"
        assert len(self._operator_outslot_names) > 0, "No output slots are defined!"

        if channel_name is None:
            channel_name = slot_name
        self._output_slots[slot_name] = channel_name
        self.__checkIfReady__()


    def setParameterSlot(self, name, value):
        """
        Set an input parameter slot and check if operator is ready
        @param name: <str> parameter name
        @param value: <obj> parameter value
        """
        assert isinstance(name, str), "Wrong parameter name input type!"
        assert name in self._essential_parameter, "Unknown parameter!"
        self._parameter[name] = value
        self.__checkIfReady__()


    def __checkIfReady__(self):
        """
        Checks if all slots are connected, if so, this method calls
        the __ready__ and the __allocateMemory__ function in all
        child classes
        """
        self._is_ready = True

        # check input slots
        for setkey in self._input_slots.keys():
            found = False
            for opkey in self._operator_inslot_names:
                if opkey == setkey:
                    found = True
            if not found:
                self._is_ready = False
                break

        # check output slots
        if self._is_ready:
            if self._operator_outslot_names is not None:
                if len(self._output_slots) < len(self._operator_outslot_names):
                    self._is_ready = False
                for setkey in self._output_slots.keys():
                    found = False
                    for opkey in self._operator_outslot_names:
                        if opkey == setkey:
                            found = True
                    if not found:
                        self._is_ready = False
                        break

        # check parameter
        if self._is_ready:
            if self._parameter is not None:
                for inparam in self._parameter.keys():
                    found = False
                    for opparam in self._essential_parameter:
                        if inparam == opparam:
                            found = True
                    if not found:
                        self._is_ready = False
                        break

        if self._is_ready:
            self.__ready__()
            if self._force_recomputation:
                self.__allocateResultMemory__()
            else:
                for osname in self.output_slots.keys():
                    if not self._dataVec.hasDumpedChannel(osname):
                        self.__allocateResultMemory__()
                        break


    def parameterNames(self):
        """
        Return names of all parameter set
        @return: <list[str]> names
        """
        return self._parameter.keys()


    def getParameter(self, name):
        """
        Get a parameter value
        @param name: <str> parameter name
        @return: parammeter value
        """
        assert isinstance(name, str), "Input slot name of wrong type!"
        assert name in self._parameter, "Parameter does not exist!"

        return self._parameter[name]


    def essentialParameter(self):
        """
        Returns a list of all essential parameter
        @return: <list[str]> names
        """
        return self._essential_parameter


    # getter of logger object
    @property
    def force_recomputation(self):
        """
        reforce computation property, if True Operator is
        executed even if input slot channel could be read
        from disc.
        """
        return self._force_recomputation


    # setter of logger object
    @force_recomputation.setter
    def force_recomputation(self, value):
        self._force_recomputation = value


    def __ready__(self):
        """
        this function is called when all slots and
        essential parameter are set and the operator is ready
        """
        pass


    def __process__(self):
        """
        This needs to be implemented in child classes
        and is called when the user calls operator.run()
        """
        pass


    def __allocateResultMemory__(self):
        """
        This function needs to be implemented in child classes
        and is called after __ready__ and when all in- and output
        slots are set.
        """
        pass




class MultiSlotSliceOperator(MultiSlotBaseOperator):

    def __init__(self, dataVec, axis):

        assert isinstance(axis, int), "Input Error, axis of wrong type!"
        assert 0 <= axis <= 2, "Input Error, axis out of range!"
        self._axis = axis

        MultiSlotBaseOperator.__init__(self, dataVec)

        self.__name__ = "MultiSlotSliceOperator"


    def __process__(self):

        indata = {}
        outdata = {}


        shape = self._input_slots_shape[self._input_slots_shape.keys()[0]]
        for key in self._input_slots_shape.keys():
            if shape != self._input_slots_shape[key]:
                assert False, "Shape Error: Slice operators need input slots of same shape!"

        try:
            # get all data channels requested from input slots
            for channel in self._input_slots.keys():
                indata[channel] = self._dataVec.getChannel(self._input_slots[channel])

            # get all output channels requested from input slots
            for channel in self._output_slots.keys():
                outdata[channel] = self._dataVec.getChannel(self._output_slots[channel])

            # get shapes of expected out slice for security check later
            slice_shapes = {}
            for out_channel in outdata.keys():
                if self._axis == 0:
                    slice_shapes[self._output_slots[out_channel]] = outdata[out_channel][0, :, :, :].shape
                elif self._axis == 1:
                    slice_shapes[self._output_slots[out_channel]] = outdata[out_channel][:, 0, :, :].shape
                elif self._axis == 2:
                    slice_shapes[self._output_slots[out_channel]] = outdata[out_channel][:, :, 0, :].shape

            # loop over input data to deliver
            # the slices orthogonal to the axis specified
            # to the __compute__ function.
            for index in range(shape[self._axis]):
                indata_slices = {}
                #outdata_slices = {}
                for in_channel in indata.keys():
                    if self._axis == 0:
                        indata_slices[in_channel] = indata[in_channel][index, :, :, :]
                    elif self._axis == 1:
                        indata_slices[in_channel] = indata[in_channel][:, index, :, :]
                    elif self._axis == 2:
                        indata_slices[in_channel] = indata[in_channel][:, :, index, :]

                # deliver data to __compute__ function. What it gets are dictionaries
                # for indata slices, outdata, slices, parameter and the current index
                # In your explicit operator implementation your can use it e.g.
                # kwargs["input_slots"]["mySlot0"] or kwargs["parameter"]["myParam"]
                # ensure that you return your results in form of a dictionary
                # which keys should be your output slot names.
                res = self.__compute__(input_slots=indata_slices,
                                       parameter=self._parameter,
                                       index=index)

                # check what you got back from __compute__
                assert isinstance(res, dict), "Seems you forgot to return your MultiSlot results as dictionary!"
                assert len(res) > 0, "Seems your MultiSlot results are empty!"
                for key in res.keys():
                    assert res[key].shape == slice_shapes[key], "Shape mismatch btw expected and delivered result slice!"

                # write result slices back into actual result container at current index
                for key in res.keys():
                    if self._axis == 0:
                        self._dataVec._data[key][index, :, :, :] = res[key][:]
                    elif self._axis == 1:
                        self._dataVec._data[key][:, index, :, :] = res[key][:]
                    elif self._axis == 2:
                        self._dataVec._data[key][:, :, index, :] = res[key][:]

        except Exception as e:
            print(e)
            print(traceback.format_exc())


    def refocus(self, img, amount=0):
        """
        this method allows to apply a affine transformation on the input
        image as relative x-axis shifts with respect to the center row.
        On epipolar plane images this is equivalent to a computational refocus
        @param img: <ndarray>
        @param amount: <float> shift amount in pixel
        @returns  <ndarray> affine transform
        """
        return refocusEpi(img, amount)


    def __compute__(self, input_slots, parameter, index):
        """
        This function needs to be implemented in child classes
        and is called when the user calls operator.run().

        Accesssing Examples:
           - input_slots["mySlot0"] -> ndarray slice from channel set to mySlot0
           - parameter["myParam"] -> value of myParam
           - index -> current slicing index
        ensure that you return your results in form of a dictionary
        which keys should be your output slot names.

        @param input_slots: <dict> containing input slots data
        @param parameter: <dict> containing your parameter slots
        @param index: <int> current slice index
        """
        return {}



class MultiSlotImgOperator(MultiSlotSliceOperator):

    def __init__(self, arrv):
        """
        This class is the basis of all slicing operators,
        means operators derived from this class can be designed
        as flexible operators able to call their __compute__
        function on 2D slices perpendicular to the axis parameter
        set. On can of course fix the axis parameter in a child
        class to build operators like ImgOperator, EpiOperator,...
        """
        MultiSlotSliceOperator.__init__(self, arrv, axis=0)

        self.__name__ = "MultiSlotImgOperator"


class MultiSlotEpiOperator(MultiSlotSliceOperator):

    def __init__(self, arrv):
        """
        This class is the basis of all slicing operators,
        means operators derived from this class can be designed
        as flexible operators able to call their __compute__
        function on 2D slices perpendicular to the axis parameter
        set. On can of course fix the axis parameter in a child
        class to build operators like ImgOperator, EpiOperator,...
        """
        MultiSlotSliceOperator.__init__(self, arrv, axis=1)

        self.__name__ = "MultiSlotEpiOperator"
