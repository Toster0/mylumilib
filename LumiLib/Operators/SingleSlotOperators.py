from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import time
try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")


import LumiLib.Tools.FocusFinder as FocusFinder
from LumiLib.Tools.refocusing import refocusEpi
from LumiLib.IO.ArrayVector import ArrayVector




class BaseOperator(object):
    def __init__(self, dataVec):
        """
        This class is the basis of all operators available.
        It has the basic functionality to set in and output
        slots and parameter slots as well. It automatically
        checks the input slots shape and provides this info
        to all children. when out and input slot are set a
        function __allocateResultMemory__ is called auto-
        matically.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        # each operator need an ArrayVector instance to operate on
        assert isinstance(dataVec, ArrayVector), "Input data object of wrong type or not set!"

        self._essential_parameter = None

        self._dataVec = dataVec
        self._parameter = {}
        self._input_slot_name = None
        self._input_slot_shape = None
        self._input_slot_dtype = None
        self._output_slot_name = None

        self._input_is_connected = False
        self._ouput_is_connected = False

        self._is_ready = False

        self._force_recomputation = True
        self.__name__ = "BaseOperator"
        self._progress_step = 0
        self._progress = 0

    def run(self):
        """
        Start the Operators processing chain
        """
        assert self._input_slot_name is not None, "No input slot set!"
        assert self._dataVec is not None, "No data object set!"
        start = time.time()
        if self._essential_parameter is not None and len(self._essential_parameter) > 0:
            assert self._parameter is not None, "No parameter set!"
            for name in self._essential_parameter:
                assert self._parameter.has_key(name), "Missing essential parameter!"

        if self._is_ready:
            print("\n--------------------------------------\napply", self.__name__, "...")

            # if computation is reforced process, else
            # process only if not output channel is already
            # dumped, otherwise load it from file
            osn = self._output_slot_name
            if self._force_recomputation:
                self.__process__()
            elif not self._dataVec.hasDumpedChannel(osn):
                self.__process__()
            else:
                self._dataVec.setVisible(osn)
                print(osn, "loaded from file")

            self._input_is_connected = False
            self._ouput_is_connected = False
            end = time.time()
            print("finished in {0:.2f} s!".format(end-start))
        else:
            assert False, "Operator has at least one empty input slots!"


    # getter of logger object
    @property
    def force_recomputation(self):
        """
        reforce computation property, if True Operator is
        executed even if input slot channel could be read
        from disc.
        """
        return self._force_recomputation

    # setter of logger object
    @force_recomputation.setter
    def force_recomputation(self, value):
        self._force_recomputation = value


    def parameterNames(self):
        """
        Return names of all parameter set
        @retval: <list[str]> names
        """
        return self._parameter.keys()


    def getParameter(self, name):
        """
        Get a parameter value
        @param name: <str> parameter name
        """
        assert isinstance(name, str), "Input slot name of wrong type!"
        assert name in self._parameter, "Parameter does not exist!"

        return self._parameter[name]


    def essentialParameter(self):
        """
        Returns a list of all essential parameter
        @retval: <list[str]> names
        """
        return self._essential_parameter


    def setInputSlot(self, name):
        """
        Set an input slot which is a ArrayVector instance and a
        channle name the operator should operate on.
        @param dataVec: <ArrayVector> instance
        @param name: <str> channel name
        """
        assert isinstance(name, str), "Input slot name of wrong type!"
        assert name in self._dataVec.getChannelNames(), "Input slot not available!"

        self._input_slot_name = name
        self._input_slot_shape = self._dataVec.shapeOf(name)
        self._input_slot_dtype = self._dataVec.dtypeOf(name)
        self._input_is_connected = True
        self.__checkIfReady__()


    def setOutputSlot(self, name):
        """
        Set the name of the output slot
        @param name: <str> output slot name
        """
        assert isinstance(name, str), "Output slot name of wrong type!"
        self._output_slot_name = name
        self._ouput_is_connected = True
        if not self._force_recomputation:
            try:
                self._dataVec.setVisible(name)
            except:
                pass
        self.__checkIfReady__()


    def setParameterSlot(self, name, value):
        """
        Set an input parameter slot
        @param name: <str> parameter name
        @param value: <obj> parameter value
        """
        assert isinstance(name, str), "Wrong parameter name input type!"
        assert name in self._essential_parameter, "Unknown parameter!"
        self._parameter[name] = value
        self.__checkIfReady__()


    def __ready__(self):
        """
        this function is called when all slots and
        essential parameter are set and the operator is ready
        """
        pass


    def __process__(self):
        """
        This needs to be implemented in child classes
        and is called when the user calls operator.run()
        """
        pass


    def __allocateResultMemory__(self):
        """
        This needs to be implemented in child classes
        and is called when in- and output slots are set
        both. Create your new data channel here using the
        infos:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        which are available when the function is called
        """
        pass


    def __checkIfReady__(self):
        """
        This method calls __allocateResultMemory__ as soon
        as in- and outputs  slots are set.
        """
        self._is_ready = True
        if not (self._input_is_connected and self._ouput_is_connected):
            self._is_ready = False
        for param in self._parameter:
            if not param in self.essentialParameter():
                self._is_ready = False
        if self._is_ready:
            self.__ready__()
            if not self._dataVec.hasChannel(self._output_slot_name):
                self.__allocateResultMemory__()









class SliceOperator(BaseOperator):

    def __init__(self, dataVec, axis=0):
        """
        This class is the basis of all slicing operators,
        means operators derived from this class can be designed
        as flexible operators able to call their __compute__
        function on 2D slices perpendicular to the axis parameter
        set. On can of course fix the axis parameter in a child
        class to build operators like ImgOperator, EpiOperator,...
        @param dataVec: <ArrayVector> input ArrayVector
        @param axis: <int> axis to operate on
        """
        BaseOperator.__init__(self, dataVec)
        self._data = None
        self._result = None
        self._dtype = None
        self._axis = axis

        self.__name__ = "Slice2D_OperatorBase"


    def refocus(self, img, amount=0):
        """
        this method allows to apply a affine transformation on the input
        image as relative x-axis shifts with respect to the center row
        @param img: <ndarray>
        @param amount: <float> shift amount in pixel
        @returns  <ndarray> affine transform
        """
        return refocusEpi(img, amount)


    def __compute__(self, img, parameter):
        """
        This needs to be implemented in child classes
        and is called when the user calls operator.run()
        """
        pass


    def __process__(self):
        """
        This method is called when operator.run() is called and loops over
        2D slices of the input slot perpendicular to the axis set. The resulting
        slices needs to be compute in the derived class __compute__ method
        and returned by this and are stored automatically in the output slot.
        """
        self._data = self._dataVec.getChannel(self._input_slot_name)

        for index in range(self._data.shape[self._axis]):
            if self._axis == 0:
                img = self._data[index, :, :, :]
            elif self._axis == 1:
                img = self._data[:, index, :, :]
            elif self._axis == 2:
                img = self._data[:, :, index, :]

            res = self.__compute__(img, self._parameter, index)

            if self._axis == 0:
                self._dataVec._data[self._output_slot_name][index, :, :, :] = res[:]
            elif self._axis == 1:
                self._dataVec._data[self._output_slot_name][:, index, :, :] = res[:]
            elif self._axis == 2:
                self._dataVec._data[self._output_slot_name][:, :, index, :] = res[:]










class ImgOperator(SliceOperator):

    def __init__(self, dataVec):
        """
        This class is the basis of all image domain slicing operators,
        means operators derived from this class can be designed
        as operators able to call their __compute__ function on the
        image domain of your volume.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        SliceOperator.__init__(self, dataVec, axis=0)
        self._data = None
        self._result = None
        self._dtype = None

        self.__name__ = "Img_OperatorBase"











class EpiOperator(SliceOperator):

    def __init__(self, dataVec):
        """
        This class is the basis of all horizontal EpiOperators,
        means operators derived from this class can be designed as operators
        able to call their __compute__ function on each horizontal epi
        in your volume. Vertical light-fields are handled by transposing
        input images while while loading. Set transposed to 1 in your ini
        file to load a transposed light-field.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        self._axis = 1

        SliceOperator.__init__(self, dataVec, axis=1)
        self._data = None
        self._result = None
        self._dtype = None

        self.__name__ = "Epi_OperatorBase"


    def setFocusshifts(self):
        """
        Set the focus shifts defined in your ini-file. If not set within
        the ini-file a routine is called to compute the focus shifts
        autonomously. Check the result if your outcome looks weird!
        """
        if self._dataVec.hasAttr("focusshifts"):
            self._focusshifts = self._dataVec.getAttr("focusshifts")
        else:
            if self._data.shape[0] < 3:
                assert False, "Not able to compute epis for less that three images in domain!"
            focusshifts = FocusFinder.focusFinder(self._data[0, :, :, :], self._data[1, :, :, :])
            self._dataVec.addAttr("focusshifts", focusshifts)
            self._focusshifts = self._dataVec.getAttr("focusshifts")

            print("\n!!!!!!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!!!")
            print("No focusshifts found, I tried to estimate them autonomously,\nin case of strange results check if my estimations", focusshifts, "are crap.")
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")




