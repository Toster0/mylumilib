from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from LumiLib.Math.Vector import Vector2

class Camera(object):

    def __init__(self):

        self._f_px = None
        self._f_mm = None
        self._pixel_size_mm = None
        self._sensor_size_mm = None
        self._resolution = (None, None)
        self._position_m = None
        self._rotation = None
        self._principal_point = (None, None)
        self._projection_matrix = None

    def setSensor(self, resolution_yx, pixel_size_mm):
        if isinstance(resolution_yx, list) or isinstance(resolution_yx, tuple):
            self._resolution = Vector2(resolution_yx[1], resolution_yx[0])
        else:
            assert False, "Input Error!"

        if isinstance(pixel_size_mm, float) or isinstance(pixel_size_mm, int):
            self._pixel_size_mm = pixel_size_mm
        else:
            assert False, "Input Error!"

    def setFocalLength(self, f_px=None, f_mm=None):
        """
        set the focal length in pixel or miliimeter, the other is computed automatically
        :param f_px: <float or int> focal length in pixel; default:None
        :param f_mm: <float or int> focal length in millimeter; default:None
        """
        self._f_mm = f_mm
        self._f_px = f_px
        if f_px is None and (isinstance(f_mm, float) or isinstance(f_mm, int)):
            self.fmm2px(f_mm)
        elif f_mm is None and (isinstance(f_px, float) or isinstance(f_px, int)):
            self.fpx2mm(f_px)
        else:
            assert False, "Input Error"

    def fmm2px(self, f_mm):
        self._f_px = (f_mm/ self._sensor_size_mm.x) * self._resolution.x

    def fpx2mm(self, f_px):
        self._f_mm = f_px / (self._resolution.x * self._sensor_size_mm.x)

if __name__ == "__main__":
    pass