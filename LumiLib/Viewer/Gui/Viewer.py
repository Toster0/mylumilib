#! /usr/bin/env python
from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""



import os
import sys

#Ui_LumiViewer = uic.loadUiType("main_UI.ui")[0]  

try:
    from OpenGL import GL
except:
    print("Seems you haven't installed PyOpenGL! -- use e.g.: pip install PyOpenGL!")
try:
    import h5py as h5
except:
    print("Seems you haven't installed h5py! -- use e.g.: pip install h5py!")
try:
    import numpy as np
except:
    print("Seems you haven't installed numpy!")
try:
    from PyQt4 import QtGui
    from PyQt4.QtCore import *
    from PyQt4.QtGui import *
    from PyQt4.QtOpenGL import *
    try:
        from PyQt4.QtCore import QString
    except ImportError:
        QString = str
except:
    print("Seems you haven't installed pyqt4!")
try:
    import qimage2ndarray
except:
    print("Seems you haven't installed qimage2ndarray! -- use e.g.: pip install qimage2ndarray!")
try:
    from glob import glob
except:
    print("Seems you haven't installed glob!")
try:
    import scipy.misc as misc
except:
    print("Seems you haven't installed scipy!")
try:
    from ConfigParser import ConfigParser
except:
    print("Seems you haven't installed ConfigParser!")


import helpers as helpers
from LumiLib.IO.ArrayVector import ArrayVector, createFrom3DSequence


try:
  _fromUtf8 = QString.fromUtf8
except AttributeError:
  _fromUtf8 = lambda s: s

from Main_UI import Ui_LumiView


CUR_PATH = os.path.dirname(os.path.abspath(__file__))


def colorRange(arr, newRange=[0.0, 255.0]):
    """
    scales data to range newRange
    @param arr: <ndarray> input data
    @param newRange: <list> new color range
    @return: <ndarray> result
    """
    assert isinstance(arr, np.ndarray), "IO Exception, ndarray expected!"
    assert isinstance(newRange, list), "IO Exception, list expected!"
    assert newRange[0] < newRange[1], "Range Exception, newRange[0] >= newRange[1]!"
    amin = float(np.amin(arr))
    amax = float(np.amax(arr))
    assert amin < amax, "Range Exception, no range to scale!"
    newRange = [float(newRange[0]),float(newRange[1])]
    
    oldRange = [amin, amax]
    oldDiff = oldRange[1] - oldRange[0]
    newDiff = newRange[1] - newRange[0]
    out = (arr.astype(np.float32) - oldRange[0]) / oldDiff * newDiff + newRange[0]
    return out


def ndarrayToPixmap(array):
    """
    converts ndarray to Qt pixmap
    @param array: <ndarray> input image
    @return: <QPixmap> result
    """
    assert isinstance(array, np.ndarray)
    if array.dtype != np.uint8:
        array = colorRange(array).astype(np.uint8)
    pic = qimage2ndarray.array2qimage(array)
    return QPixmap.fromImage(pic)



import os

class myQMainWindow(QtGui.QMainWindow):

    dragFile = pyqtSignal()

    def __init__(self, parent = None):
        super(QtGui.QMainWindow, self).__init__(parent)
        self.setAcceptDrops(True)

        self._dragPath = None

    def dragEnterEvent(self, event):
        if (event.mimeData().hasFormat('application/x-item')):
            event.acceptProposedAction()
        else:
            self.dropEvent(event)

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            path = url.toLocalFile().toLocal8Bit().data()
            if os.path.isfile(path):
                if self._dragPath is None:
                    self._dragPath = path
                    self.dragFile.emit()
        event.acceptProposedAction()
    
    

class Viewer(myQMainWindow):



    def __init__(self, arrvec=None):
        super(Viewer, self).__init__()

        self._dsets = None
        self._current_data = None
        self._current_overlay = None
        self._alpha = 0
        self._view_index = None
        self._epi_index = None
        self._horopter = None
        self._prev_horopter = 0
        self._current_view_pixMap = None
        self._current_epi_pixMap = None
        self._color_range = None
        self._overlay_color_range = None
        self._multiply_overlays = False


        self.ui = Ui_LumiView()
        self.ui.setupUi(self)
        self.setupSignals()
        self.setupDevices()


        # if data are passed through the
        # constructor register them
        if arrvec is not None:
            if isinstance(arrvec, ArrayVector):
                self._dsets = arrvec
                self.registerDsets()
            elif isinstance(arrvec, str):
                self.readFromFile(arrvec)

        self.show()


    def resetUI(self):
        """
        reset UI when new data are loaded
        """
        self._dsets = None
        self._current_data = None
        self._view_index = None
        self._epi_index = None
        self._horopter = None
        self._prev_horopter = 0
        self._current_view_pixMap = None
        self._current_epi_pixMap = None
        self._color_range = None
        self._multiply_overlays = False
        self.setupDevices()
        self.ui.attributesBrowser.clear()
        self.ui.channelComboBox.clear()
        self.ui.colorComboBox.clear()
        self.ui.mainMaxRangeSpinBox.clear()
        self.ui.mainMinRangeSpinBox.clear()



    ###########################################################################
    #####  SIGNALS SETUP
    def setupSignals(self):
        """
        setup the signal handler
        """
        # main menu
        self.ui.actionOpenH5.triggered.connect(self.openH5)
        self.ui.actionOpenSequence.triggered.connect(self.openSequence)

        # comboboxes
        self.ui.channelComboBox.activated[str].connect(self.channelChanged)
        self.ui.colorComboBox.activated[str].connect(self.colorChanged)

        # horopter
        self.ui.horopterSpinBox.valueChanged[int].connect(self.horopterChanged)

        # view slider
        self.ui.epiChangeSlider.valueChanged[int].connect(self.epiChanged)
        self.ui.viewChangeSlider.valueChanged[int].connect(self.viewChanged)

        # range
        self.ui.mainMinRangeSpinBox.valueChanged[float].connect(self.mainMinRangeChanged)
        self.ui.mainMaxRangeSpinBox.valueChanged[float].connect(self.mainMaxRangeChanged)

        # overlay
        self.connect(self.ui.overlayCheckBox, SIGNAL('stateChanged(int)'), self.enableOberlay)
        self.ui.overlayAlpha.valueChanged[int].connect(self.alphaChanged)
        self.ui.overlayChannelComboBox.activated[str].connect(self.overlayChannelChanged)
        self.ui.overlayColorComboBox.activated[str].connect(self.overlayColorChanged)
        self.connect(self.ui.overlaySwitch_btn, SIGNAL('clicked()'), self.switchOverlayType)
        self.ui.overlaySwitch_btn.setVisible(False)

        self.ui.overlayChannelComboBox.setVisible(False)
        self.ui.overlayColorComboBox.setVisible(False)
        self.ui.overlayMaxRangeSpinBox.setVisible(False)
        self.ui.overlayMinRangeSpinBox.setVisible(False)
        self.ui.overlay_range_label.setVisible(False)
        self.ui.alpha_label.setVisible(False)
        self.ui.overlayAlpha.setVisible(False)

        # save images
        self.connect(self.ui.save_view_btn, SIGNAL('clicked()'), self.saveView)
        self.connect(self.ui.save_epi_btn, SIGNAL('clicked()'), self.saveEpi)

        self.dragFile.connect(self.projectFileDragged)




    ###########################################################################
    #####  GRAPHICS

    def setupDevices(self):
        """
        setup viewports and brushes
        """
        self.ui.mainGraphicsView.setViewport(QGLWidget())
        self.ui.epiGraphicsView.setViewport(QGLWidget())

        brush = QBrush()
        brush.setColor(QColor(0, 0, 0))
        brush.setStyle(Qt.SolidPattern)

        self.ui.mainGraphicsView.setBackgroundBrush(brush)

        self.mainGraphicsScene = QGraphicsScene()
        self.epiGraphicsScene = QGraphicsScene()
        self.setDefaultScreens()


    def setDefaultScreens(self):
        """
        set initial viewports content
        """
        global CUR_PATH

        # set main graphics viewport
        self.mainGraphicsScene.clear()
        self._current_view_pixMap = QPixmap(CUR_PATH+os.sep+"util"+os.sep+"default_view.png")
        self.imageSceneItem = QGraphicsPixmapItem(self._current_view_pixMap)
        self.imageSceneItem.setTransformationMode(Qt.SmoothTransformation)
        self.mainGraphicsScene.addItem(self.imageSceneItem)
        self.ui.mainGraphicsView.setScene(self.mainGraphicsScene)
        self.ui.mainGraphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.ui.mainGraphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.ui.mainGraphicsView.show()

        # set epi graphics viewport
        self.epiGraphicsScene.clear()
        self._current_epi_pixMap = QPixmap(CUR_PATH+os.sep+"util"+os.sep+"default_epi.png")
        self.epiSceneItem = QGraphicsPixmapItem(self._current_epi_pixMap)
        self.epiSceneItem.setTransformationMode(Qt.SmoothTransformation)
        self.epiGraphicsScene.addItem(self.epiSceneItem)
        self.ui.epiGraphicsView.setScene(self.epiGraphicsScene)
        self.ui.epiGraphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.ui.epiGraphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.ui.epiGraphicsView.show()


    def renderView(self):
        """
        renders an image view
        """
        if self._current_data.shape[3] == 1:
            img = np.copy(self._current_data[self._view_index, :, :, 0])
        else:
            img = np.copy(self._current_data[self._view_index, :, :, 0:3])
        if self._color_range is not None:
            np.place(img, img > self._color_range[1], self._color_range[1])
            np.place(img, img < self._color_range[0], self._color_range[0])
        self._current_view_pixMap = ndarrayToPixmap(img)


    def renderOverlayEpi(self):
        """
        renders an epi view
        """
        # check data shape of current epi and extract QPixmap
        if self._current_data.shape[3] == 1:
            img = np.copy(self._current_data[:, self._epi_index, :, 0])
        else:
            img = np.copy(self._current_data[:, self._epi_index, :, 0:3])
        if self._color_range is not None:
            np.place(img, img > self._color_range[1], self._color_range[1])
            np.place(img, img < self._color_range[0], self._color_range[0])

        if self._current_overlay.shape[3] == 1:
            oimg = np.copy(self._current_overlay[:, self._epi_index, :, 0])
        else:
            oimg = np.copy(self._current_overlay[:, self._epi_index, :, 0:3])
        if self._overlay_color_range is not None:
            np.place(oimg, oimg > self._overlay_color_range[1], self._overlay_color_range[1])
            np.place(oimg, oimg < self._overlay_color_range[0], self._overlay_color_range[0])

        img = self.overlayImages(img, oimg)
        self._current_epi_pixMap = ndarrayToPixmap(img)


    def renderOverlayView(self):
        """
        renders an image view
        """
        if self._current_data.shape[3] == 1:
            img = np.copy(self._current_data[self._view_index, :, :, 0])
        else:
            img = np.copy(self._current_data[self._view_index, :, :, 0:3])
        if self._color_range is not None:
            np.place(img, img > self._color_range[1], self._color_range[1])
            np.place(img, img < self._color_range[0], self._color_range[0])

        if self._current_overlay.shape[3] == 1:
            oimg = np.copy(self._current_overlay[self._view_index, :, :, 0])
        else:
            oimg = np.copy(self._current_overlay[self._view_index, :, :, 0:3])
        if self._overlay_color_range is not None:
            np.place(oimg, oimg > self._overlay_color_range[1], self._overlay_color_range[1])
            np.place(oimg, oimg < self._overlay_color_range[0], self._overlay_color_range[0])

        img = self.overlayImages(img, oimg)
        self._current_view_pixMap = ndarrayToPixmap(img)


    def renderEpi(self):
        """
        renders an epi view
        """
        # check data shape of current epi and extract QPixmap
        if self._current_data.shape[3] == 1:
            img = np.copy(self._current_data[:, self._epi_index, :, 0])
        else:
            img = np.copy(self._current_data[:, self._epi_index, :, 0:3])
        if self._color_range is not None:
            np.place(img, img > self._color_range[1], self._color_range[1])
            np.place(img, img < self._color_range[0], self._color_range[0])
        self._current_epi_pixMap = ndarrayToPixmap(img)


    def renderImages(self):
        """
        renders images, view ans epi and calls renderDevices afterwards
        """
        if self._current_overlay is None:
            self.renderView()
            self.renderEpi()
        else:
            self.renderOverlayView()
            self.renderOverlayEpi()
        self.renderDevices()


    def overlayImages(self, img1, img2):
        if self._color_range[0] < 0 or self._color_range[1] > 255 or self._color_range[1] - self._color_range[0] < 25:
            img1 = colorRange(img1)
        if self._overlay_color_range[0] < 0 or self._overlay_color_range[1] > 255 or self._overlay_color_range[1] - self._overlay_color_range[0] < 25:
            if not self._multiply_overlays:
                img2 = colorRange(img2)
            else:
                img2 = colorRange(img2, newRange=[0, 1])

        img = None
        if len(img1.shape) == 2 and len(img2.shape) == 2:
            img = np.zeros((img1.shape[0], img1.shape[1]), dtype=np.uint8)
            if not self._multiply_overlays:
                img[:, :] = img1[:, :]*(1.0 - self._alpha) + img2[:, :]*self._alpha
            else:
                img[:, :] = img1[:, :] * img2[:, :]
        elif len(img1.shape) == 2 and len(img2.shape) == 3:
            img = np.zeros((img1.shape[0], img1.shape[1], 3), dtype=np.uint8)
            for c in range(3):
                if not self._multiply_overlays:
                    img[:, :, c] = img1[:, :]*(1.0 - self._alpha) + img2[:, :, c]*self._alpha
                else:
                    img[:, :, c] = img1[:, :] * img2[:, :, c]
        elif len(img1.shape) == 3 and len(img2.shape) == 2:
            img = np.zeros((img1.shape[0], img1.shape[1], 3), dtype=np.uint8)
            for c in range(3):
                if not self._multiply_overlays:
                    img[:, :, c] = img1[:, :, c]*(1.0 - self._alpha) + img2[:, :]*self._alpha
                else:
                    img[:, :, c] = img1[:, :, c] * img2[:, :]
        elif len(img1.shape) == 3 and len(img2.shape) == 2:
            img = np.zeros((img1.shape[0], img1.shape[1], 3), dtype=np.uint8)
            for c in range(3):
                if not self._multiply_overlays:
                    img[:, :, c] = img1[:, :, c]*(1.0 - self._alpha) + img2[:, :, c]*self._alpha
                else:
                    img[:, :, c] = img1[:, :, c] * img2[:, :, c]
        return img


    def renderDevices(self):
        """
        render viewports
        """
        if self._current_data is not None:

            self._current_view_pixMap = self._current_view_pixMap.scaled(self.ui.mainGraphicsView.size(), Qt.IgnoreAspectRatio, Qt.FastTransformation)

            # clear and set main viewport
            self.mainGraphicsScene.clear()
            self.mainSceneItem = QGraphicsPixmapItem(self._current_view_pixMap)
            self.mainSceneItem.setTransformationMode(Qt.SmoothTransformation)
            self.mainGraphicsScene.addItem(self.mainSceneItem)
            self.ui.mainGraphicsView.setScene(self.mainGraphicsScene)

            self._current_epi_pixMap = self._current_epi_pixMap.scaled(self.ui.epiGraphicsView.size(), Qt.IgnoreAspectRatio, Qt.FastTransformation)

            # clear and set epi viewport
            self.epiGraphicsScene.clear()
            self.epiSceneItem = QGraphicsPixmapItem(self._current_epi_pixMap)
            self.epiGraphicsScene.addItem(self.epiSceneItem)
            self.ui.epiGraphicsView.setScene(self.epiGraphicsScene)

            # draw the viewport lines
            self.drawLines()


    def resizeEvent(self, event):
        """
        Handle the resize event.
        """
        self.renderDevices()


    def drawLines(self):
        """
        draw the epi and position orientation lines
        """
        # draw epi line
        size = self.ui.mainGraphicsView.size()
        pen = QPen()
        brush = QBrush()
        brush.setColor(QColor(255, 0, 0, 255))
        brush.setStyle(Qt.SolidPattern)
        pen.setBrush(brush)
        rel_py = int(self._epi_index/float(self._current_data.shape[1])*size.height())
        self.mainGraphicsScene.addLine(0, rel_py, size.width(), rel_py, pen)

        # draw position line
        size = self.ui.epiGraphicsView.size()
        pen = QPen()
        brush = QBrush()
        brush.setColor(QColor(0, 255, 0, 255))
        brush.setStyle(Qt.SolidPattern)
        pen.setBrush(brush)
        rel_py = int(self._view_index/float(self._current_data.shape[0])*size.height())
        self.epiGraphicsScene.addLine(0, rel_py, size.width(), rel_py, pen)












    ###########################################################################
    #####  SIGNAL HANDLER

    def switchOverlayType(self):
        self._multiply_overlays = not self._multiply_overlays
        if self._multiply_overlays:
            self._alpha = 0
            self.ui.alpha_label.setVisible(False)
            self.ui.overlayAlpha.setVisible(False)
            self.ui.overlayAlpha.setValue(0)
            self.ui.overlaySwitch_btn.setText("Switch to Alpha Overlay")
        else:
            self.ui.alpha_label.setVisible(True)
            self.ui.overlayAlpha.setVisible(True)
            self.ui.overlaySwitch_btn.setText("Switch to Multiply Overlay")
        self.renderImages()


    def saveView(self):
        import cStringIO as StringIO
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save View', '', selectedFilter='*.png')
        if filename:
            filename = str(filename)
            if not filename.endswith(".png"):
                filename += ".png"
            byte_array = QByteArray()
            buffer = QBuffer(byte_array)
            buffer.open(QIODevice.WriteOnly)
            self._current_view_pixMap.save(buffer, 'PNG')
            string_io = StringIO.StringIO(byte_array)
            string_io.seek(0)
            with open(filename, 'wb') as out_file:
                out_file.write(string_io.read())

    def saveEpi(self):
        import cStringIO as StringIO
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save Epi', '', selectedFilter='*.png')
        if filename:
            filename = str(filename)
            if not filename.endswith(".png"):
                filename += ".png"
            byte_array = QByteArray()
            buffer = QBuffer(byte_array)
            buffer.open(QIODevice.WriteOnly)
            self._current_epi_pixMap.save(buffer, 'PNG')
            string_io = StringIO.StringIO(byte_array)
            string_io.seek(0)
            with open(filename, 'wb') as out_file:
                out_file.write(string_io.read())

    def epiChanged(self, value):
        """
        handle epi slider change
        """
        self._epi_index = value
        if self._current_overlay is None:
            self.renderEpi()
        else:
            self.renderOverlayEpi()
        self.renderDevices()


    def viewChanged(self, value):
        """
        handle view slider change
        """
        self._view_index = value
        if self._current_overlay is None:
            self.renderView()
        else:
            self.renderOverlayView()
        self.renderDevices()

    def alphaChanged(self, value):
        self._alpha = value/100.0
        self.renderImages()


    def enableOberlay(self, state):
        if state == 0:
            self.ui.overlayChannelComboBox.setVisible(False)
            self.ui.overlayColorComboBox.setVisible(False)
            self.ui.overlayMaxRangeSpinBox.setVisible(False)
            self.ui.overlayMinRangeSpinBox.setVisible(False)
            self.ui.overlay_range_label.setVisible(False)
            self.ui.alpha_label.setVisible(False)
            self.ui.overlayAlpha.setVisible(False)
            self.ui.overlayChannelComboBox.clear()
            self.ui.overlayColorComboBox.clear()
            self.ui.overlayMaxRangeSpinBox.clear()
            self.ui.overlayMinRangeSpinBox.clear()
            self.ui.overlayAlpha.setValue(0)
            self._alpha = 0
            self._current_overlay = None
            self.ui.overlayCheckBox.setCheckState(False)
            self.ui.overlaySwitch_btn.setVisible(False)
        else:
            self.ui.overlayChannelComboBox.setVisible(True)
            self.ui.overlayColorComboBox.setVisible(True)
            self.ui.overlayMaxRangeSpinBox.setVisible(True)
            self.ui.overlayMinRangeSpinBox.setVisible(True)
            self.ui.overlay_range_label.setVisible(True)
            self.ui.alpha_label.setVisible(True)
            self.ui.overlayAlpha.setVisible(True)
            self.ui.overlaySwitch_btn.setVisible(True)

            for channel in self._dsets.getDumpedChannelNames():
                if channel != self.ui.channelComboBox.currentText():
                    self.ui.overlayChannelComboBox.addItem(channel)
            self.ui.overlayChannelComboBox.addItem("")
            index = self.ui.overlayChannelComboBox.findText("")
            if index != -1:
                self.ui.overlayChannelComboBox.setCurrentIndex(index)


    def overlayChannelChanged(self, channel):
        channel = str(channel)
        if channel != '':
            if self._dsets is not None:
                # clear channel combobox and fill with new channels available
                self.ui.overlayColorComboBox.clear()
                for c in range(self._dsets.shapeOf(channel)[3]):
                    self.ui.overlayColorComboBox.addItem(str(c))
                if self._dsets.shapeOf(channel)[3] == 3:
                    self.ui.overlayColorComboBox.addItem("rgb")

                # find index of rgb if available and set it
                index = self.ui.overlayColorComboBox.findText("rgb")
                if index != -1:
                    self.ui.overlayColorComboBox.setCurrentIndex(index)
                else:
                    # if no rgb available set 0 as current channel
                    index = self.ui.overlayColorComboBox.findText('0')
                    if index != -1:
                        self.ui.overlayColorComboBox.setCurrentIndex(index)

                # call channel change and refresh scene
                if self.ui.overlayColorComboBox.currentText() != '':
                    self.overlayColorChanged(self.ui.overlayColorComboBox.currentText())
                    self.renderImages()

            # self._horopter = 0
            # self.ui.horopterSpinBox.setValue(0)


    def overlayColorChanged(self, color):
        """
        handle channel change request
        """
        if self._dsets is not None:
            if color is None:
                color = self.ui.colorComboBox.currentText()
            color = str(color)
            if color != '':
                # get current data set
                channel = str(self.ui.overlayChannelComboBox.currentText())

                # set data rgb or single channel
                if color == "rgb":
                    self._current_overlay = np.copy(self._dsets.getChannel(channel)[:, :, :, 0:3])
                else:
                    self._current_overlay = np.zeros((self._dsets.shapeOf(channel)[0],
                                                  self._dsets.shapeOf(channel)[1],
                                                  self._dsets.shapeOf(channel)[2], 1),
                                                  dtype=self._dsets.dtypeOf(channel))
                    self._current_overlay[:, :, :, 0] = np.copy(self._dsets.getChannel(channel)[:, :, :, int(color)])

                # data are copied so we can dump the channel again
                self._dsets.setVisible(channel, False)

                # get color range
                self._overlay_color_range = [np.amin(self._current_overlay), np.amax(self._current_overlay)]

                if self._horopter is None:
                    self._horopter = 0

                self.changeHoropter(self._horopter, slot=1)
                self.refreshUI()
                self.renderImages()


    def channelChanged(self, channel):
        """
        handle dataset change request
        """
        if self._dsets is not None:
            channel = str(channel)
            if channel != '':
                # if channel is not available set visible
                if not self._dsets.hasChannel(channel):
                    try:
                        self._dsets.setVisible(channel)
                        # set all other channels loaded to invisible
                        for name in self._dsets.getChannelNames():
                            if name != channel:
                                self._dsets.setVisible(name, False)
                    except:
                        assert False, "Failed to load dumped channel!"

                # clear channel combobox and fill with new channels available
                self.ui.colorComboBox.clear()
                for c in range(self._dsets.shapeOf(channel)[3]):
                    self.ui.colorComboBox.addItem(str(c))
                if self._dsets.shapeOf(channel)[3] == 3:
                    self.ui.colorComboBox.addItem("rgb")

                # find index of rgb if available and set it
                index = self.ui.colorComboBox.findText("rgb")
                if index != -1:
                    self.ui.colorComboBox.setCurrentIndex(index)
                else:
                    # if no rgb available set 0 as current channel
                    index = self.ui.colorComboBox.findText('0')
                    if index != -1:
                        self.ui.colorComboBox.setCurrentIndex(index)

                if self._current_overlay is not None:
                    self.enableOberlay(False)

                # call channel change and refresh scene
                if self.ui.colorComboBox.currentText() != '':
                    self.colorChanged(self.ui.colorComboBox.currentText())
                    self.renderImages()


    def colorChanged(self, color):
        """
        handle channel change request
        """
        if self._dsets is not None:
            if color is None:
                color = self.ui.colorComboBox.currentText()
            color = str(color)
            if color != '':
                # get current data set
                channel = str(self.ui.channelComboBox.currentText())

                # set data rgb or single channel
                if color == "rgb":
                    self._current_data = np.copy(self._dsets.getChannel(channel)[:, :, :, 0:3])
                else:
                    self._current_data = np.zeros((self._dsets.shapeOf(channel)[0],
                                                  self._dsets.shapeOf(channel)[1],
                                                  self._dsets.shapeOf(channel)[2], 1),
                                                  dtype=self._dsets.dtypeOf(channel))
                    self._current_data[:, :, :, 0] = np.copy(self._dsets.getChannel(channel)[:, :, :, int(color)])

                # data are copied so we can dump the channel again
                self._dsets.setVisible(channel, False)

                # get color range
                self._color_range = [np.amin(self._current_data), np.amax(self._current_data)]

                # if view index and epi index not set so far initialize them
                if self._view_index is None:
                    self._view_index = self._current_data.shape[0]/2
                if self._epi_index is None:
                    self._epi_index = self._current_data.shape[1]/2
                if self._horopter is None:
                    self._horopter = 0

                self.changeHoropter(self._horopter, slot=0)
                self.refreshUI()


    def horopterChanged(self, value):
        """
        handles request of horopter change
        """
        self._horopter = value
        self.changeHoropter(value - self._prev_horopter, slot=-1)
        self._prev_horopter = value


    def changeHoropter(self, value, slot=0):
        """
        changes the horopter
        """
        shift = value
        # shift data if available
        if self._current_data is not None and (slot == 0 or slot == -1):
            for n in range(self._current_data.shape[0]):
                for c in range(self._current_data.shape[3]):
                    self._current_data[n, :, :, c] = np.roll(self._current_data[n, :, :, c], (n-self._current_data.shape[0]/2)*shift, axis=1)

        if self._current_overlay is not None and (slot == 1 or slot == -1):
            for n in range(self._current_overlay.shape[0]):
                for c in range(self._current_overlay.shape[3]):
                    self._current_overlay[n, :, :, c] = np.roll(self._current_overlay[n, :, :, c], (n-self._current_overlay.shape[0]/2)*shift, axis=1)

        # rerender scene
        self.renderImages()


    def mainMinRangeChanged(self, value):
        """
        handles request of range change
        """
        self._color_range[0] = value
        self.renderImages()


    def mainMaxRangeChanged(self, value):
        """
        handles request of range change
        """
        self._color_range[1] = value
        self.renderImages()


    def refreshUI(self):
        """
        refresh UI if new data set is loaded
        """
        self.ui.viewChangeSlider.setValue(self._view_index)
        self.ui.viewChangeSlider.setMinimum(0)
        self.ui.viewChangeSlider.setMaximum(self._current_data.shape[0]-1)

        self.ui.epiChangeSlider.setValue(self._epi_index)
        self.ui.epiChangeSlider.setMinimum(0)
        self.ui.epiChangeSlider.setMaximum(self._current_data.shape[1]-1)

        self.ui.mainMinRangeSpinBox.setValue(float(self._color_range[0]))
        self.ui.mainMaxRangeSpinBox.setValue(float(self._color_range[1]))

        if self._current_overlay is not None:
            self.ui.overlayMinRangeSpinBox.setValue(float(self._overlay_color_range[0]))
            self.ui.overlayMaxRangeSpinBox.setValue(float(self._overlay_color_range[1]))

        # self._horopter = 0
        # self.ui.horopterSpinBox.setValue(0)






    ###########################################################################
    #####  DATA IO

    def registerDsets(self):
        """
        Prepare data input
        """
        if not self._dsets.setAllVisible(False):
            print("set all invisble failed!")
        else:
            # clear and refill comboboxes
            self.ui.channelComboBox.clear()
            self.ui.colorComboBox.clear()

            # set channel names
            for name in self._dsets.getChannelNames():
                self.ui.channelComboBox.addItem(name)
            for name in self._dsets.getDumpedChannelNames():
                index = self.ui.channelComboBox.findText(name, Qt.MatchFixedString)
                if index == -1:
                    self.ui.channelComboBox.addItem(name)

            #self.ui.channelComboBox.addItem('')
            self.setChannelComboboxItem()
            self.setColorComboboxItem()
            self.channelChanged(self.ui.channelComboBox.currentText())

            # add all attributes to textbrowser
            attrs = self._dsets.getAttr()
            text = ""
            for n, name in enumerate(attrs[0]):
                text += name + " : " + str(attrs[1][n]) + "\n"
                text += "---------------------------------------------" + "\n"
            self.ui.attributesBrowser.append(text)


    def setChannelComboboxItem(self, key=''):
        """
        set a combobox item
        """
        index = self.ui.channelComboBox.findText(key, Qt.MatchFixedString)
        if index >= 0:
             self.ui.channelComboBox.setCurrentIndex(index)


    def setColorComboboxItem(self, key=''):
        """
        set a combobox item
        """
        index = self.ui.colorComboBox.findText(key, Qt.MatchFixedString)
        if index >= 0:
             self.ui.colorComboBox.setCurrentIndex(index)


    def readFromFile(self, filename):
        assert isinstance(filename, str), "Wrong input type!"

        self.resetUI()

        if not filename.endswith(".h5"):
            filename += ".h5"
        if os.path.exists(filename):
            try:
                filename, filetype, path = helpers.splitPath(filename)
                self._dsets = ArrayVector()
                if not path.endswith(os.sep):
                    path += os.sep
                self._dsets.load(path+filename+"."+filetype)
                self.registerDsets()
            except:
                assert False, "Something went wrong preparing your data!"


    def readImageSequence(self, directory):
        self.resetUI()
        directory = str(directory)
        if not directory.endswith(os.sep):
            directory += os.sep
        self._dsets = createFrom3DSequence(dpath=directory, name="sequence", dump_filename=directory+"project.h5")
        self.registerDsets()


    def projectFileDragged(self):
        try:
            print("try to open:", self._dragPath)
            self.readFromFile(self._dragPath)
            self._dragPath = None
        except:
            pass


    def openH5(self):
        """
        open a hdf5 file
        """
        fname, ext = QFileDialog.getOpenFileNamesAndFilter(self, 'Open HDF5', "./", "(*.h5)")
        try:
            self.readFromFile(str(fname[0]))
        except:
            pass


    def openSequence(self):
        directory = QtGui.QFileDialog.getExistingDirectory(None, 'Select a folder:', "./", QtGui.QFileDialog.ShowDirsOnly)
        print()
        if str(directory) != '':
            self.readImageSequence(directory)



    
###########################################################################
#####  RUN VIEWER ROUTINES

def showViewer(arrv=None):
    """
    This method can be used to open the viewer from code or interactive
    @param arrv: <ArrayVector> input [None]
    """
    app = QtGui.QApplication(sys.argv)
    main(arrv)


def main(arrvec=None):
    """
    Open the Viewer
    @param arrv: <ArrayVector> input [None]
    """
    window = Viewer(arrvec)
    return qApp.exec_()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    if len(sys.argv) == 2:
        sys.exit(main(sys.argv[1]))
    else:
        sys.exit(main())


