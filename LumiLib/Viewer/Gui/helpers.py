from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
try:
    import scipy.misc as misc
except ImportError:
    print("seems you haven't installed scipy!")
try:
    import glob
except ImportError:
    print("seems you haven't installed glob!")
try:
    import h5py as h5
except ImportError:
    print("Seems you haven't installed h5py!")
try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")


def loadFileSequence(path, pattern=None, ftype="png"):
    if not path.endswith(os.sep):
        path += os.sep
    if not ftype.startswith("."):
        ftype = "." + ftype
    fnames = []
    for f in glob.glob(path+"*"+ftype):
        if pattern is not None:
            if os.path.basename(f).startswith(pattern):
                fnames.append(f)
        else:
            fnames.append(f)
    if len(fnames) == 0:
        return None

    fnames.sort()
    im = misc.imread(fnames[0])
    imgs = np.zeros((len(fnames), im.shape[0], im.shape[1], 3), dtype=np.uint8)
    for n, f in enumerate(fnames):
        im = imread(fnames[n])
        if len(im.shape) == 2:
            for c in range(3):
                imgs[n, :, :, c] = im[:]
        elif len(im.shape) == 3:
            imgs[n, :, :, :] = im[:, :, 0:3]
    return imgs

def splitPath(s):
  """
  @author: Sven Wanner  
  @summary: takes an absolute path and returns the filename as first, the 
  filetype as second and the location of the file as third result
  @param s: path [str]
  @return data,filetype,path
  """
  path = s
  while s.find(os.sep) != -1:
      res = s.split(os.sep)
      s = res[2]
  data = res[len(res)-1]
  filetype = data.partition(".")[2]
  data = data.partition(".")[0]
  return data, filetype, path.partition(data)[0]


def changeKey(d, old_key, new_key):
  """
  @author: Sven Wanner  
  @summary: change the key name of a dictionary key
  @param old_key: old key name [str]
  @param new_key: new key name [str]  
  """
  if type(d) is dict:
    d[new_key] = d.pop(old_key)
  else:
    print("WARNING!!! Error in changeKey!")
    
    
def ensure_dir_from_current(f):
  """
  @author: Sven Wanner  
  @summary: checks if input dir exist relative to the calling module location, if not it will be created and returned 
  @param f: directory [str]
  @return d: path [str]
  """
  cwd = os.getcwd()
  d = os.path.dirname(cwd+f)
  if not os.path.exists(d):
    os.makedirs(d)
  return d
  
def ensure_dir(f):
  """
  @author: Sven Wanner  
  @summary: checks if input dir exist, if not it will be created and returned 
  @param f: directory [str]
  @return d: path [str]
  """  
  d = os.path.dirname(f)
  if not os.path.exists(d):
    os.makedirs(d)
  return d
  
  
def createH5(datasets=[],attrs=[],location="./",name="file.h5"):
  """
  @author: Sven Wanner 
  @summary: creates a h5 file from input datasets and attributes
  @param dataset: list of dictionaries [list] -> [{"name":"dataset_name","data":ndarray,:"dtype":dtype}, ... ]
  @param attrs: list of dictionaries [list] -> [{"name":attrs_name,"value":value}, ...]
  @param location: save location [str]
  @param name: save name [str]
  """
  if location is not None:
    if location[-1] != os.sep:
      print("add a slash to location!")
      location += os.sep
    print("location in createH5", location)
    loc = ensure_dir(location)+os.sep
    if name.find(".h5") == -1:
      name+=".h5"
    
    location = loc+name
    print("try to write h5 file at: loc =",location+name,"results in location", location)
    
    f = h5.File(location, 'w')
    for d in datasets:
      f.create_dataset(d["name"], data=d["data"], dtype=d["dtype"], compression='gzip')
    for a in attrs:
      f.attrs[a["name"]] = a["value"]
    f.close()
    

def readH5(infile):
  """
  @author: Sven Wanner 
  @summary: reads a h5 file from input file string and returns lists of datasets and attributes
  @param dataset: list of dictionaries [list] -> [{"name":"dataset_name","data":ndarray,:"dtype":dtype}, ... ]
  @param attrs: list of dictionaries [list] -> [{"name":attrs_name,"value":value}, ...]
  @return dsets,ats: dsets [dictionary] of ndarrays {"name":data,....}; ats [dictionary] of attributes {"name":value,....}
  """
  f = h5.File(infile, 'r')
  datasets = list(f)
  attrs = list(f.attrs)
  
  dsets = {}
  ats = {}
  for d in datasets:
    dsets[str(d)] = np.copy(f[str(d)])
  for a in attrs:
    ats[str(a)] = f.attrs[str(a)]
    
  return dsets,ats
  
  
  
def aopen(infile, string, line=0, out = None):
  """
  @author: Sven Wanner 
  @summary: opens a file and appends some content without deleting the former content. 
  @param infile: filename [str] 
  @param line: position of content to append [int]
  @param string: content to append [string]
  @param out: outfilename [str], if None, the infilename is chosen
  """
  try:
    f = open(infile, 'r').readlines()
    a = f[:line]
    b = f[line:]
    a.append(string)
    if out is None:
      out = infile
      open(out, 'w').write(''.join(a + b))
  except Exception as e:
    print("\nException in aopen!")
    print(e, "\n")
  
  
  
    
    
