from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import pylab as plt
except ImportError:
    print("Seems you haven't installed matplotlib!")


from LumiLib.IO.ArrayVector import ArrayVector



class ImageViewer(object):

    def __init__(self):
        """
        Simple image viewer
        """
        self.img = None
        self.text = ""


    def imshow(self, img=None, text="", cmap="gray"):
        """
        shows an image (ndarray)
        @param img: <ndarray>
        @param text: <str> title text
        """
        assert isinstance(cmap, str), "Unknown type"
        self.cmap = cmap

        if img is not None:
            self.__setImage__(img)
        else:
            print("No useful data set! Please set an image!")
        self.__setText__(text)
        self.__display__()


    def arrayvectorShow(self, arrv=None, name=None, axis=0, index=0, color_index=None, text="", cmap="gray"):
        """
        show an ArrayVector slice
        @param arrv: <ArrayVector> instance
        @param name: <str> channel name
        @param text: <str> title text
        """
        assert isinstance(cmap, str), "Unknown type"
        self.cmap = cmap

        if arrv is not None and name is not None:
            self.__setArrv__(arrv, name, axis, index, color_index)
        else:
            print("No useful data set! Please pass an ArrayVector instance plus the channel name!")
        self.__setText__(text)
        self.__display__()


    def __setText__(self, text):
        """
        Set title text
        @param text: <str> title text
        """
        assert isinstance(text, str)
        self.text = text


    def __setCmap__(self):
        """
        Set colormap
        """
        if self.cmap == "gray":
            return plt.cm.gray
        elif self.cmap == "jet":
            return plt.cm.jet
        elif self.cmap == "hot":
            return plt.cm.hot


    def __setImage__(self, img):
        """
        Set an image
        @param img: <ndarray> input image
        """
        assert isinstance(img, np.ndarray)
        if len(img.shape) == 2:
            pass
        elif len(img.shape) == 3 and img.shape[2] == 1:
            img = img[:, :, 0]
        elif img.shape[2] == 3:
            pass
        else:
            assert False, "Unknown image type!"
        self.img = img


    def __setArrv__(self, arrv, name, axis, index, color_index=None, cmap="gray"):
        """
        Set an ArrayVector instance and extract 2D lice depending on parameter set
        @param arrv: <ArrayVector> ArrayVector instance
        @param name: <str> channel name
        @param axis: <int> extract 2D slice orthogonal to axis
        @param index: <int> position on axis
        @param color_index: <int> color channel [None]
        @param cmap: <str> colormap tag ["gray"]
        """
        assert isinstance(arrv, ArrayVector), "Unknown type!"
        assert isinstance(name, str), "Unknown type!"
        assert isinstance(axis, int), "Unknown type!"
        assert axis >= 0 and axis <= 2, "Axis not available!"
        assert isinstance(index, int), "Unknown type!"

        img = arrv.getSlice(name, axis, index)
        print(img.shape)
        if color_index is None:
            if img.shape[2] == 1 or img.shape[2] == 3:
                pass
            else:
                assert False, "Only data of shapes (:,:,:,1), (:,:,:,3) or (:,:,:,color_index) can be displayed!"
        else:
            img = img[:, :, color_index]

        self.__setImage__(img)


    def __display__(self):
        """
        Display routine
        """
        plt.imshow(self.img, cmap=self.__setCmap__())
        title = "shape=" + str(self.img.shape)
        title += " range=(" + str(np.amin(self.img)) + "," + str(np.amax(self.img)) + ")"
        title += " " + self.text
        plt.title(title)
        plt.show()
