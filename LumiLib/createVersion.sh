#/bin/bash

file="./__version__.py"
line=$(cat $file)        #the output of 'cat $file' is assigned to the $name variable
#echo $line               #test

INPUT=$line
tmp=`echo $INPUT | cut -d'=' -f 2`
version=`echo $tmp | cut -d"'" -f 2`
echo "version is "$version

#echo version;

pdir=./../LumiLib_versions/LumiLib_"$version"
fdir=$pdir"/LumiLib"

rm -r $pdir

mkdir $pdir
mkdir $fdir

find . -name "*.pyc" -exec rm -rf {} \;
find . -name "*~" -exec rm -rf {} \;

cp -r Common $fdir/Common
cp -r CustomWorkflows $fdir/CustomWorkflows
cp -r CustomOperators $fdir/CustomOperators
cp -r CustomVectorProcessors $fdir/CustomVectorProcessors
cp -r Executables $fdir/Executables
cp -r DemoOperators $fdir/DemoOperators
cp -r IO $fdir/IO
cp -r Operators $fdir/Operators
cp -r Tests $fdir/Tests
cp -r Tools $fdir/Tools
cp -r Viewer $fdir/Viewer
cp -r StreamEngine $fdir/StreamEngine
cp Doxyfile $fdir/Doxyfile
cp __init__.py $fdir/__init__.py
cp license $fdir/license
cp ./../README.md $fdir/../README.md
cp globals.py $fdir/globals.py
cp __version__.py $fdir/__version__.py
cp setup.sh $fdir/setup.sh
cp loadCustomModules.py $fdir/loadCustomModules.py
cp install_dependencies.sh $fdir/install_dependencies.sh

echo "Finished! Created "$fdir

