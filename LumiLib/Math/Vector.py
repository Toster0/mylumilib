from __future__ import print_function
from __future__ import  division

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import numpy as np

class Vector(object):
    def __init__(self):
        """
        Vector Baseclass implementing general vector functions
        """
        self.data = None

    def __str__(self):
        s = "("
        for i in range(self.data.shape[0]):
            s += str(self.data[i])
            if i == self.data.shape[0]-1:
                s += ")"
            else:
                s += ", "
        return s

    def __abs__(self):
        return np.sqrt(np.sum(self.data[:]**2))

    def __eq__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        for n in range(other.data.shape[0]):
            if self.data[n] != other.data[n]:
                print(self.data[n], other.data[n])
                return False
        return True

    def __ne__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        for n in range(other.data.shape[0]):
            if self.data[n] != other.data[n]:
                return True
        return False

    def __lt__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        return abs(self) < abs(other)

    def __le__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        return abs(self) <= abs(other)

    def __gt__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        return abs(self) > abs(other)

    def __ge__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same sized vectors!"
        return abs(self) >= abs(other)

    @staticmethod
    def Distance(vec1, vec2):
        return Vector.Magnitude(vec1-vec2)

    @staticmethod
    def Magnitude(vec):
        return vec.magnitude()

    @staticmethod
    def DotProduct(vec1, vec2):
        return np.sum(vec1.data[:] * vec2.data[:])

    def magnitude(self):
        return np.sqrt(np.sum(self.data**2))

    def getAsMat(self):
        return np.mat(np.copy(self.data))


class Vector2(Vector):

    def __init__(self, x=0, y=0, data=None):
        """
        Vector2 can be created using args x,y or arg data.
        Arg data can be a flat ndarray of length 2
        """
        Vector.__init__(self)

        if data is None:
            self.data = np.array([x, y], dtype=np.float32)
        elif isinstance(data, np.ndarray) and len(data.shape) == 1 and data.shape[0] == 2:
            self.data = data.astype(np.float32)
        else:
            assert False, "Unhandled Data Input!"

        self.x = self.data[0]
        self.y = self.data[1]

    def __add__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector2(data=self.data[:]+other.data[:])

    def __sub__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector2(data=self.data[:]-other.data[:])

    def __mul__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector2(data=self.data[:]*other.data[:])

    def Set(self, x, y):
        self.data[0] = x
        self.data[1] = y
        self.x = x
        self.y = y

    def normalize(self):
        norm = self.magnitude()
        return Vector2(x=self.x/norm, y=self.y/norm)


class Vector3(Vector):

    def __init__(self, x=0, y=0, z=0, data=None):
        """
        Vector3 can be created using args x,y,z or arg data.
        Arg data can be a flat ndarray of length 3
        """
        Vector.__init__(self)

        if data is None:
            self.data = np.array([x, y, z], dtype=np.float32)
        elif isinstance(data, np.ndarray) and len(data.shape) == 1 and data.shape[0] == 3:
            self.data = data.astype(np.float32)
        else:
            assert False, "Unhandled Data Input!"

        self.x = self.data[0]
        self.y = self.data[1]
        self.z = self.data[2]

    def __add__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector3(data=self.data[:]+other.data[:])

    def __sub__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector3(data=self.data[:]-other.data[:])

    def __mul__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector3(data=self.data[:]*other.data[:])

    @staticmethod
    def CrossProduct(v1, v2):
        return Vector3(x=v1.y*v2.z - v1.z*v2.y, y=v1.z*v2.x - v1.x*v2.z, z=v1.x*v2.y - v1.y*v2.x)

    def Set(self, x, y, z):
        self.data[0] = x
        self.data[1] = y
        self.data[2] = z
        self.x = x
        self.y = y
        self.z = z

    def normalize(self):
        norm = self.magnitude()
        return Vector3(x=self.x/norm, y=self.y/norm, z=self.z/norm)


class Vector4(Vector):

    def __init__(self, x=0, y=0, z=0, w=0, data=None):
        """
        Vector4 can be created using args x,y,z,w or arg data.
        Arg data can be a flat ndarray of length 4
        """
        Vector.__init__(self)

        if data is None:
            self.data = np.array([x, y, z, w], dtype=np.float32)
        elif isinstance(data, np.ndarray) and len(data.shape) == 1 and data.shape[0] == 4:
            self.data = data.astype(np.float32)
        else:
            assert False, "Unhandled Data Input!"

        self.x = self.data[0]
        self.y = self.data[1]
        self.z = self.data[2]
        self.w = self.data[3]

    def __add__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector4(data=self.data[:]+other.data[:])

    def __sub__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector4(data=self.data[:]-other.data[:])

    def __mul__(self, other):
        assert self.data.shape == other.data.shape, "Operator only works on same size vectors!"
        return Vector4(data=self.data[:]*other.data[:])

    def Set(self, x, y, z, w):
        self.data[0] = x
        self.data[1] = y
        self.data[2] = z
        self.data[3] = w
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def normalize(self):
        norm = self.magnitude()
        return Vector4(x=self.x/norm, y=self.y/norm, z=self.z/norm, w=self.w/norm)