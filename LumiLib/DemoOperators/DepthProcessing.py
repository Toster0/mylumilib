from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import traceback


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")
try:
    import scipy.ndimage.filters as filters
except ImportError:
    print("Seems you haven't installed scipy!")

from LumiLib.Tools.pointcloudHandling import disparity_to_depth, organized_cloud_from_depth_img
#from LumiLib.Operators.MultiSlotSliceOperator import MultiSlotImg_OperatorBase
from LumiLib.Operators.SingleSlotOperators import ImgOperator





class Orientation2Depth_Operator(ImgOperator):

    def __init__(self, dataVec):
        """
        This class is a Img_OperatorBase child converting
        diparity  images to real depth values
        @param dataVec: <ArrayVector> input ArrayVector
        """
        ImgOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = ["focallength_px", "baseline_mm", "mindepth_mm", "maxdepth_mm"]
        self.setParameterSlot("maxdepth_mm", None)
        self.setParameterSlot("mindepth_mm", None)
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "Orientation2Depth_Operator"


    def __ready__(self):
        """
        If operator ready set the output data shape
        """
        self.ishape = [self._input_slot_shape[0], self._input_slot_shape[1], self._input_slot_shape[2], 1]


    def __allocateResultMemory__(self):
        """
        Allocate single channel array keeping depth values
        """
        self._dataVec.addChannel(np.zeros(self.ishape, dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        Convert disparity to depth
        @param img: <ndarray> 2D disparity image
        @param parameter: <dict> parameter
        @param index: <int> current img index
        @retval: <ndarray> result
        """

        try:
            coh = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            coh[:, :, 0] = img[:, :, 1]
            res = disparity_to_depth(img[:, :, 0], base_line_mm=parameter["baseline_mm"],
                                                   focal_length_px=parameter["focallength_px"],
                                                   min_depth_mm=parameter["mindepth_mm"],
                                                   max_depth_mm=parameter["maxdepth_mm"])

            np.place(res, coh < 1e-1, 1e-25)

            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())
