from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import traceback

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")

try:
    import scipy.ndimage.filters as filters
except ImportError:
    print("Seems you haven't installed scipy!")


from LumiLib.Operators.MultiSlotOperators import MultiSlotImgOperator





class MultiSlotChannelSeparator(MultiSlotImgOperator):

    def __init__(self, dataVec):
        """
        This operator splits a multi color input channel into
        seperate color channel datasets.
        """
        MultiSlotImgOperator.__init__(self, dataVec)

        ############################################
        # This operator is an example for a special
        # usecase, namely the case where you're not
        # able to define your output slots in advance
        # because their name and count dependents on
        # the input data channel. See how to handle
        # this in the constructor and the allocate
        # memory function.
        ############################################

        # names of input slots
        self._operator_inslot_names = ["input"]

        # names of output slots
        # this is a special case, if your number of output slots
        # depend on your input, set self._operator_outslot_names
        # to None, the ready check then ignores the output slot check so
        # that _allocateResultMemory is called even if no output slots are
        # defined. You have to take care of setting the output slot names
        # for yourself then.
        self._operator_outslot_names = None

        # keeps the essential parameter names
        # we need no parameter here this set it to an
        # empty list, setting to None or avoiding to add
        # this line is supported as well
        self._essential_parameter = []

        self.__name__ = "MultiSlotChannelSeparator"


    def __allocateResultMemory__(self):
        """
        This function needs to be implemented in child classes
        and is called after __ready__ and when all in- and output
        slots are set.
        """

        # Due to the fact we wasn't able to define output slot names,
        # we have to define them here depending on our input data
        names = []
        for c in range(self._input_slots_shape["input"][3]):
            names.append(str(c))

        for c in range(self._input_slots_shape["input"][3]):
            oslot_name = self._input_slots["input"]+"_"+names[c]
            self._input_dtypes[oslot_name] = np.float32
            self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                              self._input_slots_shape["input"][1],
                                              self._input_slots_shape["input"][2],
                                              1), dtype=self._input_dtypes[oslot_name]),
                                              oslot_name)


            # if you cannot specify the output slot via setOutputSlot when
            # setting up the operator in the workflow, you need to set the
            # output slot names here manually, this is only due to the line
            # self._operator_outslot_names = None in the constructor
            self._operator_outslot_names = [oslot_name]
            self._output_slots[oslot_name] = oslot_name


    def __compute__(self, input_slots, parameter, index):
        """
        This function needs to be implemented in child classes
        and is called when the user calls operator.run().

        Accesssing Examples:
           - input_slots["mySlot0"] -> ndarray slice from channel set to mySlot0
           - parameter["myParam"] -> value of myParam
           - index -> current slicing index
        ensure that you return your results in form of a dictionary
        which keys should be your output slot names.

        @param input_slots: <dict> containing input slots data
        @param parameter: <dict> containing your parameter slots
        @param index: <int> current slice index
        """

        try:

            # create result slices for each
            # output slot as dictionary, be
            # careful here by setting the shapes
            # to be consistent to what you set
            # in __allocateResultMemory__!
            # here it's easy, all have the same.
            results = {}
            for slot in self._output_slots.keys():
                results[slot] = np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2], 1), dtype=self._input_dtypes["input"])

            # get input slot slice
            img = input_slots["input"]
            # if grayscale store the single channel with new name
            if img.shape[2] == 1:
                results[results.keys()[0]] = img[:]
            # else loop over channels
            # seperating them with new names
            else:
                for c in range(img.shape[2]):
                    tmp = np.zeros((img.shape[0], img.shape[1], 1), dtype=self._input_dtypes["input"])
                    tmp[:, :, 0] = img[:, :, c]
                    oslot_name = self._input_slots["input"]+"_"+str(c)
                    results[oslot_name] = tmp[:]

            return results

        except Exception as e:
            print(e)
            print(traceback.format_exc())


class MultiSlotGaussianSmoothing(MultiSlotImgOperator):

    def __init__(self, arrv):
        """
        This operator applies gaussian smoothing on each channel
        separately using different scales for each channel. The
        output is of same shape as the input. You can either set
        e.g. setParameterSlot("scales", [1.0, 0.5, 2.0]) to
        smooth each rgb channel with a different scale, or set a
        single number to apply this scale to all channels
        """
        MultiSlotImgOperator.__init__(self, arrv)

        ############################################
        # This might be a useless operator but shows
        # how to handle unknown number of input
        # parameter slots. To achieve this we have
        # do a trick by defining a single parameter
        # slot but passing a list to it
        ############################################

        # names of input slots
        self._operator_inslot_names = ["input"]

        # names of output slots
        self._operator_outslot_names = ["colorsmoothed"]

        # keeps the essential parameter names
        self._essential_parameter = ["scales"]

        self.__name__ = "MultiSlotGaussianSmoothing"


    def __allocateResultMemory__(self):
        """
        This function needs to be implemented in child classes
        and is called after __ready__ and when all in- and output
        slots are set.
        """
        # add an output channel same size as input channel
        self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2],
                                          self._input_slots_shape["input"][3]),
                                          dtype=self._input_dtypes["input"]),
                                          name=self._operator_outslot_names[0])


    def __compute__(self, input_slots, parameter, index):
        """
        This function needs to be implemented in child classes
        and is called when the user calls operator.run().

        Accesssing Examples:
           - input_slots["mySlot0"] -> ndarray slice from channel set to mySlot0
           - parameter["myParam"] -> value of myParam
           - index -> current slicing index
        ensure that you return your results in form of a dictionary
        which keys should be your output slot names.

        @param input_slots: <dict> containing input slots data
        @param parameter: <dict> containing your parameter slots
        @param index: <int> current slice index
        """

        try:
            # get input slot slice
            img = np.copy(input_slots["input"])

            # if only one scale is passed as number, create
            # list of scales same length as color channels
            if isinstance(parameter["scales"], float) or isinstance(parameter["scales"], int):
                scales = []
                for c in range(img.shape[2]):
                    scales.append(parameter["scales"])
            # if only one scale is passed as list, create
            # list of scales same length as color channels
            elif isinstance(parameter["scales"], list) and len(parameter["scales"]) == 1:
                scales = []
                for c in range(img.shape[2]):
                    scales.append(parameter["scales"][0])
            else:
                scales = parameter["scales"]

            # create a result image of
            # same shape as input image
            results = {}
            results[self._output_slots['colorsmoothed']] = np.zeros((img.shape[0], img.shape[1], img.shape[2]),
                                                                    dtype=self._input_dtypes['input'])

            # if grayscale smooth only this
            if img.shape[2] == 1:
                results[self._output_slots["colorsmoothed"]][:, :, 0] = vigra.filters.gaussianSmoothing(img[:, :, 0].astype(np.float32), scales[0]).astype(self._input_dtypes['input'])
            # else loop over channels smoothing
            # each separately with different scale
            else:
                # smooth channels
                for c in range(img.shape[2]):
                    if scales[c] > 0.2:
                        results[self._output_slots["colorsmoothed"]][:, :, c] = vigra.filters.gaussianSmoothing(img[:, :, c].astype(np.float32), scales[c]).astype(self._input_dtypes['input'])
                    else:
                        results[self._output_slots["colorsmoothed"]][:, :, c] = img[:, :, c]

            return results

        except Exception as e:
            print(e)
            print(traceback.format_exc())




class MultiSlotEdgeFeatures(MultiSlotImgOperator):

    def __init__(self, arrv):
        """
        This operator applies several edge feature filters on each channel
        separately using different scales for each channel.
        """
        MultiSlotImgOperator.__init__(self, arrv)

        ############################################
        # This operator computes several edge features
        # and is an example for creating several output
        # channels from a single input slot
        ############################################

        # names of input slots
        self._operator_inslot_names = ["input"]

        # names of output slots
        self._operator_outslot_names = ["dx", "dy", "laplace", "corners"]

        # keeps the essential parameter names
        self._essential_parameter = ["scale_dxy", "scale_laplace", "scale_corners"]

        self.__name__ = "MultiSlotEdgeFeatures"


    def __allocateResultMemory__(self):
        """
        This function needs to be implemented in child classes
        and is called after __ready__ and when all in- and output
        slots are set.
        """
        # add an output channel for derivation in x
        self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2],
                                          1),
                                          dtype=np.float32),
                                          name=self._output_slots["dx"])

        # add an output channel for derivation in y
        self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2],
                                          1),
                                          dtype=np.float32),
                                          name=self._output_slots["dy"])

        # add an output channel for gradient magnitude
        self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2],
                                          1),
                                          dtype=np.float32),
                                          name=self._output_slots["laplace"])

        # add an output channel for corner features
        self._dataVec.addChannel(np.zeros((self._input_slots_shape["input"][0],
                                          self._input_slots_shape["input"][1],
                                          self._input_slots_shape["input"][2],
                                          1),
                                          dtype=np.float32),
                                          name=self._output_slots["corners"])



    def __compute__(self, input_slots, parameter, index):
        """
        This function needs to be implemented in child classes
        and is called when the user calls operator.run().

        Accesssing Examples:
           - input_slots["mySlot0"] -> ndarray slice from channel set to mySlot0
           - parameter["myParam"] -> value of myParam
           - index -> current slicing index
        ensure that you return your results in form of a dictionary
        which keys should be your output slot names.

        @param input_slots: <dict> containing input slots data
        @param parameter: <dict> containing your parameter slots
        @param index: <int> current slice index
        """

        try:
            # get input slot slice
            img = np.copy(input_slots["input"])

            # create output memories
            results = {}
            results[self._output_slots['dx']] = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            results[self._output_slots['dy']] = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            results[self._output_slots['laplace']] = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            results[self._output_slots['corners']] = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)

            # if grayscale smooth only this
            if img.shape[2] == 1:
                grad = vigra.filters.gaussianGradient(img[:, :, 0].astype(np.float32), parameter["scale_dxy"])
                lap = vigra.filters.gaussianGradientMagnitude(img[:, :, 0].astype(np.float32), parameter["scale_laplace"])
                corner = vigra.analysis.cornernessHarris(img[:, :, 0].astype(np.float32), parameter["scale_corners"])
                results[self._output_slots['dx']][:, :, 0] = grad[:, :, 1]
                results[self._output_slots['dy']][:, :, 0] = grad[:, :, 0]
                results[self._output_slots['laplace']][:, :, 0] = lap[:]
                results[self._output_slots['corners']][:, :, 0] = corner[:]
            # else loop over channels smoothing
            # each separately with different scale and accumulate channels
            else:
                for c in range(img.shape[2]):
                    grad = vigra.filters.gaussianGradient(img[:, :, c].astype(np.float32), parameter["scale_dxy"])
                    lap = vigra.filters.gaussianGradientMagnitude(img[:, :, c].astype(np.float32), parameter["scale_laplace"])
                    corner = vigra.analysis.cornernessHarris(img[:, :, c].astype(np.float32), parameter["scale_corners"])
                    results[self._output_slots['dx']][:, :, 0] += grad[:, :, 1]
                    results[self._output_slots['dy']][:, :, 0] += grad[:, :, 0]
                    results[self._output_slots['laplace']][:, :, 0] += lap[:]
                    results[self._output_slots['corners']][:, :, 0] += corner[:]
                results[self._output_slots['dx']][:] /= 3.0
                results[self._output_slots['dy']][:] /= 3.0
                results[self._output_slots['laplace']][:] /= 3.0
                results[self._output_slots['corners']][:] /= 3.0

            return results

        except Exception as e:
            print(e)
            print(traceback.format_exc())


# class MultiSlotMasking_Operator(MultiSlotImg_OperatorBase):
#
#     def __init__(self, arrv):
#         """
#         """
#         assert isinstance(arrv, ArrayVector), "Wrong input type for ArrayVector!"
#         MultiSlotImg_OperatorBase.__init__(self, arrv)
#
#         # define the names of the essential parameter
#         # the user must set to use the operator
#         self._essential_parameter = []
#
#         # set your number of in and output slots
#         # your operator expects
#         self._num_of_expected_input_slots = 2
#         self._num_of_expected_output_slots = 1
#
#         # define the dtype of your output data
#         self._dtype = np.float32
#
#         self.__name__ = "MultiSlotMasking_Operator"
#
#
#     def __allocateResultMemory__(self):
#         """
#         This function needs to be implemented in child classes
#         and is called after __ready__ and when all in- and output
#         slots are set.
#         """
#         # in this case we use the self._input_slot_shape directly because
#         # smoothing should be applied on each color channel separately
#         self._dataVec.addChannel(np.zeros(self._input_slot_shapes[0], dtype=np.float32), self._output_slots[0])
#
#
#     def __compute__(self, **kwargs):
#         """
#         This method needs to be implemented and is called on each 2D slice over the
#         whole data channel. It needs to return the result slice ensuring that the
#         slice has the correct shape to fit in your result storage defined via
#         __allocateResultMemory__. Storing in the final output slot is done automatically
#         by the father class.
#         @param img: <ndarray> 2D slice extracted and passed by the fathers __process__ loop
#         @param parameter: <dict> passed by the fathers __process__ loop
#         @param index: <int> current slice index
#         @retval: <ndarray> result
#         """
#
#         try:
#             results = []
#             img = kwargs["input_slots"][0]
#             mask = kwargs["input_slots"][1]
#
#             if img.shape[2] == 1:
#                 assert mask.shape[2] == 1, "Shape mismatch between input image and mask image!"
#                 results.append(img[:]*mask[:])
#                 if not results[0].dytpe == self._dtype:
#                     results[0] = results[0].astype(self._dtype)
#             else:
#                 assert (mask.shape[2] == img.shape[2] or mask.shape[2] == 1)
#                 tmp = np.zeros_like(img).astype(self._dtype)
#                 for c in range(img.shape[2]):
#                     if mask.shape[2] == 1:
#                         tmp[:, :, c] = img[:, :, c] * mask[:, :, 0]
#                     else:
#                         tmp[:, :, c] = img[:, :, c] * mask[:, :, c]
#                 results.append(tmp)
#             return results
#
#         except Exception as e:
#             print(e)
#             print(traceback.format_exc())
#
#
#
#
