from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import traceback

try:
    import numpy as np
    from numpy.linalg import eigh
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")
try:
    import scipy.ndimage.filters as filters
except ImportError:
    print("Seems you haven't installed scipy!")

#from LumiLib.Operators.MultiSlotSliceOperator import MultiSlotEpi_OperatorBase
from LumiLib.Operators.SingleSlotOperators import EpiOperator








class EpiGaussianSmoothing_Operator(EpiOperator):

    def __init__(self, dataVec):
        """
        This class is a 2D slice operator child applying a
        gaussian smoothing to the data domain defined via
        axis flag. Smoothing is done on the 2D sliced per-
        pendicular to the given axis.
        @param axis: <int> operating axis
        """
        EpiOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = ["scale"]
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "GaussianSmoothing_Operator"


    def __allocateResultMemory__(self):
        """
        This method is called as soon as input and output slots are set.
        After that the fields:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        are available and you can decide how the result storage should look like
        """
        # in this case we use the self._input_slot_shape directly because
        # smoothing should be applied on each color channel separately
        self._dataVec.addChannel(np.zeros(self._input_slot_shape, dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each 2D slice over the
        whole data channel. It needs to return the result slice ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param img: <ndarray> 2D slice extracted and passed by the fathers __process__ loop
        @param parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """

        try:
            res = np.zeros((img.shape[0], img.shape[1], img.shape[2]), dtype=self._dtype)
            for c in range(img.shape[2]):
                res[:, :, c] = vigra.filters.gaussianSmoothing(img[:, :, c].astype(np.float32), sigma=float(parameter["scale"]))
            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())











class Epi1DGradientPrefilter_Operator(EpiOperator):

    def __init__(self, dataVec, axis=1):
        assert isinstance(axis, int)
        assert (1<= axis <= 2), "Only axis flags 1 and 2 are allowed!"
        EpiOperator.__init__(self, dataVec)
        self._essential_parameter = ["prefilterscale",
                                     "order",
                                     "magnitude"]
        self.setParameterSlot(name="prefilterscale", value=0.6)
        self.setParameterSlot(name="order", value=1)
        self.setParameterSlot(name="magnitude", value=False)

        self._dtype = np.float32
        self.__name__ = "Epi1DGradientPrefilter_Operator"

        self.kernel = None



    def __allocateResultMemory__(self):
        """
        This needs to be implemented in child classes
        and is called when in- and output slots are set
        both. Create your new data channel here using the
        infos:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        which are available when the function is called
        """
        # output slot has shape [views, y, x, 4], the color channels
        # contain [ST_xx, ST_xy, ST_yy, coherence]
        self._dataVec.addChannel(np.zeros((self._input_slot_shape[0],
                                           self._input_slot_shape[1],
                                           self._input_slot_shape[2], 1),
                                           dtype=self._dtype), self._output_slot_name)


    def __ready__(self):
        """
        this function is called when all slots and
        essential parameter are set and the operator is ready
        """
        self.kernel = vigra.filters.Kernel1D()
        vigra.filters.Kernel1D.initGaussianDerivative(self.kernel, float(self.getParameter("prefilterscale")), 1)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each horizontal epi over the
        whole data channel. It needs to return the resulting epi ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param  img: <ndarray> horizontal epi extracted and passed by the fathers __process__ loop
        @param  parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """

        try:
            # make gray scale dependent on number of channels
            if img.shape[2] > 1:
                tmp = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
                if img.shape[2] == 3:
                    tmp[:, :, 0] = 0.3*img[:, :, 0] + 0.59*img[:, :, 1] + 0.11*img[:, :, 2]
                else:

                    for c in range(img.shape[2]):
                        tmp[:, :, 0] = 1.0/img.shape[2]*img[:, :, c].astype(np.float32)
                img = tmp

            # convolve epi using self.kernel
            res = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            res[:, :, 0] = img[:, :, 0]
            for o in range(parameter["order"]):
                res[:, :, 0] = vigra.filters.convolve(res[:, :, 0].astype(np.float32), (vigra.filters.Kernel1D(), self.kernel))
            if parameter["magnitude"]:
                res[:, :, 0] = np.abs(res[:, :, 0])

            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())









class EpiStructureTensorOrientation_Operator(EpiOperator):

    def __init__(self, dataVec, axis=1):
        assert isinstance(axis, int)
        assert (1<= axis <= 2), "Only axis flags 1 and 2 are allowed!"
        EpiOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = ["innerscale",
                                     "outerscale",
                                     "coherencethreshold",
                                     "focusshifts"]
        #set default values for your parameter
        self.setParameterSlot(name="innerscale", value=0.6)
        self.setParameterSlot(name="outerscale", value=0.9)
        self.setParameterSlot(name="coherencethreshold", value=0.85)
        self.setParameterSlot(name="focusshifts", value=[0])
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "EpiStructureTensorOrientation_Operator"

    def __allocateResultMemory__(self):
        """
        This needs to be implemented in child classes
        and is called when in- and output slots are set
        both. Create your new data channel here using the
        infos:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        which are available when the function is called
        """
        # output slot has shape [views, y, x, 4], the color channels
        # contain [ST_xx, ST_xy, ST_yy, coherence]
        self._dataVec.addChannel(np.zeros((self._input_slot_shape[0],
                                           self._input_slot_shape[1],
                                           self._input_slot_shape[2], 2),
                                           dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each horizontal epi over the
        whole data channel. It needs to return the resulting epi ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param  img: <ndarray> horizontal epi extracted and passed by the fathers __process__ loop
        @param  parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """
        try:
            # final storage, channel 0 orientation, channel 1 coherence
            res = np.zeros((img.shape[0], img.shape[1], 2), dtype=np.float32)

            acc_res = np.zeros((len(parameter["focusshifts"]), img.shape[0], img.shape[1], 2), dtype=np.float32)

            # loop over focus steps
            for n, focus in enumerate(parameter["focusshifts"]):

                # refocus epi
                epi = self.refocus(img, focus)

                # stack stores results from
                # different color channels
                coh_stack = np.zeros((epi.shape[0], epi.shape[1], epi.shape[2]), dtype=np.float32)
                st_stack = np.zeros((epi.shape[0], epi.shape[1], 3, epi.shape[2]), dtype=np.float32)
                # loop over color channels
                for c in range(img.shape[2]):
                    # compute structure tensor
                    st = vigra.filters.structureTensor(epi[:, :, c].astype(np.float32),
                                                         innerScale=parameter["innerscale"],
                                                         outerScale=parameter["outerscale"])
                    # compute coherence value
                    up = np.sqrt((st[:, :, 2]-st[:, :, 0])**2 + 4*st[:, :, 1]**2)
                    down = (st[:, :, 2]+st[:, :, 0] + 1e-25)
                    coh = up / down
                    if self._dataVec.hasAttr("coherencestretch") and self._dataVec.getAttr("coherencestretch") > 0:
                        coh = (1.0/(np.exp(1.0)-1.0)*(np.exp(coh)-1.0))**self._dataVec.getAttr("coherencestretch")

                    # store structure tensor
                    # from different color channels
                    coh_stack[:, :, c] = coh[:]
                    st_stack[:, :, :, c] = st[:]

                # merge structure tensor color channels
                mst = np.zeros((epi.shape[0], epi.shape[1], 4), dtype=np.float32)
                if coh_stack.shape[2] > 1:

                    for c in range(3):
                        cur_coh = coh_stack[:, :, c]
                        tmp_coh = mst[:, :, 3]
                        win = np.where(cur_coh > tmp_coh)
                        tmp_coh[win] = cur_coh[win]
                        for stc in range(3):
                            cur_st = st_stack[:, :, stc, c]
                            tmp_st = mst[:, :, stc]
                            tmp_st[win] = cur_st[win]
                else:
                    mst[:, :, 0:3] = st_stack[:, :, :, 0]
                    mst[:, :, 3] = coh_stack[:, :, 0]

                acc_res[n, :, :, 1] = mst[:, :, 3]

                # compute disparity value
                acc_res[n, :, :, 0] = vigra.numpy.arctan2(2*mst[:, :, 1], mst[:, :, 2]-mst[:, :, 0]) / 2.0
                acc_res[n, :, :, 0] = vigra.numpy.tan(acc_res[n, :, :, 0])

                # mask pixel off boundary orientation
                invalid_ubounds = np.where(acc_res[n, :, :, 0] > 1.1)
                invalid_lbounds = np.where(acc_res[n, :, :, 0] < -1.1)
                tmp_o = acc_res[n, :, :, 0]
                tmp_coh = acc_res[n, :, :, 1]
                tmp_coh[invalid_ubounds] = 0.0
                tmp_coh[invalid_lbounds] = 0.0
                tmp_o[invalid_ubounds] = -1.1
                tmp_o[invalid_lbounds] = -1.1

                # add focus shift and
                # roll data back
                tmp_o += focus

                if focus != 0:
                    acc_res[n, :, :, 0] = self.refocus(tmp_o, -focus)[:, :, 0]
                    acc_res[n, :, :, 1] = self.refocus(tmp_coh, -focus)[:, :, 0]



            if len(parameter["focusshifts"]) > 1:
                for f in range(len(parameter["focusshifts"])):
                    cur_coh = acc_res[f, :, :, 1]
                    cur_ori = acc_res[f, :, :, 0]
                    win = np.where(cur_coh > res[:, :, 1])
                    tmp_coh = res[:, :, 1]
                    tmp_ori = res[:, :, 0]
                    tmp_coh[win] = cur_coh[win]
                    tmp_ori[win] = cur_ori[win]
            else:
                res[:, :, 1] = acc_res[0, :, :, 1]
                res[:, :, 0] = acc_res[0, :, :, 0]


            np.place(res[:, :, 1], res[:, :, 1] > 1.0, 0.0)
            np.place(res[:, :, 1], res[:, :, 1] < parameter["coherencethreshold"], 0.0)
            np.place(res[:, :, 0], res[:, :, 1] < parameter["coherencethreshold"], 0.0)
            np.place(res[:, :, 0], res[:, :, 0] > (np.amax(parameter["focusshifts"])+1.1), 0.0)

            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())






















#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!         E X P E R I M E N T A L    D O N ' T   U S E            !!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# class EpiDoubleOrientation_Operator(Epi_OperatorBase):
#
#     def __init__(self, dataVec):
#         Epi_OperatorBase.__init__(self, dataVec)
#         # define the names of the essential parameter
#         # the user must set to use the operator
#         self._essential_parameter = ["innerscale",
#                                      "outerscale",
#                                      "coherencethreshold",
#                                      "focusshifts"]
#         #set default values for your parameter
#         self.setParameterSlot(name="innerscale", value=0.6)
#         self.setParameterSlot(name="outerscale", value=0.9)
#         self.setParameterSlot(name="coherencethreshold", value=0.85)
#         self.setParameterSlot(name="focusshifts", value=[0])
#         # define the dtype of your output data
#         self._dtype = np.float32
#
#         self.__name__ = "EpiDoubleOrientation_Operator"
#
#     def __allocateResultMemory__(self):
#         """
#         This needs to be implemented in child classes
#         and is called when in- and output slots are set
#         both. Create your new data channel here using the
#         infos:
#                 self._input_slot_shape
#                 self._input_slot_dtype
#                 self._output_slot_name
#         which are available when the function is called
#         """
#         # output slot has shape [views, y, x, 4], the color channels
#         # contain [ST_xx, ST_xy, ST_yy, coherence]
#         self._dataVec.addChannel(np.zeros((self._input_slot_shape[0],
#                                            self._input_slot_shape[1],
#                                            self._input_slot_shape[2], 2),
#                                            dtype=self._dtype), self._output_slot_name)
#
#
#     def __compute__(self, img, parameter, index):
#         """
#         This method needs to be implemented and is called on each horizontal epi over the
#         whole data channel. It needs to return the resulting epi ensuring that the
#         slice has the correct shape to fit in your result storage defined via
#         __allocateResultMemory__. Storing in the final output slot is done automatically
#         by the father class.
#         @param  img: <ndarray> horizontal epi extracted and passed by the fathers __process__ loop
#         @param  parameter: <dict> passed by the fathers __process__ loop
#         @param index: <int> current slice index
#         @retval: <ndarray> result
#         """
#
#         try:
#             # final storage, channel 0 orientation, channel 1 coherence
#             res = np.ones((img.shape[0], img.shape[1], 2), dtype=np.float32)
#             res[:] = -1
#
#             # convert to grayscale before computation
#             if img.shape[2] > 1:
#                 tmp = np.zeros((img.shape[0], img.shape[1], 1), dtype=img.dtype)
#                 if img.shape[2] == 3:
#                     tmp[:, :, 0] = 0.3*img[:, :, 0] + 0.59*img[:, :, 1] + 0.11*img[:, :, 2]
#                 else:
#                     for c in range(img.shape[2]):
#                         tmp[:, :, 0] += 1.0/float(img.shape[2])*img[:, :, c]
#                 img = tmp
#
#             # loop over focus steps
#             for n, focus in enumerate(parameter["focusshifts"]):
#                 # refocus epi
#                 epi = self.refocus(img, focus)
#
#                 # create mask if orientation is available
#                 mask = None
#                 if self._dataVec.hasChannel("orientation"):
#                     mask = self._dataVec.getChannel("orientation")[:, index, :, 1]
#                     mask = self.refocus(mask, focus)
#                 np.place(mask, mask > parameter["coherencethreshold"], -1)
#                 np.place(mask, mask > 0, 0)
#                 mask[:] += 1
#
#                 # compute tensor
#                 tensor = self.computeTensor(img, parameter)
#
#                 # evaluate tensor
#                 for _y in range(img.shape[0]):
#                     for _x in range(img.shape[1]):
#                         if mask is not None:
#                             if mask[_y, _x] == 1:
#                                 w, v = eigh(np.mat(tensor[_y, _x, :, :]))
#                                 c = np.array(v[0])[0]
#                                 if c[1]**2 - 4*c[0]*c[2] > 0:
#                                     u, v = self.getOrientations(c)
#                                     d1 = u[0]/u[1]
#                                     d2 = v[0]/v[1]
#                                     if -1.1 <= d1 <= 1.1:
#                                         res[_y, _x, 0] = d1 + focus
#                                     if -1.1 <= d2 <= 1.1:
#                                         res[_y, _x, 1] = d2 + focus
#                         else:
#                             w, v = eigh(np.mat(tensor[_y, _x, :, :]))
#                             c = np.array(v[0])[0]
#                             if c[1]**2 - 4*c[0]*c[2] > 0:
#                                 u, v = self.getOrientations(c)
#                                 res[_y, _x, 0] = u[0]/u[1]
#                                 res[_y, _x, 1] = v[0]/v[1]
#
#                 res[:, :, 0] = filters.median_filter(res[:, :, 0], size=3)
#             return res
#
#         except Exception as e:
#             print(e)
#             print(traceback.format_exc())
#
#
#     def computeTensor(self, img, parameter):
#         f = vigra.filters.gaussianGradient(img[:, :, 0].astype(np.float32), parameter["innerscale"])
#         fy = f[:, :, 0]
#         fx = f[:, :, 1]
#         tensor = np.zeros((img.shape[0], img.shape[1], 3, 3), dtype=np.float32)
#
#         tensor[:, :, 0, 0] = np.abs(fx**4)
#         tensor[:, :, 0, 1] = np.abs(fx**2)*fx*fy
#         tensor[:, :, 0, 2] = np.abs(fx*fy)**2
#         tensor[:, :, 1, 0] = tensor[:, :, 0, 1]
#         tensor[:, :, 0, 1] = 0.5*(np.abs(fx)**2*np.abs(fy)**2 + np.abs(fx*fy)**2)
#         tensor[:, :, 1, 1] = np.abs(fy**2)*fx*fy
#         tensor[:, :, 2, 0] = tensor[:, :, 0, 2]
#         tensor[:, :, 2, 1] = tensor[:, :, 1, 1]
#         tensor[:, :, 2, 2] = np.abs(fy**4)
#
#         return tensor
#
#
#     def getOrientations(self, c):
#         coeff = [1, c[1], c[0]*c[2]]
#         r = np.roots(coeff)
#         z1 = r[0]
#         z2 = r[1]
#         C = np.mat(np.array([[c[0], z1], [z2, c[1]]]))
#         u = self.normVec([C[0, 0]+C[0, 1], C[1, 0]+C[1, 1]])
#         v = self.normVec([C[0, 0]+C[0, 1], C[1, 0]+C[1, 1]])
#         return u, v
#
#
#     def normVec(self, vec):
#         sum = vec[0]+vec[1]+1e-9
#         return [vec[0]/sum, vec[1]/sum]







# class OrientationLineFitOptimizer_Operator(MultiSlotEpi_OperatorBase):
#
#     def __init__(self, dataVec):
#         MultiSlotEpi_OperatorBase.__init__(self, dataVec)
#         self._essential_parameter = []
#
#         self._dtype = np.float32
#         self.__name__ = "OrientationLineFitOptimizer_Operator"
#
#
#
#
#     def __allocateResultMemory__(self):
#         """
#         This needs to be implemented in child classes
#         and is called when in- and output slots are set
#         both. Create your new data channel here using the
#         infos:
#                 self._input_slot_shape
#                 self._input_slot_dtype
#                 self._output_slot_name
#         which are available when the function is called
#         """
#         self._dataVec.addChannel(np.zeros((self._input_slot_shapes[0][0],
#                                            self._input_slot_shapes[0][1],
#                                            self._input_slot_shapes[0][2], 2),
#                                            dtype=self._dtype), self._output_slots[0])
#
#
#     def __ready__(self):
#         """
#         this function is called when all slots and
#         essential parameter are set and the operator is ready
#         """
#         pass
#
#
#     def __compute__(self, **kwargs):
#         """
#         This needs to be implemented in child classes and is called when the user calls operator.run()
#         Arguments passed are input_slots=indata_slices, output_slots=outdata_slices, parameter=self._parameter, index=index
#         """
#
#         try:
#             res = [np.zeros((kwargs["input_slots"][0].shape[0], kwargs["input_slots"][0].shape[1], 2), dtype=np.float32)]
#
#             lori = kwargs["input_slots"][0][:, :, 0]
#             lcoh = kwargs["input_slots"][0][:, :, 1]
#             color = kwargs["input_slots"][1][:, :, :]
#
#             y_cv_index = lori.shape[0]/2
#
#             for x in xrange(lori.shape[1]):
#                 col = color[y_cv_index, x, :]
#                 self.checkRayNeighborhood(lori, lcoh, pos=[y_cv_index, x])
#
#             return res
#
#         except Exception as e:
#             print(e)
#             print(traceback.format_exc())
#
#
#     def checkRayNeighborhood(self, ori_img, coh_img, pos):
#         coh = coh_img[pos[0], pos[1]]
#         if coh > 0.0:
#             ori_ref = ori_img[pos[0], pos[1]]
#             npos_top = [float(pos[0]-1.0), float(pos[1])+ori_ref]
#             npos_bot = [float(pos[0]+1.0), float(pos[1])-ori_ref]
#             ori_top = self.ipol(ori_img, npos_top)
#             ori_bot = self.ipol(ori_img, npos_bot)
#
#             energy = np.abs(ori_ref-ori_top) + np.abs(ori_ref-ori_bot) + np.abs(ori_bot-ori_top)
#
#
#     def ipol(self, ori_img, pos):
#         if (0 <= pos[0] < ori_img.shape[0]) and (0 <= pos[1] < ori_img.shape[1]):
#             x = pos[1]
#             y = pos[0]
#             x1 = int(np.floor(pos[1]))
#             x2 = int(np.ceil(pos[1]))
#             y1 = int(np.ceil(pos[0]))
#             y2 = int(np.floor(pos[0]))
#             if x1 < 0 or x2 >= ori_img.shape[1]:
#                 return None
#             if y1 >= ori_img.shape[0] or y2 < 0:
#                 return None
#             Q11 = [y1, x1]
#             Q12 = [y2, x1]
#             Q21 = [y1, x2]
#             Q22 = [y2, x2]
#
#             if x1 < x2:
#                 fR1 = (x2-x)/(x2-x1)*ori_img[Q11[0], Q11[1]] + (x-x1)/(x2-x1)*ori_img[Q21[0], Q21[1]]
#                 fR2 = (x2-x)/(x2-x1)*ori_img[Q12[0], Q12[1]] + (x-x1)/(x2-x1)*ori_img[Q22[0], Q22[1]]
#             else:
#                 fR1 = ori_img[Q11[0], Q11[1]]
#                 fR2 = ori_img[Q12[0], Q12[1]]
#             if y2 < y1:
#                 p = (y2-y)/(y2-y1)*fR1 + (y-y1)/(y2-y1)*fR2
#             else:
#                 p = fR1
#             return p
#         else:
#             return None

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!