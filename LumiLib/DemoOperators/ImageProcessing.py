from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import traceback

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")

try:
    import scipy.ndimage.filters as filters
except ImportError:
    print("Seems you haven't installed scipy!")

from LumiLib.Operators.SingleSlotOperators import ImgOperator




class ImgGaussianSmoothing_Operator(ImgOperator):

    def __init__(self, dataVec):
        """
        This class is a Img_OperatorBase child applying a
        gaussian smoothing to the data domain defined via
        axis flag. Smoothing is done on the 2D sliced per-
        pendicular to the given axis.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        ImgOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = ["scale"]
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "ImgGaussianSmoothing_Operator"


    def __allocateResultMemory__(self):
        """
        This method is called as soon as input and output slots are set.
        After that the fields:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        are available and you can decide how the result storage should look like
        """
        # in this case we use the self._input_slot_shape directly because
        # smoothing should be applied on each color channel separately
        self._dataVec.addChannel(np.zeros(self._input_slot_shape, dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each 2D slice over the
        whole data channel. It needs to return the result slice ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param img: <ndarray> 2D slice extracted and passed by the fathers __process__ loop
        @param parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """

        try:
            res = np.zeros((img.shape[0], img.shape[1], img.shape[2]), dtype=self._dtype)
            for c in range(img.shape[2]):
                res[:, :, c] = vigra.filters.gaussianSmoothing(img[:, :, c].astype(np.float32), sigma=float(parameter["scale"]))
            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())





class channelReducerRGB2Gray_Operator(ImgOperator):

    def __init__(self, dataVec):
        """
        This class is a 2D slice operator child applying a
        gaussian smoothing to the data domain defined via
        axis flag. Smoothing is done on the 2D sliced per-
        pendicular to the given axis.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        ImgOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = []
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "channelReducerRGB2Gray_Operator"


    def __ready__(self):
        """
        this function is called when all slots and
        essential parameter are set and the operator is ready
        """
        pass


    def __allocateResultMemory__(self):
        """
        This method is called as soon as input and output slots are set.
        After that the fields:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        are available and you can decide how the result storage should look like
        """
        # in this case we use the self._input_slot_shape directly because
        # smoothing should be applied on each color channel separately
        # output slot has shape [views, y, x, 4], the color channels
        # contain [ST_xx, ST_xy, ST_yy, coherence]
        self._dataVec.addChannel(np.zeros((self._input_slot_shape[0],
                                           self._input_slot_shape[1],
                                           self._input_slot_shape[2], 1),
                                           dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each 2D slice over the
        whole data channel. It needs to return the result slice ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param img: <ndarray> 2D slice extracted and passed by the fathers __process__ loop
        @param parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """

        try:
            # make gray scale dependent on number of channels
            res = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
            if img.shape[2] > 1:
                if img.shape[2] == 3:
                    res[:, :, 0] = 0.3*img[:, :, 0] + 0.59*img[:, :, 1] + 0.11*img[:, :, 2]
                else:

                    for c in range(img.shape[2]):
                        res[:, :, 0] = 1.0/img.shape[2]*img[:, :, c].astype(np.float32)
            else:
                res[:, :, 0] = img[:, :, 0]
            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())



class ImgMedianSmoothing_Operator(ImgOperator):

    def __init__(self, dataVec):
        """
        This class is a 2D slice operator child applying a
        gaussian smoothing to the data domain defined via
        axis flag. Smoothing is done on the 2D sliced per-
        pendicular to the given axis.
        @param dataVec: <ArrayVector> input ArrayVector
        """
        ImgOperator.__init__(self, dataVec)
        # define the names of the essential parameter
        # the user must set to use the operator
        self._essential_parameter = ["scale"]
        # define the dtype of your output data
        self._dtype = np.float32

        self.__name__ = "ImgMedianSmoothing_Operator"


    def __allocateResultMemory__(self):
        """
        This method is called as soon as input and output slots are set.
        After that the fields:
                self._input_slot_shape
                self._input_slot_dtype
                self._output_slot_name
        are available and you can decide how the result storage should look like
        """
        # in this case we use the self._input_slot_shape directly because
        # smoothing should be applied on each color channel separately
        shape = self._input_slot_shape
        self._dataVec.addChannel(np.zeros((shape[0], shape[1], shape[2], 2), dtype=self._dtype), self._output_slot_name)


    def __compute__(self, img, parameter, index):
        """
        This method needs to be implemented and is called on each 2D slice over the
        whole data channel. It needs to return the result slice ensuring that the
        slice has the correct shape to fit in your result storage defined via
        __allocateResultMemory__. Storing in the final output slot is done automatically
        by the father class.
        @param img: <ndarray> 2D slice extracted and passed by the fathers __process__ loop
        @param parameter: <dict> passed by the fathers __process__ loop
        @param index: <int> current slice index
        @retval: <ndarray> result
        """

        try:
            res = np.zeros((img.shape[0], img.shape[1], 2), dtype=self._dtype)
            res[:, :, 0] = filters.median_filter(img[:, :, 0].astype(np.float32), size=float(parameter["scale"]))
            res[:, :, 1] = img[:, :, 1]
            mask_indices = np.where(img[:, :, 1] == 0)
            tmp = res[:, :, 0]
            tmp[mask_indices] = 0.0
            return res

        except Exception as e:
            print(e)
            print(traceback.format_exc())
