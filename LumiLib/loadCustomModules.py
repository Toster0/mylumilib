from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import traceback
CWD = os.path.dirname(os.path.realpath(__file__))


try:
    import importlib
except ImportError:
    print("Seems you haven't installed importlib!")

def loadCustomWorkflowClass(parameter):

    name = parameter.get("WorkFlowEngine", "Workflow")
    try:
        module_ = importlib.import_module('LumiLib.CustomWorkflows.' + name)
        try:
            class_ = getattr(module_, name)
            return class_()
        except:
            print("failed to instanciate CustomWorkflowClass:", name)
            return None
    except:
        print("failed to load CustomWorkflow:", name)
        print(traceback.format_exc())
        return None

def loadCustomVectorProcessor(parameter):

    name = parameter.get("VectorEngine", "Processor")
    try:
        module_ = importlib.import_module('LumiLib.CustomVectorProcessors.' + name)
        try:
            class_ = getattr(module_, name)
            return class_()
        except:
            print("failed to instanciate CustomVectorProcessors:", name)
            return None
    except:
        print("failed to load CustomVectorProcessors:", name)
        print(traceback.format_exc())
        return None
