from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import h5py as h5
except ImportError:
    print("Seems you haven't installed h5py!")

from os import sep, mkdir
from os.path import isdir, isfile, exists

from LumiLib.Common import hickle as hkl
from LumiLib.IO.io import loadFromSequenceLoc, loadFromFilenameList, makeFromImageList



def createFromImageList(image_list, roi=None, name=None, reverse_order=False, transposed=False, arrv=None, dump_filename=None):
    """
    A 3D ArrayVector instance is returned from a list of images. If a ArrayVector
    instance is passed, the loaded sequence is merged into this and returned. Be careful
    and set a name when loading several sequences from image files into the same instance,
    data channels with the same name are overwritten.

    @param image_list: <[ndarrays]> list of image files
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @param arrv: <ArrayVector> other ArrayVector instance merges into the instance created in this function
    @oaram dump_filename: <str> set an .h5 project filename, if not set it can be set manually later using setFilename.
    @retval: ArrayVector instance
    """
    # if none passed, instanciate a new ArrayVector
    if arrv is not None:
        assert isinstance(arrv, ArrayVector)
    else:
        arrv = ArrayVector()

    data = makeFromImageList(image_list=image_list, roi=roi, name=name, transposed=transposed, reverse_order=reverse_order)

    # set default name when arg name was None
    if name is None:
        name = "color"

    # add data to ArrayVector instance
    arrv.addChannel(data, name=name)
    if roi is not None:
        arrv.addAttr("roi", np.array(roi))

    if dump_filename is not None:
        arrv.setFilename(dump_filename)

    return arrv





def createFromFilenameList(filenames, roi=None, name=None, reverse_order=False, arrv=None, dump_filename=None):
    """
    load image sequence from filename list, a 3D ArrayVector instance is returned. If a ArrayVector
    instance is passed, the loaded sequence is merged into this and returned. Be careful
    and set a name when loading several sequences from image files into the same instance,
    data channels with the same name are overwritten.

    @param filenames: <str> image filenames
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @param arrv: <ArrayVector> other ArrayVector instance merges into the instance created in this function
    @oaram dump_filename: <str> set an .h5 project filename, if not set it can be set manually later using setFilename.
    @retval: ArrayVector instance
    """

    # if none passed, instanciate a new ArrayVector
    if arrv is not None:
        assert isinstance(arrv, ArrayVector)
    else:
        arrv = ArrayVector()

    data = loadFromFilenameList(filenames, roi, name, reverse_order)

    # set default name when arg name was None
    if name is None:
        name = "color"

    # add data to ArrayVector instance
    arrv.addChannel(data, name=name)
    if roi is not None:
        arrv.addAttr("roi", np.array(roi))

    if dump_filename is not None:
        arrv.setFilename(dump_filename)

    return arrv






def createFrom3DSequence(dpath, roi=None, name=None, reverse_order=False, arrv=None, dump_filename=None):
    """
    load image sequence from files, a 3D ArrayVector instance is returned. If a ArrayVector
    instance is passed, the loaded sequence is merged into this and returned. Be careful
    and set a name when loading several sequences from image files into the same instance,
    data channels with the same name are overwritten.

    @param dpath: <str> path to image files
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @param arrv: <ArrayVector> other ArrayVector instance merges into the instance created in this function
    @oaram dump_filename: <str> set an .h5 project filename, if not set it can be set manually later using setFilename.
    @retval: ArrayVector instance
    """

    # if none passed, instanciate a new ArrayVector
    if arrv is not None:
        assert isinstance(arrv, ArrayVector)
    else:
        arrv = ArrayVector()

    data = loadFromSequenceLoc(dpath, roi, reverse_order)

    # set default name when arg name was None
    if name is None:
        name = "color"

    # add data to ArrayVector instance
    arrv.addChannel(data, name=name)
    if roi is not None:
        arrv.addAttr("roi", np.array(roi))

    if dump_filename is not None:
        arrv.setFilename(dump_filename)

    return arrv






class ArrayVector(object):
    def __init__(self, filename=None):
        """
        This class is the basis data container of the entire framework. It can
        carry an arbitrary number of numpy arrays and attributes. Each data channel
        can be dumped to a hdf5 file by setting it invisible. If a channel is requested
        by an operator the data are redumped automatically.
        @param filename: <str> optional a filename of a .h5 file can be passed
        """
        if filename is not None:
            assert isinstance(filename, str)

        # data and attributes container
        self._data = {}
        self._visibility = {}
        self._attrs = {}

        # filename and dump h5 file object
        self._filename = filename
        self._storage = None

        # check if filename is file or path
        # create hdf5 file to dump data
        if filename is not None:
            if exists(filename):
                self.load(filename)
            else:
                self.setFilename(filename)


    def setFilename(self, filename):
        """
        Set filename of dumping container
        @param: <str> dump file name
        """
        assert isinstance(filename, str)
        self._filename = filename
        self.__createDumpFile__(filename)


    def setVisible(self, name, state=True):
        """
        Change state of channel visibility
        @param name: <str> channel name
        @param state: <bool> visibility state [True]
        """
        assert isinstance(name, str), "Argument Error, name must be type str!"
        assert isinstance(state, bool), "Argument Error, state must be of type boolean!"

        if not self._data.has_key(name) and not (name in list(self._storage)):
            assert False, "Cannot change visibility of channel that doesn't exist!"

        if state:
            try:
                self.__redump__(name)
                self._visibility[name] = True
            except:
                pass
        else:
            try:
                if name not in list(self._storage):
                    self.__dump__(name)
                else:
                    self._data.pop(name)
                self._visibility[name] = False
            except:
                pass


    def setAllVisible(self, state=True):
        """
        Change state of channel visibility for all channels
        @param state: <bool> visibility state [True]
        """
        assert isinstance(state, bool), "Argument Error, state must be of type boolean!"
        if self._filename is None:
            print("No filename set, cannot dump data!")
            return False
        for key in self._data.keys():
            self.setVisible(key, state)
        return True


    def isVisible(self, name):
        """
        check if channel is visible
        @param name: <str> channel name
        @retval <bool> visibility status
        """
        assert isinstance(name, str), "Argument Error, name must be type str!"
        assert name in self._visibility.keys(), "Unknown data layer name!"
        return self._visibility[name]


    def hasChannel(self, name):
        """
        returns True if channel is available. Warning, does not check dumped only channels!"
        @param name: <str> channel name
        @retval <bool>
        """
        if name in self._data.keys():
            return True
        else:
            return False


    def dtypeOf(self, name):
        """
        returns dtype of channel. Warning, does not check dumped only channels!"
        @param name: <str> channel name
        @retval <np.dtype>
        """
        if name in self._data.keys():
            return self._data[name].dtype
        else:
            return None


    def shapeOf(self, name):
        """
        returns the shape of the data chanel requested
        @param name: <str> channel name
        @retval <list> shape of data channel
        """
        assert isinstance(name, str), "Argument Error, name must be type str!"
        if self._data.has_key(name):
            return self._data[name].shape
        elif name in list(self._storage):
            return self._storage[name].shape
        else:
            assert False, "Channel " + name + " is not available!"


    def hasDumpedChannel(self, name, load=False):
        """
        returns True if channel of name is available in the dump file but not loaded"
        @param name: <str> channel name
        @param load: <bool> if set to True the dumped channel is loaded in case of existence"
        @retval <bool>
        """
        if self._storage is None:
            print("Warning, no dump file created, call createDumpFile first!")
            return False

        if name in list(self._storage):
            if load:
                self._data[name] = np.copy(self._storage[name])
            return True
        else:
            return False


    def addAttr(self, name, value):
        """
        add attribute to ArrayVector
        @param name: <obj> attribute name
        @param value: <obj> attribute
        """
        self._attrs[name] = value


    def getAttr(self, name=None):
        """
        get attribute by name if name is None, all attributes are returned
        in separate lists [[name],[value]]
        @param name: <obj> channel name [None]
        @retval <obj> attribute
        """
        if name is not None:
            if self._attrs.has_key(name):
                return self._attrs[name]
            else:
                print("Warning, you asked for a not existing attribute (", name, "), None was returned!")
                return None
        else:
            return [self._attrs.keys(), self._attrs.values()]


    def hasAttr(self, name):
        """
        check if an attribute exist
        @param name: <str> attribute name
        @return: <bool> True if attribute exist False if not
        """
        if name in self._attrs.keys():
            return True
        return False



    def addChannel(self, array, name=None, visible=True):
        """
        add ndarray to ArrayVector data, visibility is True by default
        @param array: <ndarray> input data
        @param name: <obj> channel name
        """
        assert isinstance(array, np.ndarray)

        # if no name was set, data channel
        # gets index of current element number
        if name is None:
            name = len(self._data.keys())
        self._data[name] = array
        self._visibility[name] = visible


    def getChannel(self, name):
        """
        returns data by name
        @param name: <obj> channel name
        @retval <ndarray> data channel
        """
        if self._data.has_key(name):
            return self._data[name]
        else:
            try:
                return np.copy(self._storage[name])
            except:
                print("Warning, you asked for a not existing data channel, None was returned!")
                return None


    def getDumpedChannelNames(self):
        """
        returns a list of all dumped channel
        @retval <list> dumped channel names
        """
        names = []
        for key in list(self._storage):
            names.append(key)
        return names


    def getChannelNames(self):
        """
        returns a list of all loaded channel
        @retval <list> channel names
        """
        return self._data.keys()


    def addEmptyChannel(self, name, shape, dtype=np.float32):
        """
        Add an empy data channel
        @param name: <str> channel name
        @param shape: <tuple> shape
        @param dtype: <dtype>
        """
        assert isinstance(name, str), "Argument Error, name must be type str!"
        if isinstance(shape, list):
            shape = tuple(shape)
        assert isinstance(shape, ()), "Argument Error, shape must be type tuple!"

        self._data[name] = np.zeros(shape, dtype=dtype)
        self._visibility[name] = True


    def copyChannel(self, name, new_name):
        """
        Copy a data channel
        @param name: <str> channel name to copy
        @param new_name: <str> new channel name
        """
        assert isinstance(name, str), "Argument Error, name must be type str!"
        assert isinstance(new_name, str), "Argument Error, new_name must be type str!"
        
        if name in self._data.keys():
            self._data[new_name] = np.copy(self._data[name])
        elif name in list(self._storage):
            self._data[new_name] = np.copy(self._storage[name])
        else:
            assert False, "Channel requested for copy does not exist!"



    def getSlice(self, name, axis, index):
        """
        get a slice of a 3D Volume channel. The argument axis defines the coordinate system
        axis the slice is extracted perpendicular to. The argument index defines the position
        on this axis. It is only possible to use this function for 3D Volmumes, axis needs to be
        between 0 and 2, means color channel dimension cannot be sliced!
        @param name: <str> channel name
        @param axis: <int> slice axis
        @param index: <int> slice position
        @retval: <ndarray>
        """
        assert isinstance(name, str)
        if not name in self._data.keys():
            assert False, "Channel" + name + " not available!"
        assert isinstance(axis, int), "Wrong input type!"
        assert 0 <= axis <= 2, "only 3D plus color channels data are supported!"
        assert isinstance(index, int), "Wrong input type!"
        assert 0 <= index < self._data[name].shape[axis], "Index requested out of range!"

        if axis == 0:
            return self._data[name][index, :, :, :]
        elif axis == 1:
            return self._data[name][:, index, :, :]
        elif axis == 2:
            return self._data[name][:, :, index, :]


    def save(self, remove_data=True):
        """
        saves all data and attributes to the h5 file
        @param remove_data: <bool> if False the dumped data set is keept in in object, otherwise removed
        """
        if self._filename is not None:
            for key in self._data.keys():
                try:
                    self.__dump__(key, remove_data)
                except Exception as e:
                    print("Problems with", key)
                    print(e)
            for key in self._attrs.keys():
                try:
                    self._storage.attrs[key] = self._attrs[key]
                except Exception as e:
                    print("Problems with", key)
                    print(e)
            self._storage.close()
        else:
            assert False, "cannot save ArrayVector, no filename set!"


    def load(self, filename, redump_all=False):
        """
        load a ArrayVector from h5 file, warning, data_sets stay dumped. Please redump data you need
        or set redump_all to True.
        @param filename: <str> filename (*.h5)
        @param redump_all: <bool> if True all data_sets are load into memory
        """
        assert exists(filename), "Warning, file to load data from doesn't exist!"
        self._storage = h5.File(filename, "a")
        self.setFilename(filename)
        self._attrs = dict(self._storage.attrs)
        if redump_all:
            for name in list(self._storage):
                self._data[name] = self._storage[name]


    def loadSequence(self, images_loc, roi=None, name=None, reverse_order=False):
        """
        load a image sequence from file, setting a region of interest is possible

        @param images_loc: <str> path to image files
        @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
        @param name: <str> channel name, [default="color"]
        @param reverse_order: <bool> switch files order [default=False]
        @retval: <ndarray> data volume
        """
        if name is None:
            name = str(len(self._data)+1)
        data = loadFromSequenceLoc(images_loc, roi, name, reverse_order)
        self.addChannel(data, name)


    def isEmpty(self):
        """
        Returns True if instance has no data
        @retval: <bool> empty status
        """
        return len(self._data) == 0 and len(self._storage) == 0


    def __add__(self, other):
        """
        enables operation lf = lfa + lfb but with the restriction that only the channels the instance is keeping are
        copied. Data dumped are ignored, so ensure you redump data that should be copied! A filename will also not be
        set, needs to be set afterwards if needed as well as a dump file object.
        @param other: <ArrayVector>
        @retval: <ArrayVector>
        """
        assert isinstance(other, ArrayVector)
        out = ArrayVector()
        for key in self._attrs.keys():
            out.attrs[key] = self._attrs[key]
        for key in other.attrs.keys():
            out.attrs[key] = other.attrs[key]
        for key in self._data.keys():
            out.data[key] = np.copy(self._data[key])
        for key in other.data.keys():
            out.data[key] = np.copy(other.data[key])
        return out


    def __str__(self):
        """
        enables to do print(lf) passing information of the object content to the print function.
        @retval: <str>
        """
        txt = "Attributes:\n"
        for key in self._attrs.keys():
            txt += "\t-> " + key + " : " + str(self._attrs[key]) + "\n"
        txt += "Data:\n"
        for key in self._data.keys():
            txt += "\t-> " + key + " : " + str(self._data[key].shape) + "\n"
        if self._storage is not None:
            txt += "Dumped:\n"
            for key in list(self._storage):
                txt += "\t-> " + key + " : " + str(self._storage[key].shape) + "\n"
        if self._filename is not None:
            txt += "filename is set: "+self._filename+"\n"
            if self._storage is not None:
                txt += " dumpfile available!\n"
            else:
                txt += "!\n"
        else:
            txt += "filename not set!"
        return txt


    def __createDumpFile__(self, filename=None):
        """
        setup a dump file object
        @param filename: <str> location or filepath extension (.h5)
        """
        if filename is None and self._filename is None:
            assert False, "Warning, no filename specified, cannot create dumpfile. Call createDumpFile(filename)!"

        assert isinstance(filename, str), "Wrong filename input type!"

        if filename is not None:
            self._filename = filename
        if not filename.endswith(".h5"):
            pos_fname = self._filename + ".h5"
        else:
            pos_fname = self._filename
        if isfile(pos_fname):
            self._storage = hkl.file_opener(pos_fname, "r+")
        elif self._filename.endswith(".h5"):
            self._storage = hkl.file_opener(self._filename, "w")
        elif isdir(self._filename):
            if not self._filename.endswith(sep):
                self._filename += sep
            self._storage = hkl.file_opener(self._filename + "arrayVector.h5", "w")
        elif not isdir(self._filename):
            try:
                mkdir(self._filename)
            except:
                assert False, "Creating directory failed!"
            if not self._filename.endswith(sep):
                self._filename += sep
            self._storage = hkl.file_opener(self._filename + "arrayVector.h5", "w")
        else:
            assert False, "Uncaught Exception in ArrayVector.__init__!"





    def __dump__(self, name, remove=True):
        """
        dump data channel to file
        @param name:  <obj> channel name
        """
        if self._filename is not None:
            if self._data.has_key(name):
                if name in list(self._storage):
                    del self._storage[name]
                try:
                    assert self._data[name].dytpe == np.uint8
                    hkl.dump_ndarray(self._data[name],
                                     self._storage,
                                     name=str(name),
                                     compression="gzip",
                                     dtype=self._data[name].dtype)
                except:
                    try:
                        hkl.dump_ndarray(self._data[name],
                                         self._storage,
                                         name=str(name),
                                         compression=None,
                                         dtype=self._data[name].dtype)
                    except:
                        assert False, "Warning, error while dumping channel " + name + " to file!"

            if remove:
                if self._data.has_key(name):
                    self._data.pop(name)
        else:
            print("Warning, no dump file created, call createDumpFile first!")


    def __redump__(self, name=None):
        """<
        read data from dump file back to memory. If None all dumped data are copied.
        @param name: <str> channel name [default:None redumps all]
        """
        if self._storage is None:
            print("Warning, no dump file created, call createDumpFile first!")
            return

        if name is None:
            for key in list(self._storage):
                self._data[key] = np.copy(self._storage[key])
        else:
            if self._data.has_key(name):
                assert False, "channel cannot redumped, it's already available!"
            else:
                try:
                    d = np.copy(self._storage[name])
                    self._data[name] = np.copy(d)
                except:
                    print("Warning, couldn't redump channel " + name + "!")


