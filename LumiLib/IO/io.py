from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    #from scipy.misc import imread
    #import scipy.misc as misc
	from skimage.io import imread
except ImportError:
    print("Seems you haven't installed scipy!")

from os import sep
from glob import glob


FILE_TYPES = ["png", "PNG", "jpg", "jpeg", "JPG", "JPEG", "tif", "tiff", "TIF", "TIFF", "bmp", "ppm"]


def readImageROI(fname, roi=None, transposed=False):
    """
    read image from filename, if roi is set, a region of interest is returned.

    @param fname: <str> path to image file
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @retval: <ndarray> image data
    """
    assert isinstance(fname, str)

    # read image file
    try:
        im = imread(fname)
	print("loaded image:", fname, "of shape", im.shape)
    except:
        assert False, "read image error!"

    # if no roi was set, return images normally
    if roi is None:
        if len(im.shape) >= 3:
            if transposed:
                tmp = np.zeros((im.shape[1], im.shape[0], 3), dtype=im.dtype)
                for c in range(3):
                    tmp[:, :, c] = np.transpose(im[:, :, c])
                return tmp
            else:
                return im[:, :, 0:3]
        elif len(im.shape) == 2:
            if transposed:
                tmp = np.zeros((im.shape[1], im.shape[0], 1), dtype=im.dtype)
                tmp[:, :, 0] = np.transpose(im[:])[:]
            else:
                tmp = np.zeros((im.shape[0], im.shape[1], 1), dtype=im.dtype)
                tmp[:, :, 0] = im[:]
            return tmp
        else:
            assert False, "unknown image format!"

    # else read region of interest only
    else:
        assert len(roi) == 2, "Wrong roi format!"
        assert len(roi[0]) == 2, "Wrong roi format!"
        assert len(roi[1]) == 2, "Wrong roi format!"

        py = roi[0][0]
        px = roi[0][1]
        sy = roi[1][0]
        sx = roi[1][1]
        if len(im.shape) >= 3:
            if transposed:
                tmp = np.zeros((sx, sy, 3), dtype=im.dtype)
                try:
                    for c in range(3):
                        tmp[:, :, c] = np.transpose(im[py:py+sy, px:px+sx, c])
                except:
                    assert False, "Warning, roi grabbing failed!"
            else:
                tmp = np.zeros((sy, sx, 3), dtype=im.dtype)
                try:
                    tmp[:, :, :] = im[py:py+sy, px:px+sx, 0:3]
                except:
                    assert False, "Warning, roi grabbing failed!"
            return tmp
        elif len(im.shape) == 2:
            if transposed:
                tmp = np.zeros((sx, sy, 1), dtype=im.dtype)
                try:
                    tmp[:, :, 0] = np.transpose(im[py:py+sy, px:px+sx])
                except:
                    assert False, "Warning, roi grabbing failed!"
            else:
                tmp = np.zeros((sy, sx, 1), dtype=im.dtype)
                try:
                    tmp[:, :, 0] = im[py:py+sy, px:px+sx]
                except:
                    assert False, "Warning, roi grabbing failed!"
            return tmp
        else:
            assert False, "Warning,  unknown image format!"



def makeFromImageList(image_list, roi=None, name=None, transposed=False, reverse_order=False):
    """
    load a image sequence from file, setting a region of interest is possible

    @param dpath: <str> path to image files
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @retval: <ndarray> data volume
    """

    # check input data
    assert isinstance(image_list, list)
    assert isinstance(image_list[0], np.ndarray)
    if name is None:
        name = "color"
    assert isinstance(name, str)

    if reverse_order:
        image_list.reverse()
    if len(image_list) == 0:
        print("Warning, no data found, Nonetype is returned!")
        return None

    if roi is None:
        shape = [len(image_list)]+list(image_list[0].shape)
    else:
        shape = [len(image_list)]+roi[1]+[image_list[0].shape[2]]
        py = roi[0][0]
        px = roi[0][1]
        sy = roi[1][0]
        sx = roi[1][1]
    if transposed:
        tmp = shape[1]
        shape[1] = shape[2]
        shape[2] = tmp


    data = np.zeros(shape, dtype=image_list[0].dtype)

    #read images
    for n, img in enumerate(image_list):
        if roi is None:
            if transposed:
                for c in range(img.shape[2]):
                    data[n, :, :, c] = np.transpose(img[:, :, c])
            else:
                data[n, :, :, :] = img[:]
        else:
            if transposed:
                for c in range(img.shape[2]):
                    data[n, :, :, c] = np.transpose(img[py:py+sy, px:px+sx, c])
            else:
                data[n, :, :, :] = img[py:py+sy, px:px+sx, :]

    return data



def loadFromSequenceLoc(dpath, roi=None, transposed=False, reverse_order=False):
    """
    load a image sequence from file, setting a region of interest is possible

    @param dpath: <str> path to image files
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @retval: <ndarray> data volume
    """

    # check input data
    assert isinstance(dpath, str)
    if not dpath.endswith(sep):
        dpath += sep

    # load sequence
    files = []
    for ftype in FILE_TYPES:
        for f in glob(dpath + "*."+ftype):
            files.append(f)
    files.sort()
    if reverse_order:
        files.reverse()
    if len(files) == 0:
        print("Warning, no data found, Nonetype is returned!")
        return None

    # read first image to define memory necessary
    try:
        img = readImageROI(files[0], roi, transposed)
    except Exception as e:
        print(e)
        print("Warning, loading image failed, Nonetype is returned!")
        return None

    # uint16 pngs are incorrectly read as int32: cast to uint16
    if img.dtype == np.int32:
        imgdtype = np.uint16
    else:
        imgdtype = img.dtype
        
    shape = [len(files)]+list(img.shape)
    data = np.zeros(shape, dtype=imgdtype)
    data[0, :, :, :] = img[:]
    files.pop(0)

    #read images left
    for n, fname in enumerate(files):
        try:
            img = readImageROI(fname, roi, transposed)
        except Exception as e:
            print(e)
            print("Warning, loading image failed, Nonetype is returned!")
            return None

        data[n+1, :, :, :] = img[:]

    return data



def loadFromFilenameList(filenames, roi=None, name=None, transposed=False, reverse_order=False):
    """
    load a image sequence from filenames list, setting a region of interest is possible

    @param dpath: <str> path to image files
    @param roi: <[[],[]]> region of interest [pos=[y,x], size=[sy,sx]]
    @param name: <str> channel name, [default="color"]
    @param reverse_order: <bool> switch files order [default=False]
    @retval: <ndarray> data volume
    """

    # check input data
    assert (isinstance(filenames, list) or isinstance(filenames, tuple)), "List or tuple expected!"
    if name is None:
        name = "color"
    assert isinstance(name, str)

    files = filenames
    if reverse_order:
        files.reverse()
    if len(files) == 0:
        print("Warning, no data found, Nonetype is returned!")
        return None



    # read first image to define memory necessary
    try:
        img = readImageROI(files[0], roi, transposed)
    except Exception as e:
        print(e)
        print("Warning, loading image failed, Nonetype is returned!")
        return None
    # uint16 pngs are incorrectly read as int32: cast to uint16
    if img.dtype == np.int32:
        imgdtype = np.uint16
    else:
        imgdtype = img.dtype
    
    shape = [len(files)]+list(img.shape)
    data = np.zeros(shape, dtype=imgdtype)
    data[0, :, :, :] = img[:]
    files.pop(0)

    #read images left
    for n, fname in enumerate(files):
        try:
            img = readImageROI(fname, roi, transposed)
        except Exception as e:
            print(e)
            print("Warning, loading image failed, Nonetype is returned!")
            return None

        data[n+1, :, :, :] = img[:]

    return data



