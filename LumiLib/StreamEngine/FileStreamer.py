from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import time
import traceback
from glob import glob
from numpy import zeros
import scipy.misc as misc

from LumiLib.Tools.Parameter import Parameter
from LumiLib.IO.ArrayVector import ArrayVector

IMGFILETYPES = ["png", "PNG", "jpg", "jpeg", "JPG", "JPEG", "tif", "tiff", "TIF", "TIFF", "bmp", "BMP", "ppm"]
DUMPFILETYPES = ["h5"]


def is_locked(filepath):
    """Checks if a file is locked by opening it in append mode.
    If no exception thrown, then the file is not locked.
    """
    locked = None
    file_object = None
    if os.path.exists(filepath):
        try:
            buffer_size = 8
            # Opening file in append mode and read the first 8 characters.
            file_object = open(filepath, 'a', buffer_size)
            if file_object:
                locked = False
        except IOError:
            locked = True
        finally:
            if file_object:
                file_object.close()

    return locked



class FileStreamer(object):

    def __init__(self, parameter):
        """
        This class handles streaming of files by loading them into a buffer
        @param parameter: <Parameter> Parameter instance
        """
        assert isinstance(parameter, Parameter), "Wrong input type, need a Parameter instance!"
        self._parameter = parameter

        self._ready = False
        self._file_type = None
        self._files_dir = None
        self._buffer = []
        self._buffer_size = None
        self._max_buffer = None
        self._buffer_offset = None
        self._current_index = 0
        self._total_index = 0
        self._inipath = None
        self._filetypes_allowed = None
        self._fnames = []


    def read(self):
        pass


    def getLastFilename(self):
        return self._fnames[-1]

    def setFilesPath(self, files_dir):
        """
        set the directory to search for image files
        @param files_dir: <str> image files directory
        """
        if files_dir is not None:
            assert isinstance(files_dir, str), "Wrong files_dir type, need str"

            if not os.path.isabs(files_dir):
                files_dir = os.path.join(self._inipath, files_dir)
            assert os.path.exists(files_dir), "Your iniles path does not exist!"
            self._files_dir = files_dir
            if not self._files_dir.endswith(os.sep):
                self._files_dir += os.sep


    def checkForFiles(self):
        """
        checks if the next image file expected already exist, if so read it and store it in buffer
        @return: <bool> file existance state
        """
        assert self._files_dir is not None, "No files path set!"
        assert self._buffer_size is not None, "No buffer size set!"
        assert self._max_buffer is not None, "No max buffer size set!"

        if self._buffer_offset is None or self._buffer_offset == 0:
            self._buffer_offset = self._buffer_size

        # load all filenames found in the target directory
        fnames = glob(self._files_dir+"*")
        fnames.sort()

        # if filenames found continue,
        # else return False
        if len(fnames) != 0:
            # if no file type specified
            # choose a valid file type
            # from the image filenames found
            if self._file_type is None:
                # check for all filenames
                for f in fnames:
                    # if the inner loop breaks, this one can do die
                    if self._file_type is not None:
                        break
                    # check for all valid file type if
                    # the filename ends on it, if so break
                    for ftype in self._filetypes_allowed:
                        if f.endswith(ftype):
                            self._file_type = ftype
                            break
            # if still no file type set it is better to stop here
            assert self._file_type is not None, "Couldn't find any valid files!"

            #if not enough files found, return False
            if len(fnames) <= self._total_index:
                return False

            # if file is not used by another process read it
            if self._total_index + self._current_index >= len(fnames):
                return False

            if not is_locked(fnames[self._total_index + self._current_index]):
                time.sleep(0.1)
                self._fnames.append(fnames[self._total_index + self._current_index])
                self._buffer.append(self.read(fnames[self._total_index + self._current_index]))
                self._current_index += 1
                # if the current index reaches the buffer size
                # set the buffer to ready
                if self._current_index == self._buffer_size:
                    self._ready = True
                return True
            else:
                return False
        else:
            return False


    def setFileType(self, file_type):
        """
        set the filetype of image files to read
        @param file_type: <str> image file extension
        """
        if file_type is not None:
            assert isinstance(file_type, str), "file_type needs to be a str!"
            if file_type.startswith("."):
                file_type = file_type[1:]
            if self._file_type in self._filetypes_allowed:
                self._file_type = file_type
            else:
                assert False, "Filetype set is not a valid filetype of supprted image formats!"

    def setBufferSize(self, size):
        """
        set the buffer size
        @param size: <int> buffer size
        """
        if size is not None:
            assert isinstance(size, int), "Wrong buffer size type!"
            self._buffer_size = size

    def getBuffer(self):
        """
        get the buffer
        @return: <list> list of ndarrays
        """
        self._ready = False
        return self._buffer

    def setBufferMaximumSize(self, max_buffer):
        """
        set the maximum number of images to load
        @param max_buffer: <int> maximum number of images expected
        """
        if max_buffer is not None:
            assert isinstance(max_buffer, int), "Wrong max buffer size type!"
            assert max_buffer > 0, "Wrong max buffer size value, needs to be greater than zero!"
            self._max_buffer = max_buffer

    def flushBuffer(self):
        """
        clear the buffer
        """
        self._total_index += self._buffer_offset
        self._buffer = []
        self._fnames = []
        self._current_index = 0

    def finished(self):
        """
        FileStreamer status request, returns False if no more files are expected
        @return: <bool> state
        """
        if self._total_index >= self._max_buffer:
            print("stop image reading!")
            return True
        elif self._max_buffer - self._total_index + 1 < self._buffer_size:
            print("stop image reading!")
            return True
        else:
            return False

    def ready(self):
        """
        returns the ready state of the Streamer
        @return: <bool> is ready state
        """
        return self._ready








class ImageStreamer(FileStreamer):

    def __init__(self, parameter):
        """
        This class handles streaming of image files loading them into a buffer
        @param parameter: <Parameter> Parameter instance
        """

        FileStreamer.__init__(self, parameter)

        self._filetypes_allowed = IMGFILETYPES
        self._inipath = self._parameter.get("WorkFlowEngine", "InIPath") + os.sep
        self.setFilesPath(self._parameter.get("WorkFlowEngine", "InputDir"))
        self.setBufferSize(self._parameter.getint("WorkFlowEngine", "BufferSize"))
        self.setBufferMaximumSize(self._parameter.getint("WorkFlowEngine", "MaxBufferSize"))
        self._buffer_offset = self._buffer_size - self._parameter.getint("WorkFlowEngine", "BufferOverlap")
        assert self._buffer_offset > 0, "Buffer offset undefined!"


    def read(self, filename):
        """
        read the image file
        @param filename: <str> image filename
        @return: <ndarray> image data
        """
        try:
            im = misc.imread(filename)
        except:
            print(traceback.format_exc())
            assert False, "Could not read image file!"

        if len(im.shape) == 3:
            return im[:, :, 0:3]
        else:
            tmp = zeros((im.shape[0], im.shape[1], 1), dtype=im.dtype)
            tmp[:, :, 0] = im[:]
            return tmp





class VectorStreamer(FileStreamer):

    def __init__(self, parameter):
        """
        This class handles streaming of vector like hdf5 files loading them into a buffer
        @param parameter: <Parameter> Parameter instance
        """

        FileStreamer.__init__(self, parameter)

        self._filetypes_allowed = DUMPFILETYPES
        self._inipath = self._parameter.get("VectorEngine", "InIPath") + os.sep
        self.setFilesPath(self._parameter.get("VectorEngine", "InputDir"))
        self.setBufferSize(self._parameter.getint("VectorEngine", "BufferSize"))
        self.setBufferMaximumSize(self._parameter.getint("VectorEngine", "MaxBufferSize"))


    def read(self, filename):
        """
        read the dump file
        @param filename: <str> dump filename
        @return: <ArrayVector> ArrayVector instance
        """
        try:
            return ArrayVector(filename)
        except:
            print(traceback.format_exc())
            assert False, "Could not read dump file!"




