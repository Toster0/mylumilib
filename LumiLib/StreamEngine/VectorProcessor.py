from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from LumiLib.IO.ArrayVector import ArrayVector


class VectorProcessor(object):

    def __init__(self):

        self._vectors = None

    def __setVectorList__(self, vectors):
        """
        pass vector data list to processor
        @param vectors: <list<ArrrayVector>> vector data list
        """
        assert isinstance(vectors, list), "Input Error, vectors arg sould be of type list!"
        assert len(vectors) > 0, "Input Error, vectors list is empty!"
        assert isinstance(vectors[0], ArrayVector)

        self._vectors = vectors

    def __run__(self):
        """
        overwrite this method to process your VectorArray list. Access the elements via
        self._vectors[n]. Each entry is a VectorArray. Each channel of the VectorArray
        is invisible to save memory.
        """
        pass