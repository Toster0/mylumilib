from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from LumiLib.IO.ArrayVector import ArrayVector

class WorkFlow(object):

    def __init__(self):
        """
        This is the Baseclass of your CustomWorkFlows, deriving from this class is obligatory
        to run a workflow through the WorkFlowEngine. In child classes implement you operator
        chain by overwriting the process method.
        @param parameter:
        """

        self._data = None


    def process(self):
        """
        Overwrite this function to implement your operator chain. Your ArrayVector
        can be accessed via self._data. All Parameter are set as attributes you can
        access via getAttr(name).
        """
        pass


    def __setLightField__(self, arrv):
        """
        set ArrayVEctor instance
        @param arrv: <ArrayVector>
        """
        assert isinstance(arrv, ArrayVector)
        self._data = arrv

    def __run__(self):
        """
        run the workflow and save data when finished
        """
        try:
            self.process()
            self._data.save()
        except:
            assert False, "Ups, something went wrong while applying your workflow!"
        self._data = None