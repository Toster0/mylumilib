from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import ast
import sys
import time
import traceback

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import multiprocessing
except ImportError:
    print("Seems you haven't installed multiprocessing!")

import FileStreamer
import StreamEngine
import ConfigParser

from WorkFlow import WorkFlow
from LumiLib.Tools.Parameter import checkInI
from LumiLib.Tools.Parameter import Parameter
from LumiLib.IO.ArrayVector import ArrayVector
from LumiLib.IO.ArrayVector import createFromImageList
from LumiLib.loadCustomModules import loadCustomWorkflowClass



def startCrossWorkflowEngineFromInIFile(ini_filename_h, ini_filename_v, threading=True):
    """
    CrossWorkFlow run function creates the Parameter object from the input ini files,
    creates an ImageStreamer, loads the CustomWorkflows specified, sets up and
    runs the Engines. After computing the directons results are meged into a merged.h5
    @param ini_filename_h: <str> horizontal inifile
    @param ini_filename_v: <str> vertical inifile
    @paramm threading: <bool> if True horizontal and vertical are processed in parallel
    """
    assert isinstance(ini_filename_h, str)
    if not ini_filename_h.endswith(".ini"):
        ini_filename_h += ".ini"
    assert os.path.exists(ini_filename_h), "Horizontal ini File does not exist!"
    assert isinstance(ini_filename_v, str)
    if not ini_filename_v.endswith(".ini"):
        ini_filename_v += ".ini"
    assert os.path.exists(ini_filename_v), "Vertical ini File does not exist!"

    jobs = []
    check_h = checkInI(ini_filename_h, section="WorkFlowEngine")
    check_v = checkInI(ini_filename_v, section="WorkFlowEngine")
    if check_h is None and check_h is None:
        # read configfiles
        config_h = ConfigParser.ConfigParser()
        config_h.read(ini_filename_h)
        config_v = ConfigParser.ConfigParser()
        config_v.read(ini_filename_v)

        if threading:
            # start jobs horizontal and vertical in parallel
            p = multiprocessing.Process(target=startWorkflowEngineFromInIFile, args=(ini_filename_h,))
            jobs.append(p)
            p.start()
            p = multiprocessing.Process(target=startWorkflowEngineFromInIFile, args=(ini_filename_v,))
            jobs.append(p)
            p.start()

            # wait until jobs finished
            for job in jobs:
                job.join()
        else:
            startWorkflowEngineFromInIFile(ini_filename_h)
            startWorkflowEngineFromInIFile(ini_filename_v)

        # build result filenames
        resdir_h = config_h.get("WorkFlowEngine", "outputdir")
        resdir_v = config_h.get("WorkFlowEngine", "outputdir")
        resdir_h = os.path.dirname(ini_filename_h) + os.sep + resdir_h
        resdir_v = os.path.dirname(ini_filename_v) + os.sep + resdir_v
        if not resdir_h.endswith(os.sep):
            resdir_h += os.sep
        if not resdir_v.endswith(os.sep):
            resdir_v += os.sep
        resdir_h += config_h.get("WorkFlowEngine", "inputname") + "_0000.h5"
        resdir_v += config_v.get("WorkFlowEngine", "inputname") + "_0000.h5"

        # read data
        lf_h = ArrayVector(resdir_h)
        lf_v = ArrayVector(resdir_v)

        # get channels to merge
        if lf_h.hasDumpedChannel("orientation") and lf_v.hasDumpedChannel("orientation"):
            ori_h = lf_h.getChannel("orientation")[lf_h.shapeOf("orientation")[0]/2, :, :, :]
            ori_v = np.zeros_like(ori_h)
            ori_v[:, :, 0] = np.transpose(lf_v.getChannel("orientation")[lf_v.shapeOf("orientation")[0]/2, :, :, 0])
            ori_v[:, :, 1] = np.transpose(lf_v.getChannel("orientation")[lf_v.shapeOf("orientation")[0]/2, :, :, 1])
        else:
            assert False, "No orientation found!"

        # if median channel available
        # use depth from here
        median_h = None
        median_v = None
        if lf_h.hasDumpedChannel("median") and lf_v.hasDumpedChannel("median"):
            median_h = lf_h.getChannel("median")[lf_h.shapeOf("median")[0]/2, :, :, 0]
            median_v = np.transpose(lf_v.getChannel("median")[lf_v.shapeOf("median")[0]/2, :, :, 0])

        # merge data
        result = np.zeros((ori_h.shape[0], ori_h.shape[1], 2), dtype=np.float32)
        if median_h is not None:
            tmp_oh = median_h[:]
            tmp_ov = median_v[:]
        else:
            tmp_oh = ori_h[:, :, 0]
            tmp_ov = ori_v[:, :, 0]
        tmp_ch = ori_h[:, :, 1]
        tmp_cv = ori_v[:, :, 1]

        winner = np.where(tmp_cv > tmp_ch)
        result[:, :, 0] = tmp_oh[:]
        result[:, :, 1] = tmp_ch[:]
        tmp_ro = result[:, :, 0]
        tmp_rc = result[:, :, 1]
        tmp_ro[winner] = tmp_ov[winner]
        tmp_rc[winner] = tmp_cv[winner]

        # save results
        res_fname = os.path.dirname(resdir_h) + os.sep + "merged.h5"
        resVec = ArrayVector(res_fname)
        resVec.addChannel(result, "merged")
        resVec.save()

    else:
        print("InI-File seems to be not in appropriate condition. Use the tool makeExampleInI() from your workflow!")
        if check_h is not None:
            print("Problems occured at the h entry:", check_h)
        if check_v is not None:
            print("Problems occured at the h entry:", check_v)



def startWorkflowEngineFromInIFile(ini_filename):
    """
    WorkFlow run function creates the Parameter object from the input ini file,
    creates an ImageStreamer, loads the CustomWorkflow specified, sets up and
    runs the Engine.
    @param ini_filename: <str> inifile
    """
    assert isinstance(ini_filename, str)
    if not ini_filename.endswith(".ini"):
        ini_filename += ".ini"
    assert os.path.exists(ini_filename), "InI File does not exist!"

    parameter = Parameter(ini_filename)
    fileStreamer = FileStreamer.ImageStreamer(parameter)

    workflow = loadCustomWorkflowClass(parameter)

    engine = WorkFlowEngine(parameter)
    engine.setFileStreamer(fileStreamer)
    engine.setWorkflow(workflow)
    engine.run()







class WorkFlowEngine(StreamEngine.StreamEngine):
    def __init__(self, parameter):
        """
        This class controls the computational pipeline by running the FileReader passing the data
        to the CustomWorkflow and stopping the pipeline when finished
        @param parameter: <Parameter> Parameter instance keeping all ini file information
        """

        StreamEngine.StreamEngine.__init__(self, parameter)

        self.__name__ = "WorkFlowEngine"    # instance name flag, important to decide btw the ini file sections to read
        self._workflow = None               # custom workflow loaded defined in ini file
        self.initialize()                   # initialize the child parameter depending on __name__


    def setWorkflow(self, workflow):
        """
        Set the WorkFlow instance
        @param workflow: <WorkFlow>
        """
        assert isinstance(workflow, WorkFlow)
        self._workflow = workflow


    def setAttributes(self, lightfield):
        """
        Set all attributes defined in you ini file as attributes of your ArrayVector
        @param workflow: <WorkFlow>
        """
        sections = self._parameter.sections()
        for section in sections:
            items = dict(self._parameter.items(section))
            for key in items:
                try:
                    # if this fails the value is a string
                    value = ast.literal_eval(items[key])
                except:
                    value = items[key]
                lightfield.addAttr(key, value)
        print(lightfield)


    def run(self):
        """
        Set engines main routine running the whole process chain
        """

        # some counters for file reading tries,
        # number of packages already computed and
        # number of images streamed
        fail_counter = 0
        lf_counter = 0
        img_counter = 0

        # start the main loop
        while not self._finished:

            try:
                # sleep a short while before checking if an image is available
                time.sleep(self._check_interval)
                # check if file is availabe
                fail_counter += 1
                if fail_counter % 2 == 0:
                    sys.stdout.write("wait for image . ")
                else:
                    sys.stdout.write("wait for image o ")
                if self._file_streamer.checkForFiles():
                    # if image found reset fail counter
                    # and increase image counter
                    fail_counter = 0
                    img_counter += 1
                    print(" found " + self._file_streamer.getLastFilename())
                else:
                    sys.stdout.write("\r")
                sys.stdout.flush()
                # abort if failed too often to read an image
                assert fail_counter < self._max_tries, "No image found since " + str(fail_counter) + " checks!"
            except Exception as e:
                    print(traceback.format_exc())
                    sys.exit()

            # if FileStreamer buffer is full it's ready
            if self._file_streamer.ready():

                print("\n==============================================")
                print("=======     process image packages     =========")
                try:
                    # get the image buffer
                    imgs = self._file_streamer.getBuffer()

                    # read all important parameter necessary to setup the ArrayVector
                    input_name = self._parameter.get(self.__name__, "inputname")
                    # if lf_counter > 0:
                    #     input_name = input_name[:-5]
                    self._parameter.set(self.__name__, "inputname", input_name)
                    dset_name = input_name+"_"+str(lf_counter).zfill(4)

                    # set the data input name using the current sub lf count
                    self._parameter.set(self.__name__, "dsetname", dset_name)

                    try:
                        roi = self._parameter.getList("Optional", "roi")
                    except:
                        roi = None
                    try:
                        reverse = bool(self._parameter.getint("Optional", "reverseorder"))
                    except:
                        print("Warning, no reverse images flag set, use default", StreamEngine.REVERSE, "!")
                        reverse = StreamEngine.REVERSE
                    try:
                        transposed = bool(self._parameter.getint("Optional", "transposed"))
                    except:
                        print("Warning, no transposed flag set, use default", StreamEngine.TRANSPOSED, "!")
                        transposed = StreamEngine.TRANSPOSED
                    lf_counter += 1

                    # setup the ArrayVector
                    lightfield = createFromImageList(image_list=imgs,
                                                     roi=roi,
                                                     name=input_name,
                                                     reverse_order=reverse,
                                                     transposed=transposed,
                                                     dump_filename=self._output_dir+dset_name+".h5")

                    # set the attributes read from ini file
                    self.setAttributes(lightfield)

                    # pass the ArrayVector to the workflow and start it
                    self._workflow.__setLightField__(lightfield)
                    self._workflow.__run__()

                    # clear the FileStreamer buffer
                    self._file_streamer.flushBuffer()

                except Exception as e:
                    print(e)
                    print(traceback.format_exc())
                    sys.exit()
                print("==============================================\n")

                # if FileStreamer has finished exit the application
                if self._file_streamer.finished():
                    self._finished = True





if __name__ == "__main__":
    if not 2 <= len(sys.argv) <= 3:
        print("Need ini filename as input!")
        sys.exit()
    if len(sys.argv) == 2:
        startWorkflowEngineFromInIFile(sys.argv[1])
    elif len(sys.argv) == 3:
        startCrossWorkflowEngineFromInIFile(sys.argv[1], sys.argv[2])