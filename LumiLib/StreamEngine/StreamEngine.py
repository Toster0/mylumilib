from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import sys
from LumiLib.Tools.Parameter import Parameter


# global defaults
MAX_TRIES = 1000
CHECK_INTERVAL = 0.1
TRANSPOSED = False
REVERSE = False




class StreamEngine(object):
    def __init__(self, parameter):
        """
        This class controls the computational pipeline by running the FileReader passing the data
        to the CustomWorkflow and stopping the pipeline when finished
        @param parameter: <Parameter> Parameter instance keeping all ini file information
        """
        self.__name__ = "StreamEngine"  # instance name flag, important to decide btw the ini file sections to read
        self._output_dir = None         # directory to save results
        self._max_tries = None          # maximum tries of the FileReader to look for the next file
        self._check_interval = None     # time in seconds the FileReader waits between 2 tries to load a files
        self._finished = False          # flag True when streaming finished
        self._file_streamer = None      # instance of a streaming object
        self._parameter = None          # parameter object keeping all the ini file content
        self.setParameter(parameter)    # set the Parameter instance


    def initialize(self):
        # set some WorkflowEngine specific parameter
        try:
            self.setOutputDir(self._parameter.get(self.__name__, "OutputDir"))
        except:
            print("No OutputDir set!")
            sys.exit(0)
        try:
            self.setMaxTries(self._parameter.getint(self.__name__, "MaxTries"))
        except:
            print("Warning, no max_tries set, use default value", MAX_TRIES, "!")
            self.setMaxTries(MAX_TRIES)
        try:
            self.setCeckIntervall(self._parameter.getfloat(self.__name__, "CheckInterval"))
        except:
            print("Warning, no check_interval set, use default value", CHECK_INTERVAL, "!")
            self.setCeckIntervall(CHECK_INTERVAL)



    def setFileStreamer(self, file_streamer):
        """
        Set the FileStreamer instance
        @param file_streamer: <FileStreamer>
        """
        self._file_streamer = file_streamer


    def setParameter(self, parameter):
        """
        Set the Parameter instance
        @param parameter: <Parameter>
        """
        assert isinstance(parameter, Parameter), "Wrong input type, Parameter instance expected!"
        self._parameter = parameter

    def setMaxTries(self, max_tries):
        """
        Set the maximum tries the FileReader will do to load an image
        @param max_tries: <int>
        """
        assert isinstance(max_tries, int), "Wrong parameter input!"
        assert max_tries > 0, "Invalid parameter!"
        self._max_tries = max_tries

    def setCeckIntervall(self, check_interval):
        """
        Set the time in seconds the FileReader waits between two tries to read an image
        @param check_interval: <float>
        """
        assert isinstance(check_interval, float) or isinstance(check_interval, int), "Wrong parameter input!"
        assert check_interval > 0, "Invalid parameter!"
        self._check_interval = float(check_interval)

    def setOutputDir(self, output_dir):
        """
        Set the directory your results should be stored
        @param output_dir: <str>
        """
        assert isinstance(output_dir, str), "Wrong parameter input!"
        if not os.path.isabs(output_dir):
            path = self._parameter.get(self.__name__, "InIPath") + os.sep
            output_dir = path + output_dir
        path = os.path.abspath(output_dir)
        if not os.path.exists(path):
            os.mkdir(path)
        if not path.endswith(os.sep):
            path += os.sep
        self._output_dir = path


    def run(self):
        """
        Set engines main routine running the whole process chain
        """
        pass
