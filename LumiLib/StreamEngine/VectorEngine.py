from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import sys
import time
import traceback

import FileStreamer
import StreamEngine
from LumiLib.Tools.Parameter import Parameter
from LumiLib.loadCustomModules import loadCustomVectorProcessor
from LumiLib.StreamEngine.VectorProcessor import VectorProcessor



def startVectorEngineFromInIFile(ini_filename):
    """
    Engine run function creates the Parameter object from the input ini file,
    creates a DumpStreamer and offer the access to all vectors in an easy to
    extend class.
    @param ini_filename: <str> inifile
    """
    assert isinstance(ini_filename, str)
    if not ini_filename.endswith(".ini"):
        ini_filename += ".ini"
    assert os.path.exists(ini_filename), "InI File does not exist!"

    parameter = Parameter(ini_filename)
    vectorStreamer = FileStreamer.VectorStreamer(parameter)

    processor = loadCustomVectorProcessor(parameter)

    engine = VectorEngine(parameter)
    engine.setFileStreamer(vectorStreamer)
    engine.setVectorProcessor(processor)
    engine.run()







class VectorEngine(StreamEngine.StreamEngine):
    def __init__(self, parameter):
        """
        This class controls the computational pipeline by running the FileReader passing the data
        to the CustomWorkflow and stopping the pipeline when finished
        @param parameter: <Parameter> Parameter instance keeping all ini file information
        """

        StreamEngine.StreamEngine.__init__(self, parameter)

        self.__name__ = "VectorEngine"      # instance name flag, important to decide btw the ini file sections to read
        self._vector_processor = None       # instance of a vector processor loaded dynamically specified in ini file
        self.initialize()                   # initialize the child parameter depending on __name__


    def setVectorProcessor(self, processor):
        """
        Set the vector processor instance
        @param processor: <VectorProcessor>
        """
        assert isinstance(processor, VectorProcessor)
        self._vector_processor = processor


    def run(self):
        """
        Set engines main routine running the whole process chain
        """

        # some counters for file reading tries,
        # number of packages already computed and
        # number of images streamed
        fail_counter = 0
        vecpack_counter = 0
        vec_counter = 0

        # start the main loop
        while not self._finished:

            try:
                # sleep a short while before checking if an image is available
                time.sleep(self._check_interval)
                # check if file is availabe
                fail_counter += 1
                if fail_counter % 2 == 0:
                    sys.stdout.write("wait for vector . ")
                else:
                    sys.stdout.write("wait for vector o ")
                if self._file_streamer.checkForFiles():
                    # if image found reset fail counter
                    # and increase image counter
                    fail_counter = 0
                    vec_counter += 1
                    print(" found " + self._file_streamer.getLastFilename())
                else:
                    sys.stdout.write("\r")
                sys.stdout.flush()
                # abort if failed too often to read an image
                assert fail_counter < self._max_tries, "No image found since " + str(fail_counter) + " checks!"
            except Exception as e:
                    print(traceback.format_exc())
                    sys.exit()

            # if FileStreamer buffer is full it's ready
            if self._file_streamer.ready():

                print("\n==============================================")
                print("======      process vector data       ==========")
                try:

                    #deliver vector data to a processing routine
                    self._vector_processor.__setVectorList__(self._file_streamer.getBuffer())
                    self._vector_processor.__run__()

                    vecpack_counter += 1

                    # clear the FileStreamer buffer
                    self._file_streamer.flushBuffer()

                except Exception as e:
                    print(e)
                    print(traceback.format_exc())
                    sys.exit()
                print("==============================================\n")

                # if FileStreamer has finished exit the application
                if self._file_streamer.finished():
                    self._finished = True





if __name__ == "__main__":
    if not len(sys.argv) == 2:
        print("Need ini filename as input!")
        sys.exit()
    startVectorEngineFromInIFile(sys.argv[1])