from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")


def refocusEpi(img, amount=0):
        """
        this method allows to apply a affine transformation on the input
        image as relative x-axis shifts with respect to the center row.
        On epipolar plane images this is equivalent to a computational refocus
        @param img: <ndarray>
        @param amount: <float> shift amount in pixel
        @returns:  <ndarray> affine transform
        """
        if amount != 0:
            if len(img.shape) == 3:
                tmp = np.zeros((img.shape[0], img.shape[1], img.shape[2]), dtype=img.dtype)
            elif len(img.shape) == 2:
                tmp = np.zeros((img.shape[0], img.shape[1], 1), dtype=img.dtype)
            else:
                assert False, "Unknown image format!"
            for h in xrange(img.shape[0]):
                if len(img.shape) == 3:
                    for c in range(img.shape[2]):
                        tmp[h, :, c] = np.roll(img[h, :, c], (h-img.shape[0]/2)*amount)
                elif len(img.shape) == 2:
                    tmp[h, :, 0] = np.roll(img[h, :], (h-img.shape[0]/2)*amount)
            return tmp
        else:
            return np.copy(img)