from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import vigra
import numpy as np

def findValley(values):
    """
    find a valley in a list of numbers
    @param values: <list> list of numbers
    @retval: <list> list of valley indices
    """
    glob_min = [np.argmin(values), np.min(values)]

    left_max = [0, 0]
    left_max[0] = 0
    left_max[1] = values[0]
    for n, v in enumerate(values):
        if v > glob_min[1]:
            if v > left_max[1]:
                left_max[0] = n
                left_max[1] = v
        else:
            break


    right_max = [len(values)-1, values[-1]]
    right_max[0] = 0
    right_max[1] = values[-1]
    for n in range(len(values)-1, glob_min[0], -1):
        if values[n] > right_max[1]:
            right_max[0] = n
            right_max[1] = values[n]

    thres_left = 0.8*(left_max[1] - glob_min[1])+ glob_min[1]
    thres_right = 0.8*(right_max[1] - glob_min[1])+ glob_min[1]

    valley_min = 0
    valley_max = len(values)
    for n in range(len(values)):
        if values[n] <= thres_left and n < glob_min[0]:
            valley_min = n
            break
    for n in range(right_max[0]-1, glob_min[0], -1):
        if values[n] <= thres_right and n > glob_min[0]:
            valley_max = n
            break

    return range(valley_min, valley_max)


def focusFinder(im_left, im_right, max_disp=None):
    """
    find a focus valley by checking diffenences of left and right image
    @param im_left: <ndarray> left image
    @param im_right: <ndarray> right image
    @param max_disp: <int> max disparity, if not set the half width of the image is chosen
    @return: <list> most likely focus values
    """
    max_disp = max_disp

    if len(im_left.shape) == 3:
        im_left = im_left[:, :, 0:3]
    else:
        tmp = np.zeros((im_left.shape[0], im_left.shape[1], 1), dtype=im_left.dtype)
        tmp[:, :, 0] = im_left[:]
        im_left = tmp

    im_left = im_left[:].astype(np.float32)/255.0
    for c in range(im_left.shape[2]):
        im_left[:, :, c] = vigra.filters.laplacianOfGaussian(im_left[:, :, c], scale=1.0)


    if len(im_right.shape) == 3:
        im_right = im_right[:, :, 0:3]
    else:
        tmp = np.zeros((im_right.shape[0], im_right.shape[1], 1), dtype=im_right.dtype)
        tmp[:, :, 0] = im_right[:]
        im_right = tmp

    im_right = im_right[:].astype(np.float32)/255.0
    for c in range(im_right.shape[2]):
        im_right[:, :, c] = vigra.filters.laplacianOfGaussian(im_right[:, :, c], scale=1.0)

    assert im_left.shape == im_right.shape, "Left and right image shape mismatch!"

    if max_disp is None:
        max_disp = im_left.shape[1]/2

    values = []
    for x in range(max_disp):
        diff_sum = 0.0
        for c in range(im_left.shape[2]):
            tmp_right = np.copy(np.roll(im_right[:, :, c], shift=x, axis=1))
            diff = np.abs(im_left[:, :, c] - tmp_right[:, :])**2
            if x > 0:
                diff[:, 0:x] = 0
            diff_sum += np.sum(diff)
        values.append(diff_sum)

    focuses = findValley(values)
    return focuses








