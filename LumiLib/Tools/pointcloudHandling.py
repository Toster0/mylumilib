from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import traceback

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")


def disparity_to_depth(disparity, base_line_mm, focal_length_px, min_depth_mm=None, max_depth_mm=None):
    """
    computes depth in millimeter from input disparity ndarray

    @param disparity: <ndarray> containing disparities
    @param base_line_mm: <float> float distance between two cameras in millimeter
    @param focal_length_px: <float> camera focal_length in pixel
    @param min_depth_mm: <float> lower depth clipping value in millimeter
    @param max_depth_mm: <float> upper depth clipping value in millimeter
    @return: <ndarray> depth (invalids are set to 1e-25)
    """
    if min_depth_mm is None:
        min_depth_mm = 0.001
        print("Warning, no min_depth_mm defined, set it to 0.001!")
    if isinstance(min_depth_mm, int) or isinstance(min_depth_mm, np.float32):
        min_depth_mm = float(min_depth_mm)
    if max_depth_mm is None:
        max_depth_mm = 100.0
        print("Warning, no max_depth_mm defined, set it to 100.0!")
    if isinstance(max_depth_mm, int) or isinstance(max_depth_mm, np.float32):
        max_depth_mm = float(max_depth_mm)
    if isinstance(base_line_mm, int) or isinstance(base_line_mm, np.float32):
        base_line_mm = float(base_line_mm)
    if isinstance(focal_length_px, int) or isinstance(focal_length_px, np.float32):
        focal_length_px = float(focal_length_px)

    assert isinstance(disparity, np.ndarray), "Input Error, disparity of wrong type!"
    assert isinstance(base_line_mm, float), "Input Error, base_line_mm of wrong type!"
    assert base_line_mm > 0.0, "Input Error, base_line_mm range Exception!"
    assert isinstance(focal_length_px, float), "Input Error, focal_length_px of wrong type!"
    assert focal_length_px > 0.0, "Input Error, focal_length_px range Exception!"
    assert isinstance(min_depth_mm, float), "Input Error, min_depth_mm of wrong type!"
    assert min_depth_mm > 0.0, "Input Error, min_depth_mm range Exception!"
    assert isinstance(max_depth_mm, float), "Input Error, max_depth_mm of wrong type!"
    assert max_depth_mm > min_depth_mm, "Input Error, max_depth_mm range Exception!"

    if len(disparity.shape) == 2:
        depth = np.zeros((disparity.shape[0], disparity.shape[1], 1), dtype=np.float32)
        tmp_disp = np.zeros_like(depth)
        tmp_disp[:, :, 0] = disparity[:]
    elif len(disparity.shape) == 3 and disparity.shape[2] == 1:
        depth = np.zeros_like(disparity).astype(np.float32)
        tmp_disp = disparity
    else:
        assert False, "Input Error, cannot handle disparity input shape!"

    try:
        np.place(tmp_disp, tmp_disp <= 0, 1e-25)
        depth[:] = focal_length_px*base_line_mm / tmp_disp[:]
        np.place(depth, depth < min_depth_mm, 1e-25)
        np.place(depth, depth > max_depth_mm, 1e-25)
    except Exception as e:
        print(e)
        print(traceback.format_exc())

    return depth



def organized_cloud_from_depth_img(depth, focal_length_px=None, color=None, principal_point_yx_px=None):
    """
    computes a point cloud from a depth img by reprojecting from
    image space to world space. The returned cloud represents 3D
    position cloud[x,y,z].

    @param depth_map: <ndarray> depth data
    @param focal_length_px: <float> focal length in px
    @param color: <ndarray> color image
    @return: <ndarray> cloud
    """
    assert isinstance(depth, np.ndarray), "Input Error, depth of wrong type!"
    assert len(depth.shape) != 2, "Input Error, depth of wrong shape!"
    if isinstance(focal_length_px, int) or isinstance(focal_length_px, np.float32):
        focal_length_px = float(focal_length_px)
    assert isinstance(focal_length_px, float), "Input Error, focal_length_px of wrong type!"
    assert focal_length_px > 0.0, "Input Error, focal_length_px range Exception!"
    if color is not None:
        assert isinstance(color, np.ndarray), "Input Error, color of wrong type!"
        assert depth.shape[0] == color.shape[0] and depth.shape[1] == color.shape[1], "Shape mismatch btw depth and color!"
    if principal_point_yx_px is None:
        principal_point_yx_px = [depth.shape[0]/2.0, depth.shape[1]/2.0]
    assert isinstance(principal_point_yx_px, list) or isinstance(principal_point_yx_px, tuple), "Input Error, principal_point_yx_px, not a list or tuple!"
    assert len(principal_point_yx_px) == 2, "Input Error, principal_point_yx_px wrong shape!"

    cloud = np.zeros((depth.shape[0], depth.shape[1], 3), dtype=np.float32)
    cloud[:, :, 2] = depth[:]

    # create a meshgrid
    x = np.linspace(0, depth.shape[1]-1, depth.shape[1])
    y = np.linspace(0, depth.shape[0]-1, depth.shape[0])
    xg, yg = np.meshgrid(x, y)

    # substract the image center
    xg[:] -= principal_point_yx_px[1]
    yg[:] -= principal_point_yx_px[0]

    cloud_x = cloud[y, x, 0]
    cloud_y = cloud[y, x, 1]

    # reproject depth xy plane
    cloud_x[:] = xg[:]*depth[:]/focal_length_px
    cloud_y[:] = yg[:]*depth[:]/focal_length_px

    return cloud