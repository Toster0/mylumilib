from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
import ast
import ConfigParser

from LumiLib.globals import OBLIGATORY_INI_ENTRIES


def checkInI(filename, section):
    config = ConfigParser.ConfigParser()
    config.read(filename)
    for entry in OBLIGATORY_INI_ENTRIES[section]:
        try:
            config.get(section, entry)
        except:
            return entry
    return None


class Parameter(ConfigParser.ConfigParser):
    def __init__(self, filename=None):
        """
        This class handles reading of InI-files and storing
        the attributes set. Accessing them can be done using
        get("section", "name")
        @param filename: <str> input filename of InI-file
        """
        ConfigParser.ConfigParser.__init__(self)

        self.ready = False

        if filename is not None:
            assert isinstance(filename, str), "Wrong input type!"
            self.setFilename(filename)

    def setFilename(self, filename):
        """
        set the InI-file name
        @param filename: <str> filename
        """
        assert isinstance(filename, str), "Wrong input type!"
        assert os.path.exists(filename), "Configfile does not exist!"
        self.read(filename)
        self.ready = True
        try:
            self.set('WorkFlowEngine', 'InIPath', os.path.dirname(filename))
        except:
            pass
        try:
            self.set('VectorEngine', 'InIPath', os.path.dirname(filename))
        except:
            pass

    def getList(self, section, name):
        """
        if value is a list this function returns it correctly
        @param section: <str> section name
        @param name: <str> attribute name
        @return: <list>
        """
        return ast.literal_eval(self.get(section, name))