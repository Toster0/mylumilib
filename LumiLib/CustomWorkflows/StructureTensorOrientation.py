from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""





"""
1. Copy the whole content of this file

2. Paste this code in a new file in your CustomWorkflows directory

3. Give the file and your class the exact same name

!!! CHANGE ONLY CODE WHICH IS WITHIN ##### COMMENT LINES !!!

4. write your makeExampleInI function to ensure users are able to create a valid ini demo ini file
"""




import traceback

from LumiLib.StreamEngine.WorkFlow import WorkFlow

#############################################################################################
#################   L O A D   Y O U R   M O D U L E S   H E R E   ###########################

from LumiLib.DemoOperators.EpiProcessing import Epi1DGradientPrefilter_Operator
from LumiLib.DemoOperators.ImageProcessing import channelReducerRGB2Gray_Operator
from LumiLib.DemoOperators.EpiProcessing import EpiStructureTensorOrientation_Operator
from LumiLib.DemoOperators.ImageProcessing import ImgMedianSmoothing_Operator
from LumiLib.DemoOperators.DepthProcessing import Orientation2Depth_Operator

#                                                                                           #
#############################################################################################


# switches to force re-
# computations of several
# operator steps
RECOMP_PREF = False
RECOMP_ORI = False
RECOMP_MED = False


# !!! name your class exactly the same as your module !!!
class StructureTensorOrientation(WorkFlow):

    def __init__(self):
        WorkFlow.__init__(self)


        #############################################################################################
        ###############   C R E A T E   O T H E R   V A R I A B L E S   H E R E   ###################



        #                                                                                           #
        #############################################################################################




    def process(self):
        """
        Overwrite this function to implement your operator chain. Your ArrayVector
        can be accessed via self._data. All Parameter are set as attributes you can
        access via self._data.getAttr(name).
        """

        #############################################################################################
        ###########   B U I L D   Y O U R   O P E R A T O R    C H A I N   H E R E   ################

        try:
            # get the data input name set via ini file all
            # other channel names we fix here within the workflow
            input_name = self._data.getAttr("inputname")

            # reducecolor not set, set False as default
            if not self._data.hasAttr("reducecolor"):
                self._data.addAttr("reducecolor", 0)

            # prefilterscale not set, set 0 as default
            if not self._data.hasAttr("prefilterscale"):
                self._data.addAttr("prefilterscale", 0)

            # if reducecolor is set, try to convert rgb to
            # grayscale, if data already grayscale  nothing happens
            if self._data.getAttr("reducecolor"):

                # create a channel reducer operator
                reducer = channelReducerRGB2Gray_Operator(self._data)
                reducer.setInputSlot(name=input_name)
                reducer.setOutputSlot(name="gray")
                reducer.run()

                # we do not need rgb data anymore
                self._data.setVisible(input_name, False)

                # if prefilterscale is valid apply gradient prefiltering
                if self._data.getAttr("prefilterscale") > 0.0:
                    # create an epi prefilter operator
                    prefilter = Epi1DGradientPrefilter_Operator(self._data)
                    prefilter.force_recomputation = RECOMP_PREF
                    prefilter.setInputSlot(name="gray")
                    prefilter.setParameterSlot(name="prefilterscale", value=self._data.getAttr("prefilterscale"))
                    prefilter.setOutputSlot(name="prefiltered")
                    prefilter.run()

                    # we do not need grayscale data anymore
                    self._data.setVisible("gray", False)

                    # create an horizontal EpiStructureTensorOrientation_Operator
                    # operating on the horizontal epi domain
                    orientation = EpiStructureTensorOrientation_Operator(self._data, axis=1)
                    orientation.force_recomputation = RECOMP_ORI
                    orientation.setInputSlot(name="prefiltered")
                    orientation.setOutputSlot(name="orientation")
                    orientation.setParameterSlot(name="innerscale", value=self._data.getAttr("innerscale"))
                    orientation.setParameterSlot(name="outerscale", value=self._data.getAttr("outerscale"))
                    orientation.setParameterSlot(name="focusshifts", value=self._data.getAttr("focusshifts"))
                    orientation.setParameterSlot(name="coherencethreshold", value=self._data.getAttr("coherencethreshold"))
                    orientation.run()

                    # we do not need prefiltered data anymore
                    self._data.setVisible("prefiltered", False)

                # if no prefiltering is desired
                # compute from grayscale data
                else:
                    # create an horizontal EpiStructureTensorOrientation_Operator
                    # operating on the horizontal epi domain
                    orientation = EpiStructureTensorOrientation_Operator(self._data, axis=1)
                    orientation.force_recomputation = RECOMP_ORI
                    orientation.setInputSlot(name="gray")
                    orientation.setOutputSlot(name="orientation")
                    orientation.setParameterSlot(name="innerscale", value=self._data.getAttr("innerscale"))
                    orientation.setParameterSlot(name="outerscale", value=self._data.getAttr("outerscale"))
                    orientation.setParameterSlot(name="focusshifts", value=self._data.getAttr("focusshifts"))
                    orientation.setParameterSlot(name="coherencethreshold", value=self._data.getAttr("coherencethreshold"))
                    orientation.run()

                    # we do not need grayscale data anymore
                    self._data.setVisible("gray", False)

            # in this branch we use rgb data if available
            # without applying a channel reducing beforehand
            else:

                # if prefilterscale is valid apply gradient prefiltering
                if self._data.getAttr("prefilterscale") > 0.0:
                    # create an epi prefilter operator
                    prefilter = Epi1DGradientPrefilter_Operator(self._data)
                    prefilter.force_recomputation = RECOMP_PREF
                    prefilter.setInputSlot(name=input_name)
                    prefilter.setParameterSlot(name="prefilterscale", value=self._data.getAttr("prefilterscale"))
                    prefilter.setOutputSlot(name="prefiltered")
                    prefilter.run()

                    # we do not need rgb data anymore
                    self._data.setVisible(input_name, False)

                    # create an horizontal EpiStructureTensorOrientation_Operator
                    # operating on the horizontal epi domain
                    orientation = EpiStructureTensorOrientation_Operator(self._data, axis=1)
                    orientation.force_recomputation = RECOMP_ORI
                    orientation.setInputSlot(name="prefiltered")
                    orientation.setOutputSlot(name="orientation")
                    orientation.setParameterSlot(name="innerscale", value=self._data.getAttr("innerscale"))
                    orientation.setParameterSlot(name="outerscale", value=self._data.getAttr("outerscale"))
                    orientation.setParameterSlot(name="focusshifts", value=self._data.getAttr("focusshifts"))
                    orientation.setParameterSlot(name="coherencethreshold", value=self._data.getAttr("coherencethreshold"))
                    orientation.run()

                    # we do not need prefiltered data anymore
                    self._data.setVisible("prefiltered", False)

                # if no prefiltering is desired
                # compute from rgb data if available
                else:
                    # create an horizontal EpiStructureTensorOrientation_Operator
                    # operating on the horizontal epi domain
                    orientation = EpiStructureTensorOrientation_Operator(self._data, axis=1)
                    orientation.force_recomputation = RECOMP_ORI
                    orientation.setInputSlot(name=input_name)
                    orientation.setOutputSlot(name="orientation")
                    orientation.setParameterSlot(name="innerscale", value=self._data.getAttr("innerscale"))
                    orientation.setParameterSlot(name="outerscale", value=self._data.getAttr("outerscale"))
                    orientation.setParameterSlot(name="focusshifts", value=self._data.getAttr("focusshifts"))
                    orientation.setParameterSlot(name="coherencethreshold", value=self._data.getAttr("coherencethreshold"))
                    orientation.run()

                    # we do not need rgb data anymore
                    self._data.setVisible(input_name, False)

            # this is the postprocessing branch, we assume that a
            # channel orientation is now available for final steps
            # if medianscale is available and greater than 0 filter
            if self._data.getAttr("medianscale") and self._data.getAttr("medianscale") > 0:
                median = ImgMedianSmoothing_Operator(self._data)
                median.force_recomputation = RECOMP_MED
                median.setInputSlot(name="orientation")
                median.setParameterSlot(name="scale", value=self._data.getAttr("medianscale"))
                median.setOutputSlot(name="median")
                median.run()

                # if focal length and baseline parameter are set we assume that median
                # filtered orientation should be converted to real depth values.
                if self._data.getAttr("focallength_px") and self._data.getAttr("baseline_mm"):
                    ori2depth = Orientation2Depth_Operator(self._data)
                    ori2depth.force_recomputation = True
                    ori2depth.setInputSlot(name="median")
                    ori2depth.setParameterSlot(name="focallength_px", value=self._data.getAttr("focallength_px"))
                    ori2depth.setParameterSlot(name="baseline_mm", value=self._data.getAttr("baseline_mm"))
                    if self._data.getAttr("mindepth_mm"):
                        ori2depth.setParameterSlot(name="mindepth_mm", value=self._data.getAttr("mindepth_mm"))
                    if self._data.getAttr("maxdepth_mm"):
                        ori2depth.setParameterSlot(name="maxdepth_mm", value=self._data.getAttr("maxdepth_mm"))
                    ori2depth.setOutputSlot(name="depth")
                    ori2depth.run()

            # if no median filtering is desired, continue
            # here and check if depth should be computed
            else:
                # if focal length and baseline parameter are set we assume
                # that orientation should be converted to real depth values.
                if self._data.getAttr("focallength_px") and self._data.getAttr("baseline_mm"):
                    ori2depth = Orientation2Depth_Operator(self._data)
                    ori2depth.force_recomputation = True
                    ori2depth.setInputSlot(name="orientation")
                    ori2depth.setParameterSlot(name="focallength_px", value=self._data.getAttr("focallength_px"))
                    ori2depth.setParameterSlot(name="baseline_mm", value=self._data.getAttr("baseline_mm"))
                    if self._data.getAttr("mindepth_mm"):
                        ori2depth.setParameterSlot(name="mindepth_mm", value=self._data.getAttr("mindepth_mm"))
                    if self._data.getAttr("maxdepth_mm"):
                        ori2depth.setParameterSlot(name="maxdepth_mm", value=self._data.getAttr("maxdepth_mm"))
                    ori2depth.setOutputSlot(name="depth")
                    ori2depth.run()

            #                                                                                           #
            #############################################################################################


        except Exception as e:
            print(e)
            print(traceback.format_exc())


    #############################################################################################
    ################   C R E A T E   O T H E R   F U N C T I O N S   H E R E   ##################

    def foo(self):
        pass

    #                                                                                           #
    #############################################################################################



#################################################################################################
#################################################################################################
### It is highly recommended to write an makeExampleInI function for your custom workflow     ###
### to guarantee that users are able to create a demo INI containing all important parameter  ###
### your workflow need to run                                                                 ###

import os
import ConfigParser

def makeExampleInI(filename):
    assert isinstance(filename, str)
    if not filename.endswith(".ini"):
        filename += ".ini"
    filename = os.path.abspath(filename)
    if not os.path.exists(os.path.dirname(filename)):
        os.mkdir(filename)

    f = open(filename, "w")

    Config = ConfigParser.ConfigParser(allow_no_value=True)
    cfgfile = open(filename, 'w')

    # add your sections and parameter as well as your comments here using
    # add_section and set functions of the ConfigParser. The section
    # WorkFlowEngine and all parameter in this section are obligatory if you
    # want to use the WorkFlowEngine and FileStreaming procedure...
    # using .set only with 2 parameter starting the second string with a ;
    # acts as comments, otherwise the third parameter is your value

    Config.add_section('WorkFlowEngine')
    Config.set('WorkFlowEngine', ";-----------------------------------------")
    Config.set('WorkFlowEngine', ";PARAMETER IN THIS SECTION ARE OBLIGATORY!")
    Config.set('WorkFlowEngine', ";")
    Config.set('WorkFlowEngine', ";Directory to store your results")
    Config.set('WorkFlowEngine', 'OutputDir', './here/your/result/location/folder')
    Config.set('WorkFlowEngine', ";Directory of your image sequence")
    Config.set('WorkFlowEngine', 'InputDir', './here/your/image/sequence/folder')
    Config.set('WorkFlowEngine', ";Channel name of your Operator chain input!")
    Config.set('WorkFlowEngine', 'InputName', 'rgb')
    Config.set('WorkFlowEngine', ";Name of your workflow script, searched in CustomWorkflows")
    Config.set('WorkFlowEngine', 'Workflow', 'myOwnWorkflow')
    Config.set('WorkFlowEngine', ";MaxTries*CheckInterval is the maximum")
    Config.set('WorkFlowEngine', ";time the engine waits for new images")
    Config.set('WorkFlowEngine', 'MaxTries', 1000)
    Config.set('WorkFlowEngine', 'CheckInterval', 0.1)
    Config.set('WorkFlowEngine', ";Number of images your workflow gets")
    Config.set('WorkFlowEngine', 'BufferSize', 11)
    Config.set('WorkFlowEngine', ";Expected number of images in total")
    Config.set('WorkFlowEngine', 'MaxBufferSize', 121)
    Config.set('WorkFlowEngine', ";BufferOffset defines the overlap of consecutively image packages")
    Config.set('WorkFlowEngine', ";If 0 or not set, none overlapping packages are processed")
    Config.set('WorkFlowEngine', 'BufferOverlap', 0)



    #############################################################################################
    #######           start setting your custom paramter examples here                  #########

    Config.add_section('Optional')
    Config.set('Optional', ";Region of Interest [pos_yx,size_yx]")
    Config.set('Optional', 'roi', [[0, 0], [100, 100]])
    Config.set('Optional', ";Flip files order")
    Config.set('Optional', 'reverseorder', 0)
    Config.set('Optional', ";Vertically sampled light-fields need to be transposed")
    Config.set('Optional', 'transposed', 0)

    Config.add_section('Operator')
    Config.set('Operator', ";---------------------------------------------")
    Config.set('Operator', ";set parameter your operator chain needs here:")
    Config.set('Operator', ";")
    Config.set('Operator', ";parameter names must be the same as your operator expect!")
    Config.set('Operator', ";1 convert rgb to grayscale, 0 use data as rgb if available!")
    Config.set('Operator', "reducecolor", 0)
    Config.set('Operator', ";scale of the prefilter")
    Config.set('Operator', "prefilterscale", 0.4)
    Config.set('Operator', ";structure tensor inner scale")
    Config.set('Operator', "innerscale", 0.6)
    Config.set('Operator', ";structure tensor outer scale")
    Config.set('Operator', "outerscale", 1.0)
    Config.set('Operator', ";threshold ]0,1[ supressing estimation with lower reliability")
    Config.set('Operator', "coherencethreshold", 0.9)
    Config.set('Operator', ";epi shifts in pixel list")
    Config.set('Operator', "focusshifts", [1,2,3])
    Config.set('Operator', ";size of the median filter to smooth the disparity maps, 0 means disabled")
    Config.set('Operator', "medianscale", 3)
    Config.set('Operator', ";if 0 the coherence stays untouched, values > 0 stretches the coherence")
    Config.set('Operator', "coherencestretch", 0)
    Config.set('Operator', ";focal length in pixel")
    Config.set('Operator', "focallength_px", 1000.0)
    Config.set('Operator', ";baseline in millimeter")
    Config.set('Operator', "baseline_mm", 1.0)
    Config.set('Operator', ";minimal depth in millimeter accepted")
    Config.set('Operator', "mindepth_mm", 0.001)
    Config.set('Operator', ";maximal depth in millimeter accepted")
    Config.set('Operator', "maxdepth_mm", 1000.0)
    #                                                                                           #
    #############################################################################################

    Config.write(cfgfile)
    cfgfile.close()




