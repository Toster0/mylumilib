from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
sep = os.sep
CWD = os.path.dirname(os.path.realpath(__file__))

import numpy as np

import unittest

from LumiLib.IO.ArrayVector import ArrayVector, createFrom3DSequence

class TestArrayVector(unittest.TestCase):

    def test_initialization(self):
        # test empty initialization
        arrv = ArrayVector()

        self.assertIsInstance(arrv._data, dict)
        self.assertEqual(len(arrv._data), 0)
        self.assertIsInstance(arrv._attrs, dict)
        self.assertEqual(len(arrv._attrs), 0)
        self.assertIsInstance(arrv._visibility, dict)
        self.assertEqual(len(arrv._visibility), 0)
        self.assertIs(arrv._filename, None)
        self.assertIs(arrv._storage, None)

        # test creating dumpfile while construction
        dump_fname = CWD+sep+"TestSequence"+sep+"test.h5"
        arrv = ArrayVector(filename=dump_fname)

        self.assertIsInstance(arrv._filename, str)
        self.assertEqual(str(type(arrv._storage)), "<class 'h5py._hl.files.File'>")

        arrv.save()
        os.remove(dump_fname)


    def test_dumpfileprocedure(self):

        # test adding data, dumping and loading back
        dump_fname = CWD+sep+"TestSequence"+sep+"test2.h5"
        arrv = ArrayVector(filename=dump_fname)
        gray = np.ones((5, 10, 10, 1), dtype=np.uint8)
        rgb = np.ones((5, 10, 10, 3), dtype=np.float32)
        arrv.addChannel(gray, "gray")
        arrv.addChannel(rgb, "rgb")

        self.assertEqual(len(arrv._data), 2)
        self.assertIsInstance(arrv._data["gray"], np.ndarray)
        self.assertIsInstance(arrv._data["rgb"], np.ndarray)
        self.assertEqual(arrv.hasChannel("gray"), True)
        self.assertEqual(arrv.hasChannel("rgb"), True)
        self.assertEqual(arrv.shapeOf("gray"), (5, 10, 10, 1))
        self.assertEqual(arrv.shapeOf("rgb"), (5, 10, 10, 3))

        # dump data to file
        arrv.setVisible("gray", False)
        arrv.setVisible("rgb", False)

        # data should be removed from object
        # but now stored in the dump file
        self.assertEqual(len(arrv._data), 0)
        self.assertEqual(len(arrv._storage), 2)
        self.assertEqual(arrv._storage["rgb"].shape, (5, 10, 10, 3))
        self.assertEqual(arrv._storage["gray"].shape, (5, 10, 10, 1))
        self.assertEqual(arrv._storage["rgb"].dtype, np.float32)
        self.assertEqual(arrv._storage["gray"].dtype, np.uint8)

        # get data back from file
        arrv.setVisible("gray")
        arrv.setVisible("rgb")

        # data should be back in object
        self.assertEqual(len(arrv._data), 2)
        self.assertEqual(arrv.shapeOf("rgb"), (5, 10, 10, 3))
        self.assertEqual(arrv.shapeOf("gray"), (5, 10, 10, 1))
        self.assertEqual(arrv.dtypeOf("rgb"), np.float32)
        self.assertEqual(arrv.dtypeOf("gray"), np.uint8)

        arrv.save()

        # test reading from existing .h5 file
        # using the load function
        arrv = ArrayVector(filename=dump_fname)

        # if you load from file, by default the data are dumped so you
        # have to set the channels to visible if you want to use them
        arrv.setVisible("rgb")
        arrv.setVisible("gray")

        self.assertEqual(arrv.shapeOf("rgb"), (5, 10, 10, 3))
        self.assertEqual(arrv.shapeOf("gray"), (5, 10, 10, 1))
        self.assertEqual(arrv.dtypeOf("rgb"), np.float32)
        self.assertEqual(arrv.dtypeOf("gray"), np.uint8)

        arrv.save()
        os.remove(dump_fname)


    def test_loadingSequences(self):
        # load a sequence of grayscale images
        seq_loc_bw = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc_bw, name="gray")
        # load a second sequence in the same array vector
        seq_loc_rgb = CWD+sep+"TestSequence"+sep+"rgb"
        arrv = createFrom3DSequence(dpath=seq_loc_rgb, name="rgb", arrv=arrv)

        self.assertTrue(arrv.hasChannel("rgb"))
        self.assertTrue(arrv.hasChannel("gray"))
        self.assertEqual(arrv.shapeOf("rgb"), (9, 100, 100, 3))
        self.assertEqual(arrv.shapeOf("gray"), (9, 100, 100, 1))

        # test setting attributes
        arrv.addAttr("myAttr", 100)

        self.assertEqual(arrv.getAttr("myAttr"), 100)

        # test if attributes are saved and can be loaded again
        dump_fname = CWD+sep+"TestSequence"+sep+"test3.h5"
        # when loading a sequence the ArrayVector instance
        # returned has no filename set, so we have one manually
        arrv.setFilename(dump_fname)
        arrv.save()

        # loading again and test if attributes are there
        # and all data if we use the redump_all flag
        arrv = ArrayVector()
        arrv.load(filename=dump_fname, redump_all=True)
        self.assertEqual(arrv.getAttr("myAttr"), 100)
        self.assertTrue(arrv.hasChannel("rgb"))
        self.assertTrue(arrv.hasChannel("gray"))
        self.assertEqual(arrv.shapeOf("rgb"), (9, 100, 100, 3))
        self.assertEqual(arrv.shapeOf("gray"), (9, 100, 100, 1))

        arrv.save()
        os.remove(dump_fname)

        arrv = ArrayVector()
        arrv.setFilename(dump_fname)
        arrv.loadSequence(seq_loc_rgb, name="rgb")
        arrv.loadSequence(seq_loc_bw, name="gray")
        self.assertTrue(arrv.hasChannel("rgb"))
        self.assertTrue(arrv.hasChannel("gray"))
        self.assertEqual(arrv.shapeOf("rgb"), (9, 100, 100, 3))
        self.assertEqual(arrv.shapeOf("gray"), (9, 100, 100, 1))

        arrv.save()
        os.remove(dump_fname)




if __name__ == "__main__":
    unittest.main()