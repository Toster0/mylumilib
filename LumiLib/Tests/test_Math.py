from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
sep = os.sep
CWD = os.path.dirname(os.path.realpath(__file__))

import numpy as np

import unittest

from LumiLib.Math.Vector import Vector, Vector2, Vector3, Vector4

class TestVector(unittest.TestCase):

    def test_Vector2(self):
        v2 = Vector2()
        assert v2.x == 0
        assert v2.y == 0
        v2 = Vector2(x=1, y=2)
        assert v2.x == 1
        assert v2.y == 2
        v2 = Vector2(data=np.array([1, 2]))
        assert v2.x == 1
        assert v2.y == 2

        vec1 = Vector2(1, 1)
        vec2 = Vector2(1, 2)
        tmp = vec1+vec2
        assert type(tmp) == Vector2
        assert tmp == Vector2(2, 3)
        tmp = vec1-vec2
        assert type(tmp) == Vector2
        assert tmp == Vector2(0, -1)
        tmp = vec1*vec2
        assert type(tmp) == Vector2
        assert tmp == Vector2(1, 2)
        tmp = Vector.DotProduct(vec1, vec2)
        assert tmp == 3
        assert vec1.magnitude() > 1.41421 and vec1.magnitude() < 1.41422
        assert Vector.Magnitude(vec1) == vec1.magnitude()
        assert Vector.Distance(vec1, vec2) == 1

    def test_Vector3(self):
        v3 = Vector3()
        assert v3.x == 0
        assert v3.y == 0
        assert v3.z == 0
        v3 = Vector3(x=1, y=2, z=3)
        assert v3.x == 1
        assert v3.y == 2
        assert v3.z == 3
        v3 = Vector3(data=np.array([1, 2, 3]))
        assert v3.x == 1
        assert v3.y == 2
        assert v3.z == 3

        vec1 = Vector3(1, 1, 1)
        vec2 = Vector3(1, 2, 3)
        tmp = vec1+vec2
        assert type(tmp) == Vector3
        assert tmp == Vector3(2, 3, 4)
        tmp = vec1-vec2
        assert type(tmp) == Vector3
        assert tmp == Vector3(0, -1, -2)
        tmp = vec1*vec2
        assert type(tmp) == Vector3
        assert tmp == Vector3(1, 2, 3)
        tmp = Vector.DotProduct(vec1, vec2)
        assert tmp == 6
        assert vec1.magnitude() > 1.7320 and vec1.magnitude() < 1.7321
        assert Vector.Magnitude(vec1) == vec1.magnitude()
        assert Vector.Distance(vec1, vec2) > 2.23606 and vec1.magnitude() < 2.23607
        assert Vector3.CrossProduct(vec1, vec2) == Vector3(1, -2, 1)

    def test_Vector4(self):
        v4 = Vector4()
        assert v4.x == 0
        assert v4.y == 0
        assert v4.z == 0
        assert v4.w == 0
        v4 = Vector4(x=1, y=2, z=3, w=4)
        assert v4.x == 1
        assert v4.y == 2
        assert v4.z == 3
        assert v4.w == 4
        v4 = Vector4(data=np.array([1, 2, 3, 4]))
        assert v4.x == 1
        assert v4.y == 2
        assert v4.z == 3
        assert v4.w == 4

        vec1 = Vector4(1, 1, 1, 1)
        vec2 = Vector4(1, 2, 3, 4)
        tmp = vec1+vec2
        assert type(tmp) == Vector4
        assert tmp == Vector4(2, 3, 4, 5)
        tmp = vec1-vec2
        assert type(tmp) == Vector4
        assert tmp == Vector4(0, -1, -2, -3)
        tmp = vec1*vec2
        assert type(tmp) == Vector4
        assert tmp == Vector4(1, 2, 3, 4)
        tmp = Vector.DotProduct(vec1, vec2)
        assert tmp == 10
        assert vec1.magnitude() == 2
        assert Vector.Magnitude(vec1) == vec1.magnitude()
        assert Vector.Distance(vec1, vec2) > 3.741 and vec1.magnitude() < 3.742