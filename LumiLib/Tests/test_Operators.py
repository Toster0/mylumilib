from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
sep = os.sep
CWD = os.path.dirname(os.path.realpath(__file__))


try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")
try:
    import h5py as h5
except ImportError:
    print("Seems you haven't installed h5py!")
try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import scipy.misc as misc
except ImportError:
    print("Seems you haven't installed scipy!")


import unittest

from LumiLib.IO.ArrayVector import ArrayVector
from LumiLib.Viewer.ImageViewer import ImageViewer
from LumiLib.IO.ArrayVector import createFrom3DSequence
from LumiLib.DemoOperators.ImageProcessing import ImgGaussianSmoothing_Operator
from LumiLib.DemoOperators.EpiProcessing import EpiStructureTensorOrientation_Operator, EpiGaussianSmoothing_Operator
from LumiLib.StreamEngine.WorkFlowEngine import startWorkflowEngineFromInIFile
from LumiLib.StreamEngine.WorkFlowEngine import startCrossWorkflowEngineFromInIFile




class TestOperators(unittest.TestCase):

    def setUp(self):
        self.viewer = ImageViewer()


    def test_gaussianImgSmoothingOperator(self):

        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"test.h5"
        seq_loc_bw = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc_bw, name="gray")
        # load a second sequence in the same array vector
        seq_loc_rgb = CWD+sep+"TestSequence"+sep+"rgb"
        arrv = createFrom3DSequence(dpath=seq_loc_rgb, name="rgb", arrv=arrv)
        arrv.setFilename(dump_fname)

        # create and apply an GaussianSmoothing operator
        # operating on the image domain
        imgSmoother = ImgGaussianSmoothing_Operator(arrv)
        imgSmoother.setInputSlot(name="rgb")
        imgSmoother.setOutputSlot(name="smoothed")
        imgSmoother.setParameterSlot(name="scale", value=2.0)
        imgSmoother.run()

        # test if smoothing worked
        result = arrv.getChannel("smoothed")[3, :, :, :]
        ref_img = misc.imread(CWD+sep+"TestSequence"+sep+"rgb"+sep+"0003.png")[:, :, 0:3].astype(np.float32)
        for c in range(ref_img.shape[2]):
            ref_img[:, :, c] = vigra.filters.gaussianSmoothing(ref_img[:, :, c].astype(np.float32), sigma=2.0)
        ref_img = np.sum(ref_img, axis=2)
        result = np.sum(result, axis=2)
        sum = np.sum(ref_img-result)
        self.assertAlmostEqual(sum, 0.0)

        # smooth the other channel, too
        imgSmoother.setInputSlot(name="gray")
        imgSmoother.setOutputSlot(name="smoothed_bw")
        imgSmoother.setParameterSlot(name="scale", value=2.0)
        imgSmoother.run()

        # test if smoothing worked
        result = arrv.getChannel("smoothed_bw")[4, :, :, 0]
        ref_img = misc.imread(CWD+sep+"TestSequence"+sep+"bw"+sep+"0004.png")[:, :].astype(np.float32)
        ref_img[:, :] = vigra.filters.gaussianSmoothing(ref_img[:, :].astype(np.float32), sigma=2.0)
        sum = np.sum(ref_img-result)
        self.assertAlmostEqual(sum, 0.0)

        arrv.save()
        os.remove(dump_fname)


    def test_gaussianEpiSmoothingOperator(self):

        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"test1.h5"
        seq_loc_bw = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc_bw, name="gray")
        # load a second sequence in the same array vector
        seq_loc_rgb = CWD+sep+"TestSequence"+sep+"rgb"
        arrv = createFrom3DSequence(dpath=seq_loc_rgb, name="rgb", arrv=arrv)
        arrv.setFilename(dump_fname)

        # create and apply an GaussianSmoothing operator
        # operating on the image domain
        epiSmoother = EpiGaussianSmoothing_Operator(arrv)
        epiSmoother.setInputSlot(name="rgb")
        epiSmoother.setOutputSlot(name="smoothed")
        epiSmoother.setParameterSlot(name="scale", value=1.0)
        epiSmoother.run()

        # test if smoothing worked
        result = arrv.getChannel("smoothed")[:, 50, :, :]
        ref_epi = np.zeros(result.shape, dtype=np.float32)
        for n in range(9):
            ref_epi[n, :, :] = misc.imread(CWD+sep+"TestSequence"+sep+"rgb"+sep+"{}.png".format(str(n).zfill(4)))[50, :, 0:3].astype(np.float32)
        for c in range(ref_epi.shape[2]):
            ref_epi[:, :, c] = vigra.filters.gaussianSmoothing(ref_epi[:, :, c].astype(np.float32), sigma=1.0)

        ref_epi = np.sum(ref_epi, axis=2)
        result = np.sum(result, axis=2)
        sum = np.sum(ref_epi-result)
        self.assertAlmostEqual(sum, 0.0)

        # smooth the other channel, too
        epiSmoother.setInputSlot(name="gray")
        epiSmoother.setOutputSlot(name="smoothed_bw")
        epiSmoother.setParameterSlot(name="scale", value=2.0)
        epiSmoother.run()

        # test if smoothing worked
        result = arrv.getChannel("smoothed_bw")[:, 50, :, 0]
        ref_epi = np.zeros(result.shape, dtype=np.float32)
        for n in range(9):
            ref_epi[n, :] = misc.imread(CWD+sep+"TestSequence"+sep+"bw"+sep+"{}.png".format(str(n).zfill(4)))[50, :].astype(np.float32)
        ref_epi = vigra.filters.gaussianSmoothing(ref_epi.astype(np.float32), sigma=2.0)
        sum = np.sum(ref_epi-result)
        self.assertAlmostEqual(sum, 0.0)

        # if you want keep data in object after
        # saving set remove_data to False
        arrv.save(remove_data=False)
        os.remove(dump_fname)




    def test_EpiStructureTensorOrientation_Operator(self):

        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"test2.h5"
        seq_loc_bw = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc_bw, name="gray")
        # load a second sequence in the same array vector
        seq_loc_rgb = CWD+sep+"TestSequence"+sep+"rgb"
        arrv = createFrom3DSequence(dpath=seq_loc_rgb, name="rgb", arrv=arrv)
        arrv.setFilename(dump_fname)

        # create and apply an horizontal EpiStructureTensorOrientation_Operator
        # operating on the horizontal epi  domain
        orientation = EpiStructureTensorOrientation_Operator(arrv, axis=1)
        orientation.setInputSlot(name="rgb")
        orientation.setOutputSlot(name="orientation_rgb")
        orientation.setParameterSlot(name="innerscale", value=0.6)
        orientation.setParameterSlot(name="outerscale", value=0.8)
        orientation.setParameterSlot(name="focusshifts", value=[-1, 0, 1])
        orientation.setParameterSlot(name="coherencethreshold", value=0.8)
        orientation.run()

        orientation.setInputSlot(name="gray")
        orientation.setOutputSlot(name="orientation_bw")
        orientation.setParameterSlot(name="innerscale", value=0.6)
        orientation.setParameterSlot(name="outerscale", value=0.8)
        orientation.setParameterSlot(name="focusshifts", value=[-1, 0, 1])
        orientation.setParameterSlot(name="coherencethreshold", value=0.8)
        orientation.run()

        # if you want keep data in object after
        # saving set remove_data to False
        arrv.save(remove_data=False)
        os.remove(dump_fname)


    def test_workflowEngine(self):
        dump_fname = CWD+sep+"TestSequence"+sep+"rgb_0000.h5"
        startWorkflowEngineFromInIFile(CWD+sep+"TestSequence"+sep+"test.ini")
        lf = ArrayVector(dump_fname)
        self.assertTrue(lf.hasDumpedChannel("rgb"))
        self.assertTrue(lf.hasDumpedChannel("prefiltered"))
        self.assertTrue(lf.hasDumpedChannel("median"))
        self.assertTrue(lf.hasDumpedChannel("orientation"))

        refm = np.array([ -1.75567161e-09,  -1.85120830e-09,  -4.56146854e-09,
        -5.92981353e-09,  -4.02597378e-09,  -1.36231371e-09,
         2.15671969e-09,   3.24179195e-09,   2.56921218e-09,
         1.61975389e-09,   3.46025181e-10,  -1.06897846e-09,
        -1.69841488e-10,   1.79342963e-09,   5.10252773e-09,
         5.93483751e-09,   4.84561324e-09,   3.83312004e-09,
         3.19818771e-09,   3.20200422e-09,   3.20200422e-09,
         2.60249244e-12,  -1.41117473e-09,  -1.41117473e-09,
        -9.31748123e-10,  -1.60176081e-10,  -5.88977145e-10,
        -3.51739615e-09,  -3.51739615e-09,  -6.21267537e-10,
         9.85523552e-10,  -1.69351166e-09,  -3.87593913e-09,
        -7.48141282e-09,  -6.99250124e-09,  -6.99250124e-09,
        -6.68563693e-09,   4.35922658e-08,   5.16772270e-05,
         7.44462013e-05,   7.44462013e-05,   7.44462013e-05,
         0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
         0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
         9.98622298e-01,   1.00000203e+00,   1.00000203e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   9.99982774e-01,
         9.99964058e-01,   9.99962389e-01,   9.99989569e-01,
         1.00072074e+00,   1.00792837e+00,   1.00856233e+00,
         1.02451193e+00])

        refo = np.array([7.18861326e-09,   4.68639660e-09,   2.85120616e-09,
         2.79635537e-09,   4.45742332e-09,   8.81576856e-09,
         9.61271240e-09,   6.35384545e-09,   6.50279253e-09,
         2.46979948e-09,  -2.48116927e-09,  -2.33316388e-09,
        -8.30852553e-10,  -1.69841488e-10,  -1.65868619e-09,
         1.17808860e-10,   3.83312004e-09,   7.09724990e-09,
         8.39893044e-09,   7.38560280e-09,   4.29922542e-09,
        -1.48621268e-10,  -1.41117473e-09,  -2.41921083e-09,
        -3.42370599e-09,  -6.12149842e-09,  -6.76386236e-09,
        -3.95768218e-09,  -6.21267537e-10,   3.41370732e-09,
         4.67512073e-09,   9.85523552e-10,  -1.69351166e-09,
        -3.87593913e-09,  -7.48141282e-09,  -6.99250124e-09,
        -7.71284814e-09,  -1.03133652e-06,   5.16772270e-05,
         6.16908073e-05,   1.20699406e-04,   9.62674618e-04,
         8.85105133e-03,   0.00000000e+00,   0.00000000e+00,
         0.00000000e+00,   0.00000000e+00,   9.87889290e-01,
         9.98622298e-01,   9.99953449e-01,   1.00000203e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
         1.00000000e+00,   1.00000143e+00,   9.99956429e-01,
         9.99937356e-01,   9.99886811e-01,   9.99864161e-01,
         1.00002670e+00,   1.00304806e+00,   1.02451193e+00,
         1.07456565e+00])

        refd = np.array([1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00000002e-25,   1.00000002e-25,   1.00000002e-25,
         1.00137962e+02,   9.99997940e+01,   9.99997940e+01,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00000000e+02,
         1.00000000e+02,   1.00000000e+02,   1.00001724e+02,
         1.00003593e+02,   1.00003761e+02,   1.00001045e+02,
         9.99279785e+01,   9.92134018e+01,   9.91510391e+01,
         9.76074524e+01])

        self.assertAlmostEqual(np.sum(np.abs(refo[:]-lf.getChannel("orientation")[4, 40, :, 0])), 0.0, places=2)
        self.assertAlmostEqual(np.sum(np.abs(refm[:]-lf.getChannel("median")[4, 40, :, 0])), 0.0, places=2)
        self.assertAlmostEqual(np.sum(np.abs(refd[:]-lf.getChannel("depth")[4, 40, :, 0])), 0.0, places=2)
        os.remove(dump_fname)

        dump_fname = CWD+sep+"TestSequence"+sep+"bw_0000.h5"
        startWorkflowEngineFromInIFile(CWD+sep+"TestSequence"+sep+"test_bw.ini")
        lf = ArrayVector(dump_fname)
        self.assertTrue(lf.hasDumpedChannel("bw"))
        self.assertTrue(lf.hasDumpedChannel("prefiltered"))
        self.assertTrue(lf.hasDumpedChannel("median"))
        self.assertTrue(lf.hasDumpedChannel("orientation"))

        self.assertAlmostEqual(np.sum(np.abs(refo[:]-lf.getChannel("orientation")[4, 40, :, 0])), 0.2319833, places=2)
        self.assertAlmostEqual(np.sum(np.abs(refm[:]-lf.getChannel("median")[4, 40, :, 0])), 0.0214166, places=2)
        self.assertAlmostEqual(np.sum(np.abs(refd[:]-lf.getChannel("depth")[4, 40, :, 0])), 1.886458, places=2)
        os.remove(dump_fname)


    def test_crossWorkflowEngine(self):
        startCrossWorkflowEngineFromInIFile(CWD+sep+"TestSequence"+sep+"cross" + sep + "test_h.ini", CWD+sep+"TestSequence"+sep+"cross" + sep + "test_v.ini")

        ref = np.array([ 0.18730211,  0.20971477,  0.19027287,  0.23426485,  0.24714488,
        0.24218768,  0.21576631,  0.21576631,  0.23049909,  0.23049909,
        0.22045738,  0.23492104,  0.23619759,  0.24264044,  0.24785668,
        0.25623488,  0.21772134,  0.25080085,  0.2392785 ,  0.2392785 ,
        0.24688649,  0.25232792,  0.25518519,  0.28258014,  0.26593411,
        0.29517508,  0.21076024,  0.29959214,  0.30270284,  0.31302661,
        0.32284218,  0.34174943,  0.34174943,  0.32331765,  0.32283008,
        0.32852131,  0.32690954,  0.46453184,  0.55254227,  0.60291368,
        0.71059906,  0.79792082,  0.8134461 ,  0.8161267 ,  0.81210464,
        0.8161267 ,  0.81253332,  0.79012972,  0.78942347,  0.799559  ,
        0.79293984,  0.85040182,  0.79265285,  0.80534887,  0.83227754,
        0.81878197,  0.8171289 ,  0.81071907,  0.70267326,  0.61902803,
        0.57634532,  0.55557865,  0.49942017,  0.35633641,  0.31044972,
        0.33353996,  0.31092781,  0.32777667,  0.31825942,  0.30502003,
        0.28695256,  0.26548761,  0.26548761,  0.22496009,  0.2285952 ,
        0.22858942,  0.20680094,  0.2722519 ,  0.21563482,  0.23337555,
        0.22001761,  0.21591473,  0.21501267,  0.21501267,  0.2520355 ,
        0.22931391,  0.23709059,  0.24907583,  0.25290018,  0.25145102,
        0.25145102,  0.25145102,  0.26079506,  0.25117302,  0.20619684,
        0.20619684,  0.20389831,  0.20774668,  0.23085529,  0.2632733 ])

        f = h5.File(CWD+sep+"TestSequence"+sep+"cross" + sep + "merged.h5", "r")
        res = np.copy(f["merged"][30, :, 0])
        self.assertAlmostEqual(np.sum(np.abs(ref[:]-res[:])), 0.0, places=3)

        os.remove(CWD+sep+"TestSequence"+sep+"cross" + sep + "merged.h5")
        os.remove(CWD+sep+"TestSequence"+sep+"cross" + sep + "test_h_0000.h5")
        os.remove(CWD+sep+"TestSequence"+sep+"cross" + sep + "test_v_0000.h5")

if __name__ == "__main__":
    unittest.main()
