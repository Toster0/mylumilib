from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


import os
sep = os.sep
CWD = os.path.dirname(os.path.realpath(__file__))

import vigra
import numpy as np
from scipy.misc import imread, imsave

import unittest

from LumiLib.IO.ArrayVector import createFrom3DSequence
from LumiLib.DemoOperators.MultiSlotImageProcessing import MultiSlotChannelSeparator
from LumiLib.DemoOperators.MultiSlotImageProcessing import MultiSlotGaussianSmoothing
from LumiLib.DemoOperators.MultiSlotImageProcessing import MultiSlotEdgeFeatures




class TestOperators(unittest.TestCase):


    def test_MultiSlotChannelSeparator(self):
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"cross"+sep+"test.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"
        arrv = createFrom3DSequence(dpath=seq_loc, name="rgb")
        arrv.setFilename(dump_fname)

        separator = MultiSlotChannelSeparator(arrv)
        separator.setInputSlot("input", "rgb")
        separator.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"+sep+"0007.png"
        cv = imread(cv_loc)[:, :, 0:3]
        for c in range(3):
            self.assertAlmostEqual(np.abs(np.sum(cv[:, :, c]-arrv.getChannel("rgb_{}".format(str(c)))[6, :, :, 0])), 0)

        arrv.save()
        os.remove(dump_fname)


    def test_MultiSlotGaussianSmoothing(self):
        # test if different scales for each channel works
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"cross"+sep+"test2.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"
        arrv = createFrom3DSequence(dpath=seq_loc, name="rgb")
        arrv.setFilename(dump_fname)

        scales = [0.3, 2.0, 0.0]
        separator = MultiSlotGaussianSmoothing(arrv)
        separator.setInputSlot("input", "rgb")
        separator.setParameterSlot("scales", scales)
        separator.setOutputSlot("colorsmoothed")
        separator.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"+sep+"0007.png"
        cv = imread(cv_loc)[:, :, 0:3]
        smths = []
        for c in range(3):
            if scales[c] > 0.0:
                smths.append(vigra.filters.gaussianSmoothing(cv[:, :, c].astype(np.float32), scales[c]).astype(np.uint8))
            else:
                smths.append(cv[:, :, c])

        for c in range(3):
            self.assertAlmostEqual(np.abs(np.sum(smths[c]-arrv.getChannel("colorsmoothed")[6, :, :, c])), 0)

        arrv.save()
        os.remove(dump_fname)

        # test if single scale in list works
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"cross"+sep+"test3.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"
        arrv = createFrom3DSequence(dpath=seq_loc, name="rgb")
        arrv.setFilename(dump_fname)

        scales = [2.0, 2.0, 2.0]
        separator = MultiSlotGaussianSmoothing(arrv)
        separator.setInputSlot("input", "rgb")
        separator.setParameterSlot("scales", [2.0])
        separator.setOutputSlot("colorsmoothed")
        separator.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"+sep+"0007.png"
        cv = imread(cv_loc)[:, :, 0:3]
        smths = []
        for c in range(3):
            if scales[c] > 0.0:
                smths.append(vigra.filters.gaussianSmoothing(cv[:, :, c].astype(np.float32), scales[c]).astype(np.uint8))
            else:
                smths.append(cv[:, :, c])

        for c in range(3):
            self.assertAlmostEqual(np.abs(np.sum(smths[c]-arrv.getChannel("colorsmoothed")[6, :, :, c])), 0)

        arrv.save()
        os.remove(dump_fname)


        # test if single scale works
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"cross"+sep+"test4.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"
        arrv = createFrom3DSequence(dpath=seq_loc, name="rgb")
        arrv.setFilename(dump_fname)

        scales = [2.0, 2.0, 2.0]
        separator = MultiSlotGaussianSmoothing(arrv)
        separator.setInputSlot("input", "rgb")
        separator.setParameterSlot("scales", 2.0)
        separator.setOutputSlot("colorsmoothed")
        separator.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"+sep+"0007.png"
        cv = imread(cv_loc)[:, :, 0:3]
        smths = []
        for c in range(3):
            if scales[c] > 0.0:
                smths.append(vigra.filters.gaussianSmoothing(cv[:, :, c].astype(np.float32), scales[c]).astype(np.uint8))
            else:
                smths.append(cv[:, :, c])

        for c in range(3):
            self.assertAlmostEqual(np.abs(np.sum(smths[c]-arrv.getChannel("colorsmoothed")[6, :, :, c])), 0)

        arrv.save()
        os.remove(dump_fname)



        # test if operator also works on grayscale
        # input load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"test5.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc, name="bw")
        arrv.setFilename(dump_fname)

        separator = MultiSlotGaussianSmoothing(arrv)
        separator.setInputSlot("input", "bw")
        separator.setParameterSlot("scales", 2.0)
        separator.setOutputSlot("colorsmoothed")
        separator.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"bw"+sep+"0004.png"
        cv = imread(cv_loc)
        smth = vigra.filters.gaussianSmoothing(cv.astype(np.float32), 2.0).astype(np.uint8)
        self.assertAlmostEqual(np.abs(np.sum(smth-arrv.getChannel("colorsmoothed")[4, :, :, 0])), 0)

        arrv.save()
        os.remove(dump_fname)


    def test_MultiSlotEdgeFeatures(self):
        # test edge features for rgb input
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"cross"+sep+"test6.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"
        arrv = createFrom3DSequence(dpath=seq_loc, name="rgb")
        arrv.setFilename(dump_fname)

        efeatures = MultiSlotEdgeFeatures(arrv)
        efeatures.setInputSlot("input", "rgb")
        efeatures.setParameterSlot("scale_dxy", 1.0)
        efeatures.setParameterSlot("scale_laplace", 1.0)
        efeatures.setParameterSlot("scale_corners", 1.0)
        efeatures.setOutputSlot("dx", "myDx")
        efeatures.setOutputSlot("dy", "myDy")
        efeatures.setOutputSlot("laplace", "myLaplace")
        efeatures.setOutputSlot("corners", "myCorners")
        efeatures.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"cross"+sep+"h"+sep+"0007.png"
        cv = imread(cv_loc)[:, :, 0:3]
        smths = {"myDx":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myDy":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myLaplace":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myCorners":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32)
                 }

        for c in range(3):
            grad = vigra.filters.gaussianGradient(cv[:, :, c].astype(np.float32), 1.0)
            lap = vigra.filters.gaussianGradientMagnitude(cv[:, :, c].astype(np.float32), 1.0)
            corner = vigra.analysis.cornernessHarris(cv[:, :, c].astype(np.float32), 1.0)

            smths["myDx"][:, :, 0] += grad[:, :, 1]
            smths["myDy"][:, :, 0] += grad[:, :, 0]
            smths["myLaplace"][:, :, 0] += lap[:]
            smths["myCorners"][:, :, 0] += corner[:]

        smths["myDx"][:, :, 0] /= 3.0
        smths["myDy"][:, :, 0] /= 3.0
        smths["myLaplace"][:, :, 0] /= 3.0
        smths["myCorners"][:, :, 0] /= 3.0

        for key in smths.keys():
            self.assertAlmostEqual(np.abs(np.sum(smths[key]-arrv.getChannel(key)[6, :, :, :])), 0)

        arrv.save()
        os.remove(dump_fname)


        # test edge features for gray input
        # load a sequence of grayscale images
        dump_fname = CWD+sep+"TestSequence"+sep+"test7.h5"
        seq_loc = CWD+sep+"TestSequence"+sep+"bw"
        arrv = createFrom3DSequence(dpath=seq_loc, name="bw")
        arrv.setFilename(dump_fname)

        efeatures = MultiSlotEdgeFeatures(arrv)
        efeatures.setInputSlot("input", "bw")
        efeatures.setParameterSlot("scale_dxy", 1.0)
        efeatures.setParameterSlot("scale_laplace", 1.0)
        efeatures.setParameterSlot("scale_corners", 1.0)
        efeatures.setOutputSlot("dx", "myDx")
        efeatures.setOutputSlot("dy", "myDy")
        efeatures.setOutputSlot("laplace", "myLaplace")
        efeatures.setOutputSlot("corners", "myCorners")
        efeatures.run()

        cv_loc = CWD+sep+"TestSequence"+sep+"bw"+sep+"0004.png"
        cv = imread(cv_loc)
        smths = {"myDx":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myDy":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myLaplace":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32),
                 "myCorners":np.zeros((cv.shape[0], cv.shape[1], 1), dtype=np.float32)
                 }

        grad = vigra.filters.gaussianGradient(cv[:].astype(np.float32), 1.0)
        lap = vigra.filters.gaussianGradientMagnitude(cv[:].astype(np.float32), 1.0)
        corner = vigra.analysis.cornernessHarris(cv[:].astype(np.float32), 1.0)

        smths["myDx"][:, :, 0] = grad[:, :, 1]
        smths["myDy"][:, :, 0] = grad[:, :, 0]
        smths["myLaplace"][:, :, 0] = lap[:]
        smths["myCorners"][:, :, 0] = corner[:]

        for key in smths.keys():
            self.assertAlmostEqual(np.abs(np.sum(smths[key]-arrv.getChannel(key)[4, :, :, :])), 0)

        arrv.save()
        os.remove(dump_fname)

