from __future__ import print_function
from __future__ import division

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")


def getPixelLookup(pos_yx, slope, shape):
    """
    returns a y-coordinate, x-coordinates list of pixels hits depending
    on ray position and slope. The Position lookups are in subpixel accuracy,
    thus cannot used directly to lookup the values of an array!
    :param pos_yx: <list> [y,x] pixel position
    :param slope: <float> ray slope
    :param shape: <list> image shape [sy,sx]
    :return: <list> [[y_hits],[x_hits]]
    """
    lookup = [[], []]
    slope = 1.0/(slope+1e-23)
    x = lambda y : ((-y + pos_yx[0]) + slope*pos_yx[1]) / slope
    for y in range(0, shape[0]):
        _x  = x(y)
        if np.ceil(round(_x)) >= 0 and np.floor(round(_x)) < shape[1]:
            lookup[0].append(y)
            lookup[1].append(_x)
    return lookup


def getStripeLookup(pos_yx, slope, shape):
    """
    returns a y-coordinate, x-coordinates +/- 1 list of pixels hits depending
    on ray position and slope. The Position lookups are in subpixel accuracy,
    thus cannot used directly to lookup the values of an array!
    :param pos_yx: <list> [y,x] pixel position
    :param slope: <float> ray slope
    :param shape: <list> image shape [sy,sx]
    :return: <list> [[y_hits],[x_hits]]
    """
    lookup = [[], []]
    slope = 1.0/(slope+1e-23)
    x = lambda y : ((-y + pos_yx[0]) + slope*pos_yx[1]) / slope
    for y in range(0, shape[0]):
        _x  = x(y)
        for offset in range(-1, 2):
            if np.ceil(round(_x+offset)) >= 0 and np.floor(round(_x+offset)) < shape[1]:
                lookup[0].append(y)
                lookup[1].append(_x+offset)
    return lookup


def grabFeatures(img, pos_yx, slope, stripe=False):
    """
    grabs pixel along a ray depending on pos_yx and corresponding slope.
    If stripe is true values are extracted also on left and right neighboring
    pixels. The function returns a dictionary with keys pos and values, containing
    each pixel position and the values at this pixel respectively.
    :param img: multichannel image
    :param pos_yx: <list> [y,x] pixel position
    :param slope: <float> ray slope
    :param stripe: <bool> flag for stripe extraction
    :return: <dict> containing pos:pixel positions and values:value ndarray at each pixel
    """
    if len(img.shape) == 2:
        img = img.reshape((img.shape[0], img.shape[1], 1))
    shape = (img.shape[0], img.shape[1])
    if stripe:
        lookup = getStripeLookup(pos_yx, slope, shape)
    else:
        lookup = getPixelLookup(pos_yx, slope, shape)
    features = {"pos":[], "values":[]}
    for n, y in enumerate(lookup[0]):
        x = lookup[1][n]
        features["pos"].append([y,x])
        if np.ceil(x) == np.floor(x):
            features["values"].append(img[int(y), int(x), :])
        else:
            x1 = np.floor(x)
            x2 = np.ceil(x)
            if x2 < shape[1] and x1 >= 0:
                w1 = x2-x
                w2 = x-x1
                features["values"].append(img[int(y), int(x1), :]*w1 + img[int(y), int(x2), :]*w2)
    return features


def buildFeatures(**kwargs):
    """
    Builds a feature vector image from input ndarrays of shape structure shape(sy,sx,c).
    Grayscale data should also be of form shape(sy,sx,1). Argument names are used as keys
    to keep track of data channels. The keys are stored as dictionary {"key":[c0, c1]}
    Accessing features:
    -----------------------------------------------------------------------------------------
                     features[:,:,feature_keys[0]:feature_keys[1]]
    -----------------------------------------------------------------------------------------
    Be aware that this fails if your feature is a single channel feature.
    Access single channel feature:
    -----------------------------------------------------------------------------------------
                            features[:,:,feature_keys[0]]
    -----------------------------------------------------------------------------------------
    To simplify this use the function:
    -----------------------------------------------------------------------------------------
                            getFeatureFromKey(features, feature_keys, key)
    -----------------------------------------------------------------------------------------
    @param **kwargs: input features of type ndarray [sy,sx,c],
    the argument name will be the feature key
    @return: [features<ndarray>, feature_keys<dict>]
    """
    data_channels = kwargs.keys()
    data_channels.reverse()
    num_of_channels = 0
    for key in data_channels:
        num_of_channels += kwargs[key].shape[2]

    features = np.empty((kwargs[data_channels[0]].shape[0], kwargs[data_channels[0]].shape[1], num_of_channels), dtype=np.float32)
    feature_keys = {}
    n = 0
    for key in data_channels:
        features[:, :, n:n+kwargs[key].shape[2]] = kwargs[key][:]
        feature_keys[key] = [n, n+kwargs[key].shape[2]]
        n += kwargs[key].shape[2]
    return [features, feature_keys]


def getFeatureFromKey(data, keys, key):
    """
    Feature vector data access function. Data within the data vector can
    be accessed using the channel indices stored in the keys map. This works
    for a full feature vector array as well as for a single feature vector
    @param data: <ndarray> feature vector array (sy,sx,c) or array(c)
    @param keys: <dict> feature name<->index map
    @param key: <str> feature key to be extracted
    @return: <ndarray> feature vector subset
    """
    if len(data.shape) == 3:
        if keys[key][1]-keys[key][0] == 1:
            return data[:, :, keys[key][0]]
        else:
            return data[:, :, keys[key][0]:keys[key][1]]
    elif len(data.shape) == 1:
        if keys[key][1]-keys[key][0] == 1:
            return data[keys[key][0]]
        else:
            return data[keys[key][0]:keys[key][1]]