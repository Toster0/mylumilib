from __future__ import print_function
from __future__ import division
from tarfile import _data

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")
try:
    import scipy.misc as misc
except ImportError:
    print("Seems you haven't installed scipy!")
try:
    from skimage.color import rgb2gray
except ImportError:
    print("Seems you haven't installed skimage!")


from LumiLib.Tools.refocusing import refocusEpi
from LumiLib.Viewer.viewer import pltshow2, pltshow, imshow

#########################################################################################
#########################################################################################
#################        S T R U C T U R E    T E N S O R S         #####################
#########################################################################################

# def structureTensorOrientation(img, inner_scale=0.6, outer_scale=1.1, max_slope=2.0, tensortype="vigra"):
#     """
#     Returns pixelwise slope values and corresponding coherence
#     Argument img is concerted to grayscale automatically
#     """
#     #TODO: this is obsolet, remove when merge into master
#     if len(img.shape) == 3 and img.shape[2] >= 3:
#         img = rgb2gray(img)
#     elif len(img.shape) == 3 and img.shape[2] == 1:
#         img = img[:, :, 0]
#     if tensortype == "vigra":
#         st = vigra.filters.structureTensor(img.astype(np.float32), inner_scale, outer_scale)
#         Axx = st[:, :, 0]
#         Axy = st[:, :, 1]
#         Ayy = st[:, :, 2]
#     elif tensortype == "skimage":
#         Axx, Axy, Ayy = structure_tensor(img.astype(np.float32), sigma=inner_scale)
#     else:
#         assert False, "Unknown tensortype!"
#
#     up = np.sqrt((Ayy[:]-Axx[:])**2 + 4*Axy[:]**2)
#     down = Ayy[:] + Axx[:]
#     np.place(down, down < 1e-9, 1)
#     coh = np.empty((img.shape[0], img.shape[1], 1), dtype=np.float32)
#     coh[:, :, 0] = up / down
#
#     slope = np.empty((img.shape[0], img.shape[1], 1), dtype=np.float32)
#     slope[:, :, 0] = vigra.numpy.arctan2(2*Axy[:], Ayy[:]-Axx[:]) / 2.0
#     slope[:, :, 0] = vigra.numpy.tan(slope[:, :, 0])
#     if max_slope is not None and max_slope > 0:
#         invalid_ubounds = np.where(slope > max_slope)
#         invalid_lbounds = np.where(slope < -max_slope)
#         coh[invalid_ubounds] = 0.0
#         coh[invalid_lbounds] = 0.0
#         slope[invalid_ubounds] = -max_slope
#         slope[invalid_lbounds] = -max_slope
#
#     return slope, coh


def structureTensorVigra(img, inner_scale=0.6, outer_scale=1.1):
    """
    Returns pixelwise slope values and corresponding coherence
    Argument img is concerted to grayscale automatically
    """
    assert isinstance(img, np.ndarray), "Input Error!"

    if len(img.shape) == 2:
        G = np.copy(img[:])
    elif img.shape[2] == 3:
        G = 0.3*img[:, :, 0]+0.59*img[:, :, 1]+0.11*img[:, :, 2]
    elif img.shape[2] == 1:
        G = img[:, :, 0]
    else:
        assert False, "Wrong input image type!"

    ST = vigra.filters.structureTensor(G.astype(np.float32), inner_scale, outer_scale)
    up = np.sqrt((ST[:, :, 2]-ST[:, :, 0])**2 + 4*ST[:, :, 1]**2)
    down = ST[:, :, 2] + ST[:, :, 0]
    np.place(down, down < 1e-9, 1)
    coh = np.empty((img.shape[0], img.shape[1], 1), dtype=np.float32)
    coh[:, :, 0] = up / down
    return ST, coh

def structureTensorScharr_5x5(img, scale):

    assert isinstance(img, np.ndarray)

    if len(img.shape) == 2:
        G = np.copy(img[:])
    elif img.shape[2] == 3:
        G = 0.3*img[:, :, 0]+0.59*img[:, :, 1]+0.11*img[:, :, 2]
    elif img.shape[2] == 1:
        G = img[:, :, 0]
    else:
        assert False, "Wrong input image type!"

    G = G.astype(np.float32)
    k_x = vigra.filters.Kernel2D()
    k_y = vigra.filters.Kernel2D()
    scharr_x = np.array([[-1, -1, 0, 1, 1],
                         [-2, -2, 0, 2, 2],
                         [-3, -6, 0, 6, 3],
                         [-2, -2, 0, 2, 2],
                         [-1, -1, 0, 1, 1]])/60.0
    scharr_y = np.array([[-1, -2, -3, -2, -1],
                         [-1, -2, -6, -2, -1],
                         [0, 0, 0, 0, 0],
                         [1, 2, 6, 2, 1],
                         [1, 2, 3, 2, 1]])/60.0
    vigra.filters.Kernel2D.initExplicitly(k_x, upperLeft=(0, 0), lowerRight=(4, 4), contents=scharr_x)
    vigra.filters.Kernel2D.initExplicitly(k_y, upperLeft=(0, 0), lowerRight=(4, 4), contents=scharr_y)
    Dx = vigra.filters.convolve(G, k_x)
    Dy = vigra.filters.convolve(G, k_y)

    ST = np.empty((img.shape[0], img.shape[1], 3), dtype=np.float32)
    ST[:, :, 0] = Dy*Dy
    ST[:, :, 1] = Dx*Dy
    ST[:, :, 2] = Dx*Dx
    for c in range(3):
        ST[:, :, c] = vigra.filters.gaussianSmoothing(ST[:, :, c].astype(np.float32), scale)

    up = np.sqrt((ST[:, :, 2]-ST[:, :, 0])**2 + 4*ST[:, :, 1]**2)
    down = ST[:, :, 2] + ST[:, :, 0]
    np.place(down, down < 1e-9, 1)
    C = np.empty((img.shape[0], img.shape[1], 1), dtype=np.float32)
    C[:, :, 0] = up / down
    return ST, C


def orientationFromStructureTensor(tensor,
                                   coherence=None,
                                   coh_threshold=0.5,
                                   max_slope=1.2):
    """
    Computes the orientation from a given structure tensor. If coherence
    is passed, too it thresholds the orientation and the coherence to remove
    bad orientations.
    """
    slope = np.empty((tensor.shape[0], tensor.shape[1], 1), dtype=np.float32)
    slope[:, :, 0] = vigra.numpy.arctan2(2*tensor[:, :, 1], tensor[:, :, 2]-tensor[:, :, 0]) / 2.0
    slope[:, :, 0] = vigra.numpy.tan(slope[:, :, 0])
    if coherence is not None and coh_threshold > 0 and max_slope > 0:
        invalid_ubounds = np.where(slope > max_slope)
        invalid_lbounds = np.where(slope < -max_slope)
        coherence[invalid_ubounds] = 0.0
        coherence[invalid_lbounds] = 0.0
        slope[invalid_ubounds] = -max_slope
        slope[invalid_lbounds] = -max_slope
        invalids = np.where(coherence < coh_threshold)
        coherence[invalids] = 0
        slope[invalids] = -max_slope
    return slope


def focusedStructureTensorOrientation(img,
                                      inner_scale=0.6,
                                      outer_scale=1.1,
                                      focuses=[0],
                                      max_slope=1.2,
                                      coh_threshold=0.95,
                                      st_type="vigra"):
    """
    Returns pixelwise slope values and corresponding coherence
    Argument img is converted to grayscale automatically
    """
    if len(img.shape) == 3 and img.shape[2] >= 3:
        img = rgb2gray(img)
    if len(img.shape) == 3 and img.shape[2] == 1:
        img = img[:, :, 0]

    slope_stack = np.zeros((img.shape[0], img.shape[1], len(focuses)), dtype=np.float32)
    coh_stack = np.zeros((img.shape[0], img.shape[1], len(focuses)), dtype=np.float32)

    for n, f in enumerate(focuses):
        if f == 0:
            epi = img.reshape((img.shape[0], img.shape[1], 1))
        else:
            epi = refocusEpi(img, f)
        if st_type == "vigra":
            st, coh = structureTensorVigra(epi,
                                              inner_scale=inner_scale,
                                              outer_scale=outer_scale)
        if st_type == "scharr_5x5":
            st, coh = structureTensorScharr_5x5(epi, scale=outer_scale)

        ori = orientationFromStructureTensor(st,
                                             coherence=coh,
                                             coh_threshold=coh_threshold,
                                             max_slope=max_slope)
        ori[:] += f
        if f > 0:
            ori = refocusEpi(ori, -f)
            coh = refocusEpi(coh, -f)

        slope_stack[:, :, n] = ori[:, :, 0]
        coh_stack[:, :, n] = coh[:, :, 0]

    # weighted merge
    np.place(coh_stack, coh_stack < coh_threshold, 0)
    csum = np.sum(coh_stack, axis=2)
    np.place(csum, csum < 1e-9, 1)
    for c in range(coh_stack.shape[2]):
        coh_stack[:, :, c] /= csum[:]
    slope_stack *= coh_stack
    final_coh = np.sum(coh_stack, axis=2)
    fonal_ori = np.sum(slope_stack, axis=2)
    return fonal_ori.reshape((img.shape[0], img.shape[1], 1)), final_coh.reshape((img.shape[0], img.shape[1], 1))