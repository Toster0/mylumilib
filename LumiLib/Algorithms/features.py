from __future__ import print_function

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import vigra
except ImportError:
    print("Seems you haven't installed vigra!")

from LumiLib.IO.io import rgb2hsv


def get1DLoGConfidence(epi, kernel_size=7, sigma=1.0, threshold=0):
    assert isinstance(epi, np.ndarray)

    if len(epi.shape) == 2:
        G = np.copy(epi[:])
    elif epi.shape[2] == 3:
        G = 0.3*epi[:, :, 0]+0.59*epi[:, :, 1]+0.11*epi[:, :, 2]
    elif epi.shape[2] == 1:
        G = epi[:, :, 0]
    else:
        assert False, "Wrong input image type!"

    G = vigra.filters.gaussianSmoothing(G.astype(np.float32), 0.6)
    #r = int(kernel_size/2)
    #K = np.array(list(map(lambda x: -1.0/(np.pi*sigma**4)*np.exp(-x**2/(2*sigma**2))*(1-x**2/(2*sigma**2)), range(-r, r+1))))
    #K = np.array([4, 0, -17, -30, -17, 0, 4]).astype(np.float64)
    #K[:] /= np.sum(K)
    k_x = vigra.filters.Kernel1D()
    #vigra.filters.Kernel1D.initExplicitly(k_x, 0, 6, contents=K)
    vigra.filters.Kernel1D.initGaussianDerivative(k_x, sigma, 2)
    LoG = vigra.filters.convolveOneDimension(G, 1, k_x)
    if threshold > 0:
        np.place(LoG, LoG > threshold, 1)
        np.place(LoG, LoG < threshold, 0)
    return LoG.reshape((epi.shape[0], epi.shape[1], 1))


def getEgdeConfidence(epi, min_coherence=0.045):
    """
    Compute edge confidence using the gaussian gradient magnitude normed to [0,1]
    @param epi:
    @param threshold:
    @return:
    """
    conf = np.empty_like(epi)
    if conf.dtype != np.float32:
        conf = conf.astype(np.float32)
    for c in range(epi.shape[2]):
        mag = vigra.filters.gaussianGradientMagnitude((epi[:, :, c]).astype(np.float32), 0.8)
        mag[:] /= np.max(mag[:])
        conf[:, :, c] = mag
    res = np.empty((conf.shape[0], conf.shape[1], 1), dtype=np.float32)
    res[:, :, 0] = np.sum(conf, axis=2)
    res /= np.max(res)
    np.place(res, res < min_coherence, 0)
    return res


def getDOGConfidence(img, threshold=0.01):
    if len(img.shape) == 2:
        img = img.reshape((img.shape[0], img.shape[1], 1))
    if img.shape[2] >= 3:
        img = 0.3*img[:, :, 0]+0.59*img[:, :, 1]+0.11*img[:, :, 2]
        img = img.reshape((img.shape[0], img.shape[1], 1))
    conf = vigra.filters.laplacianOfGaussian(img[:, :, 0].astype(np.float32), 0.6)
    if threshold <= 0:
        return conf.reshape((img.shape[0], img.shape[1], 1))
    np.place(conf, conf < threshold, 0)
    np.place(conf, conf >= threshold, 1)
    return conf.reshape((img.shape[0], img.shape[1], 1))


def getDisneyEgdeConfidence(img, kernel_size=9, threshold=0.02):
    """
    Computes the Disney edge confidence approach.
    !!! Very slow, only for testing at that time !!!
    """
    tmp = np.zeros((img.shape[0], img.shape[1]), dtype=np.float32)
    if len(img.shape) == 2:
        tmp2 = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.float32)
        tmp2[:, :, 0] = img[:]
        img = tmp2
    for c in xrange(img.shape[2]):
        print("process channel", c)
        for y in xrange(img.shape[0]):
            for x in xrange(img.shape[1]):
                val = img[y, x, c]
                res = 0
                for k in xrange(-kernel_size/2, kernel_size/2):
                    if 0 <= x+k < img.shape[1]:
                        res += np.mean(val - img[y, x+k, c])**2
                tmp[y, x] += res
    tmp[:] /= img.shape[2]
    np.place(tmp, tmp < threshold, 0)
    np.place(tmp, tmp >= threshold, 1)
    return tmp

def getIlluminationRobust(im_rgb):
    """
    Returns a illumination robust image by converting rgb to
    an hcc image, whereas h stands for the hue of hsv and saturation
    and value are replaced by an edge weight version of the input image
    """
    assert isinstance(im_rgb, np.ndarray)
    assert len(im_rgb.shape) == 3, "Can only handle rgb images"
    if np.amax(im_rgb) >= 1:
        im_rgb[:] /= 255.0
    hsv = rgb2hsv(im_rgb)
    conf = getEgdeConfidence(im_rgb)
    hsv[:, :, 1] = conf[:]
    hsv[:, :, 2] = conf[:]
    return hsv





if __name__ == "__main__":
    import LumiLib.IO.io as io
    from LumiLib.Viewer.viewer import imshow
    im = io.readRGB("/home/swanner/Dropbox/test_images/lena_rgb.png")
    conf = getEgdeConfidence(im)
    imshow(conf)