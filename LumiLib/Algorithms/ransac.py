from __future__ import print_function
from __future__ import division

"""
Copyright (c) <2014> <copyright Sven Wanner sven.wanner@iwr.uni-heidelberg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


try:
    import numpy as np
except ImportError:
    print("Seems you haven't installed numpy!")
try:
    import scipy as sp
except ImportError:
    print("Seems you haven't installed scipy!")



def ransac(data, model, n, k, t, d):
    """
    fit model parameters to data using the RANSAC algorithm

    @param data: a set of observed data points
    @param model: a model that can be fitted to data points
    @param n: the minimum number of data values required to fit the model
    @param k: the maximum number of iterations allowed in the algorithm
    @param t: a threshold value for determining when a data point fits a model
    @param d: the number of close data values required to assert that a model fits well to data

    @return: bestfit - model parameters which best fit the data (or nil if no good model is found)
    """
    iter = 0
    optfit = None
    opterr = np.inf
    while iter < k:
        maybe_idxs, test_idxs = random_partition(n, data.shape[0])
        possibleinliers = data[maybe_idxs, :]
        test_points = data[test_idxs]
        possiblemodel = model.fit(possibleinliers)
        test_err = model.get_error( test_points, possiblemodel)
        also_idxs = test_idxs[test_err < t]
        alsoinliers = data[also_idxs, :]
        if len(alsoinliers) > d:
            betterdata = np.concatenate((possibleinliers, alsoinliers))
            bettermodel = model.fit(betterdata)
            better_errs = model.get_error(betterdata, bettermodel)
            thiserr = np.mean(better_errs)
            if thiserr < opterr:
                optfit = bettermodel
                opterr = thiserr
        iter += 1
    if optfit is None:
        raise ValueError("did not meet fit acceptance criteria")
    else:
        return optfit

def random_partition(n, n_data):
    """
    return n random rows of data (and also the other len(data)-n rows)
    """
    all_idxs = np.arange(n_data)
    np.random.shuffle(all_idxs)
    idxs1 = all_idxs[:n]
    idxs2 = all_idxs[n:]
    return idxs1, idxs2

class LineFitModel:
    """
    linear system solved using linear least squares

    This class serves as an example that fulfills the model interface
    needed by the ransac() function.
    """
    def __init__(self, input_columns, output_columns):
        self.input_columns = input_columns
        self.output_columns = output_columns

    def fit(self, data):
        X = np.vstack([data[:, i] for i in self.input_columns]).T
        Y = np.vstack([data[:, i] for i in self.output_columns]).T
        XM = np.mean(X)
        YM = np.mean(Y)
        n = data.shape[0]
        XY = np.sum(X*Y)
        XX = np.sum(X*X)
        a = (XY - n*XM*YM)/(XX-n*XM**2+1e-9)
        b = YM - a*XM
        return a, b

    def get_error(self, data, model):
        X = np.vstack([data[:, i] for i in self.input_columns]).T
        Y = np.vstack([data[:, i] for i in self.output_columns]).T
        Y_fit = sp.dot(X, model[0])+model[1]
        err_per_point = np.sum((Y-Y_fit)**2, axis=1) # sum squared error per row
        return err_per_point